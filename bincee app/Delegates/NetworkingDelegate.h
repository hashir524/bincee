//
//  NetworkingDelegate.h
//  shop catalog
//
//  Created by Arslan Raza on 8/31/17.
//  Copyright © 2017 MacBook Pro. All rights reserved.
//

#ifndef NetworkingDelegate_h
#define NetworkingDelegate_h

@protocol NetworkingDelegate <NSObject>
-(void)onFailure:(id)failureError;
@optional
-(void)onGetDataReceive:(id)data;
-(void)onPostDataReceive:(id)data;
-(void)onServiceCancelled:(id)data;
-(void)onDeleteDataResult:(id)data;
@end

#endif /* NetworkingDelegate_h */
