//
//  MyProfileController.swift
//  bincee app
//
//  Created by Apple on 10/17/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class MyProfileController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource,NetworkingDelegate,UITextFieldDelegate,ChangeKidProfileImageProtocol {
    
    var activityIndicator : NVActivityIndicatorView!
    var phoneNoPrevious : String = ""
    var addressPrevious : String = ""
    var namePrevious : String = ""
    var uploadedImage : String = ""
    var uploadedNow : Bool = false
    var kidImageToUploadTag : Int = -1
    
    var previousImage : UIImage = UIImage()
    var isKidImageChanged : Bool = false
    @IBOutlet weak var txtPhoneNo: UITextField!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnEditProfile: UIButton!
    let imagePicker = UIImagePickerController()
    @IBOutlet weak var txtAddress: UITextView!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var imgPerson: UIImageView!
    @IBOutlet weak var btnEditImage: UIButton!
    @IBOutlet weak var tableViewMain: UITableView!
    var kidsArray : NSArray = NSArray()
    override func viewDidAppear(_ animated: Bool) {
        NetworkingFunctions.shared()?.networkingDelegate = self
        updateViewsWithText()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnEditImage.isHidden = true
        kidsArray = KidsArrayData.shared.kidsArraySaved
        self.imgPerson.layer.cornerRadius = self.imgPerson.frame.height / 2
        self.imgPerson.contentMode = .scaleAspectFill
        self.imgPerson.layer.masksToBounds = true
        self.txtName.isEnabled = false
        self.txtAddress.isEditable = false
        self.txtPhoneNo.isEnabled = false
        self.txtName.delegate = self
        self.txtPhoneNo.delegate = self
        imagePicker.delegate = self
        self.tableViewMain.delegate = self
        self.tableViewMain.dataSource = self
        self.makeLoader()
        print("kids array count \(kidsArray.count)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    @IBAction func btnBackPressed(_ sender: Any) {
        
        //Code With Animation
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        self.view.window!.layer.add(transition, forKey: nil)
        self.dismiss(animated: false, completion: nil)
        //Original Coe
        //self.dismiss(animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var headerView : UIView = UIView.init(frame:CGRect.init(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        headerView.backgroundColor = UIColor.white
        
        var headerLable : UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 10, width: tableView.bounds.size.width, height: 20))
        
        
        
        headerLable.text = "Registered Kids"
        headerLable.font = UIFont.init(name: "HelveticaNeue-Bold", size: 17)
        headerLable.textAlignment = .center
        // do whatever headerLabel configuration you want here
        
        
        headerView.addSubview(headerLable)
        
        // Return the headerView
        return headerView;
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var currentKidInfo = kidsArray.object(at: indexPath.row) as! NSDictionary
        self.tableViewMain.register(UINib(nibName: "ChildListCell", bundle: nil), forCellReuseIdentifier: "ChildListCell")
        let cell = tableViewMain.dequeueReusableCell(withIdentifier: "ChildListCell", for: indexPath) as! ChildListCell
        cell.lblChildName.text = currentKidInfo.value(forKey: "fullname") as? String
        cell.lblAddress.text = UserDefaults.standard.value(forKey: "userAddress") as? String
        
        cell.childImageDelegate = self
        cell.imgChild.sd_setImage(with: URL.init(string: currentKidInfo.value(forKey: "photo") as? String ?? ""), placeholderImage: UIImage.init(named: "Kid-profile"), options: .continueInBackground, progress: nil, completed: nil)
        var kidId : Int = currentKidInfo.value(forKey: "id") as? Int ?? -1
        cell.btnChildImageChange.tag = kidId
        if(btnEditProfile.isSelected == true)
        {
            cell.btnChildImageChange.isHidden = false
        }
        else
        {
            cell.btnChildImageChange.isHidden = true
            
        }
        return cell;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return kidsArray.count;
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func updateViewsWithText() {
        txtName.text = UserDefaults.standard.value(forKey: "userFullname") as? String
        lblPhoneNumber.text = UserDefaults.standard.value(forKey: "userPhoneNo") as? String
        txtPhoneNo.text = lblPhoneNumber.text
        txtAddress.text = UserDefaults.standard.value(forKey: "userAddress") as? String
        var profileImageUrl : String = UserDefaults.standard.value(forKey: "userPhoto") as! String
        self.imgPerson.sd_setShowActivityIndicatorView(true)
        self.imgPerson.sd_setIndicatorStyle(.gray)
        if(uploadedNow == false)
        {
            self.imgPerson.sd_setImage(with: URL.init(string: profileImageUrl), placeholderImage: UIImage.init(named: "Avatar-myprofile"), options: .continueInBackground, progress: nil, completed: nil)
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        uploadedNow = false
    }
    @IBAction func btnEditClicked(_ sender: Any) {
        btnEditProfile.isSelected = !btnEditProfile.isSelected
        self.tableViewMain.reloadData()
        if(btnEditProfile.isSelected)
        {
            btnEditImage.isHidden = false
            namePrevious = txtName.text ?? ""
            phoneNoPrevious = txtPhoneNo.text ?? ""
            addressPrevious = txtAddress.text ?? ""
            previousImage = self.imgPerson.image!
            btnEditProfile.setTitle("Save", for: .normal)
            self.txtName.isEnabled = true
            self.txtPhoneNo.isEnabled = true
            self.txtAddress.isEditable = true
            
            self.txtName.becomeFirstResponder()
        }
        else
        {
            btnEditImage.isHidden = true
            btnEditProfile.setTitle("Edit", for: .normal)
            self.txtName.isEnabled = false
            self.txtPhoneNo.isEnabled = false
            self.txtAddress.isEditable = false
            if(self.txtName.text == namePrevious && self.txtAddress.text == addressPrevious && self.txtPhoneNo.text == phoneNoPrevious && self.imgPerson.image == previousImage)
            {
                print("same previous data")
            }
            else
            {
                callApiToUpdateUserData()
                //                uploadImageToServer()
                print("Some change in data")
            }
        }
    }
    func openActionSheet()
    {
        let myAlert = UIAlertController(title: "Select Image Source", message: "Select Camera / Gallery", preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self.imagePicker.mediaTypes = [kUTTypeImage as String]
                self.imagePicker.allowsEditing = true
                self.present( self.imagePicker, animated: true, completion: nil)
            }
        }
        let CameraRollAction = UIAlertAction(title: "Photo Gallery", style: .default) { (action) in
            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary))
            {
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self.imagePicker.mediaTypes = [kUTTypeImage as String]
                self.imagePicker.allowsEditing = true
                self.present( self.imagePicker, animated: true, completion: nil)
                
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            if(self.isKidImageChanged == true)
            {
                self.isKidImageChanged = false
            }
        }
        myAlert.addAction(cameraAction)
        myAlert.addAction(CameraRollAction)
        myAlert.addAction(cancelAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    func callApiToUpdateUserData() {
        var params : [String : Any] = ["fullname" : self.txtName.text , "address" : self.txtAddress.text , "phone_no" : self.txtPhoneNo.text ,"photo" : uploadedImage];
        var userId = UserDefaults.standard.value(forKey: "userId") ?? ""
        self.showLoader()
        NetworkingFunctions.shared()?.postJSON(toURL: "http://access.bincee.com/school/parent/\(userId)", withParams: params, withBearer: true)
    }
    
    func callApiToUpdateKidData(photoUrl : String) {
        var params : [String : Any] = ["photo" : photoUrl];
        
        self.showLoader()
        NetworkingFunctions.shared()?.postJSON(toURL: "http://access.bincee.com/school/student/\(kidImageToUploadTag)", withParams: params, withBearer: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        let image = info[UIImagePickerControllerEditedImage] as? UIImage
        
        
        
        if(isKidImageChanged == true)
        {
            uploadedNow == true
            
            self.sendImageForKid(imgSelected: image!)
            picker.dismiss(animated: true, completion: nil)
            return
        }
        
        self.imgPerson.image = image
        
        
        uploadedNow = true
        
        self.sendImage(imgSelected: image!)
        
        picker.dismiss(animated: false)
        
    }
    func makeLoader()  {
        activityIndicator = NVActivityIndicatorView(frame: CGRect.init(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: 100, height: 100))
        activityIndicator.type = . ballScaleRippleMultiple // add your type
        activityIndicator.color = UIColor(red: 90 / 255, green: 198 / 255, blue: 243 / 255, alpha: 1) // add your color
        activityIndicator.center = self.view.center
    }
    @IBAction func btnEditImageClicked(_ sender: Any) {
        self.isKidImageChanged = false
        openActionSheet()
    }
    
    func checkImage(info: [String : Any])  {
        if let imgUrl = info[UIImagePickerControllerImageURL] as? URL{
            let imgName = imgUrl.lastPathComponent
            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
            let localPath = documentDirectory?.appending(imgName)
            
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            let data = UIImagePNGRepresentation(image)! as NSData
            data.write(toFile: localPath!, atomically: true)
            
            let photoURL = URL.init(fileURLWithPath: localPath!)
            print(photoURL)
            
            
        }
    }
    func onFailure(_ failureError: Any!) {
        self.hideLoader()
        print("failure occured")
    }
    func onPostDataReceive(_ data: Any!) {
        self.hideLoader()
        var convertedData : NSDictionary = data as! NSDictionary
        if(convertedData.value(forKey: "status") as! NSNumber == 200)
        {
            if(isKidImageChanged == true)
            {
                isKidImageChanged == false
                callApiToUpdateKids()
                return
            }
            else
            {
                var nameString = self.txtName.text ?? ""
                var phoneString = self.txtPhoneNo.text ?? ""
                var addressString = self.txtAddress.text ?? ""
                UserDefaults.standard.setValue(nameString, forKey: "userFullname")
                UserDefaults.standard.setValue(phoneString, forKey: "userPhoneNo")
                UserDefaults.standard.setValue(addressString, forKey: "userAddress")
                UserDefaults.standard.setValue(uploadedImage, forKey: "userPhoto")
            }
        }
        else
        {
            print("No data received")
        }
        print("update successfully")
    }
    func sendImage(imgSelected : UIImage) {
        self.showLoader()
        
        
        var authToken : String = UserDefaults.standard.value(forKey: "authToken") as! String
        
        let headers: HTTPHeaders = ["Authorization": "Bearer \(authToken)"]
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            if let imageData = UIImageJPEGRepresentation(imgSelected,0.6) {
                
                multipartFormData.append(imageData, withName: "image", fileName: "file_\(NSTimeIntervalSince1970).png", mimeType: "image/png")
                
            }
        },
                         to: "http://access.bincee.com/avatar/upload",
                         method: .post,
                         headers: headers) { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                print("Success")
                                
                                upload.responseJSON{ response in
                                    print(response.request)
                                    print(response.response)
                                    print(response.data)
                                    print(response.result)
                                    if let JSON = response.result.value {
                                        print("JSON: \(JSON)")
                                        var responseData = JSON as! NSDictionary
                                        self.hideLoader()
                                        if(responseData.value(forKey: "status") as! Int == 200)
                                        {
                                            responseData = responseData.value(forKey: "data") as! NSDictionary
                                            self.uploadedImage = responseData.value(forKey: "path") as! String
                                            self.btnEditClicked(self.btnEditProfile)
                                            
                                        }
                                    }
                                }
                            case .failure(let encodingError):
                                self.hideLoader()
                                print(encodingError)
                            }
        }
    }
    
    func sendImageForKid(imgSelected : UIImage) {
        
        self.showLoader()
        
        var authToken : String = UserDefaults.standard.value(forKey: "authToken") as! String
        
        let headers: HTTPHeaders = ["Authorization": "Bearer \(authToken)"]
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            if let imageData = UIImageJPEGRepresentation(imgSelected,0.6) {
                
                multipartFormData.append(imageData, withName: "image", fileName: "file.png", mimeType: "image/png")
                
            }
        },
                         to: "http://access.bincee.com/avatar/upload",
                         method: .post,
                         headers: headers) { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                print("Success")
                                
                                upload.responseJSON{ response in
                                    print(response.request)
                                    print(response.response)
                                    print(response.data)
                                    print(response.result)
                                    if let JSON = response.result.value {
                                        print("JSON: \(JSON)")
                                        var responseData = JSON as! NSDictionary
                                        self.hideLoader()
                                        if(responseData.value(forKey: "status") as! Int == 200)
                                        {
                                            responseData = responseData.value(forKey: "data") as! NSDictionary
                                            
                                            self.callApiToUpdateKidData(photoUrl: responseData.value(forKey: "path") as! String)
                                            
                                            
                                        }
                                    }
                                }
                            case .failure(let encodingError):
                                self.hideLoader()
                                print(encodingError)
                            }
        }
    }
    func changeImageButtonPressed(btnIndex: Int) {
        self.isKidImageChanged = true
        kidImageToUploadTag = btnIndex
        self.openActionSheet()
    }
    
    func showLoader()  {
        if(activityIndicator.isAnimating == false)
        {
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
        }
    }
    func hideLoader()  {
        if(activityIndicator.isAnimating == true)
        {
            activityIndicator.stopAnimating()
            self.activityIndicator.removeFromSuperview()
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func uploadImageToServer() {
        
        print("Selected image :\(String(describing: self.imgPerson.image))")
        
    }
    func checkIndexPathOfCell() {
        for i in 0..<kidsArray.count
        {
            var oneKidInfo : NSDictionary = kidsArray.object(at: i) as? NSDictionary ?? NSDictionary()
            if(oneKidInfo.value(forKey: "id") as? Int ?? -1 == kidImageToUploadTag)
            {
                
            }
        }
    }
    func callApiToUpdateKids()  {
        var completeUrl : String!
        completeUrl = "http://access.bincee.com/school/parent/student/\(UserDefaults.standard.value(forKey: "userId")!)"
        
        self.showLoader()
        NetworkingFunctions.shared().getJsonData(completeUrl, withParams: nil, withBearer: true)
        
        
        
    }
    func onGetDataReceive(_ data: Any!) {
        var dataConverted = data as! NSDictionary
        
        self.hideLoader()
        if (dataConverted.value(forKey: "status") as! NSNumber == 200)
        {
            
            if dataConverted.value(forKey: "data") is NSArray {
                kidsArray = self.sortArray(currentKidsArray: dataConverted.value(forKey: "data") as! NSArray)
                
                self.tableViewMain.reloadData()
            }
            else
            {
                return
            }
        }
    }
    func sortArray(currentKidsArray : NSArray) -> NSArray {
        let sortedArray = (currentKidsArray as NSArray).sortedArray(using: [NSSortDescriptor(key: "id", ascending: true)]) as? NSArray ?? NSArray()
        return sortedArray
        
    }
}
protocol ChangeKidProfileImageProtocol {
    func changeImageButtonPressed( btnIndex : Int)
    
}

