//
//  SummarizedStatusController.swift
//  bincee app
//
//  Created by Apple on 10/5/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import MapboxCoreNavigation
import MapboxNavigation
import MapboxDirections
import SDWebImage

class SummarizedStatusController: UIViewController,iCarouselDelegate,iCarouselDataSource,LocateMeDelegate,NetworkingDelegate,PerformSegueFromTabbarDelegate{
    func performLocateMeSegue() {
        
        vc.delegate = self
        self.view.addSubview(vc);
    }
    
    let db = Firestore.firestore()
    
    var driverInformation : NSDictionary = NSDictionary()
    var tokenAlreadyExist : Bool = false;
    @IBOutlet weak var imgLocationIcon: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!
    var directionsRoute: Route?
    @IBOutlet weak var vuCarousel: iCarousel!
    @IBOutlet weak var vuLoader: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSchoolName: UILabel!
    @IBOutlet weak var lblETA: UILabel!
    @IBOutlet weak var btnCalendar: UIButton!
    @IBOutlet weak var btnFindMe: UIButton!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnCall: UIButton!
    var timerObject : Timer = Timer()
    var isFirstTime : Bool = true
    var kidsArray : NSArray = NSArray()
    var schoolInfo : NSDictionary = NSDictionary()
    let vc = LocateMeAlert.init(frame: UIScreen.main.bounds)
    //    var isGetKidsApiCalled : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        (self.tabBarController as! TabbarController).segueDelegate = self
        self.saveTokenToFirebase()
        
        callApi()
        self.revealViewController().rearViewRevealWidth = self.view.frame.width-85;
        
        btnFindMe.layer.cornerRadius = self.btnFindMe.frame.height/2;
        
        btnCalendar.layer.cornerRadius = self.btnCalendar.frame.height/2;
        self.vuCarousel.delegate = self
        self.vuCarousel.dataSource = self
        
        self.vuCarousel.type = .invertedTimeMachine
        if self.revealViewController() != nil {
            self.btnMenu.addTarget(self.revealViewController(), action:  #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            
            
        }
        btnFindMe.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0).cgColor
        btnFindMe.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        btnFindMe.layer.shadowOpacity = 0.25
        btnFindMe.layer.shadowRadius = 5.0
        self.btnFindMe.layer.masksToBounds = false
        self.btnFindMe.layoutIfNeeded()
        
        
        btnCalendar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0).cgColor
        btnCalendar.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        btnCalendar.layer.shadowOpacity = 0.25
        btnCalendar.layer.shadowRadius = 5.0
        self.btnCalendar.layer.masksToBounds = false
        
        self.btnCalendar.layoutIfNeeded()
        
        
        timerObject = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(timerFireFunction), userInfo: nil, repeats: true)
        
        //Retreving Token
        print("*****Token From User Defalts \(UserDefaults.standard.string(forKey: "authToken")!)********")
        
        
        
        
        
        
    }
    
    
    func storeNotificationStatus()  {
        //Store Notification status...
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        timerObject.invalidate()
    }
    
    @objc func timerFireFunction()
    {
        self.makePointIndividually(rideTime: UserDefaults.standard.value(forKey: "rideTime") as? String ?? "")
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if(isFirstTime == false)
        {
            callApiWithoutLoader()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        NetworkingFunctions.shared().networkingDelegate = self
        (self.tabBarController as! TabbarController).showTabbar()
        
        
    }
    func checkLocateMe(dataReceived : NSDictionary) {
        
        if(dataReceived.value(forKey: "lat") as? Double ?? 0.0 == 0.0 && dataReceived.value(forKey: "lng") as? Double ?? 0.0 == 0.0)
        {
            UserDefaults.standard.setValue(false, forKey: "isFirstTime")
            
            vc.delegate = self
            self.view.addSubview(vc);
        }
        else
        {
            
        }
    }
    func callApi() {
        self.vuLoader.isHidden = false
        var completeUrl : String!
        completeUrl = "http://access.bincee.com/parent/getData/\(UserDefaults.standard.value(forKey: "userId")!)"
        
        (self.tabBarController as! TabbarController).showLoader()
        NetworkingFunctions.shared()?.cancelTask()
        NetworkingFunctions.shared().getJsonData(completeUrl, withParams: nil, withBearer: true)
    }
    func callApiWithoutLoader() {
        var completeUrl : String!
        completeUrl = "http://access.bincee.com/parent/getData/\(UserDefaults.standard.value(forKey: "userId")!)"
        NetworkingFunctions.shared()?.cancelTask()
        
        NetworkingFunctions.shared().getJsonData(completeUrl, withParams: nil, withBearer: true)
    }
    func showFeedbackAlert() -> Void {
        let vc = ProvideFeedbackAlert.init(frame: UIScreen.main.bounds)
        let vd = LocateMeAlert.init(frame: UIScreen.main.bounds)
        vd.delegate = self
        let ve = SimpleAlert.init(frame: UIScreen.main.bounds)
        
        self.view.addSubview(vd);
        self.view.addSubview(vc);
        self.view.addSubview(ve);
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnMenuPressed(_ sender: Any) {
        
    }
    func numberOfItems(in carousel: iCarousel) -> Int {
        return kidsArray.count
    }
    
    @IBAction func btnCalendarPressed(_ sender: Any) {
        
        (self.tabBarController as! TabbarController).selectCustomTabWithIndex(1)
    }
    
    @IBAction func btnFindMePressed(_ sender: Any) {
        self.performSegue(withIdentifier: "TrackKidSegue", sender: self)
    }
    // MARK : Carousel
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        var currentKidInfo : NSDictionary = NSDictionary()
        var itemView: UIImageView
        var encloseingImageview : UIImageView
        //reuse view if available, otherwise create a new view
        if let view = view as? UIImageView {
            encloseingImageview = view
            itemView = UIImageView(frame: view.frame)
            itemView.sd_setShowActivityIndicatorView(true)
            itemView.sd_setIndicatorStyle(.gray)
            currentKidInfo = kidsArray.object(at: index) as! NSDictionary
            
            var profileImageUrl : String = currentKidInfo.value(forKey: "photo") as? String ?? ""
            
            
            itemView.contentMode = .scaleAspectFill
            itemView.clipsToBounds = true
            if(profileImageUrl == "")
            {
                itemView.image = UIImage(named: "Girk-profile-bg")
            }
            else
            {
                itemView.sd_setShowActivityIndicatorView(true)
                
                itemView.sd_setIndicatorStyle(.gray)
                itemView.sd_setImage(with: URL.init(string: profileImageUrl), placeholderImage: UIImage.init(named: "Girk-profile-bg"), options: .continueInBackground, progress: nil, completed: nil)
            }
            
        } else {
            
            itemView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.vuCarousel.layer.frame.width - 30, height: self.vuCarousel.layer.frame.height - 30))
            encloseingImageview = UIImageView.init(frame: itemView.frame)
            currentKidInfo = kidsArray.object(at: index) as! NSDictionary
            
            var profileImageUrl : String = currentKidInfo.value(forKey: "photo") as? String ?? ""
            
            itemView.contentMode = .scaleAspectFill
            itemView.clipsToBounds = true
            
            if(profileImageUrl == "")
            {
                itemView.image = UIImage(named: "Girk-profile-bg")
            }
            else
            {
                itemView.sd_setShowActivityIndicatorView(true)
                
                itemView.sd_setIndicatorStyle(.gray)
                itemView.sd_setImage(with: URL.init(string: profileImageUrl), placeholderImage: UIImage.init(named: "Girk-profile-bg"), options: .continueInBackground, progress: nil, completed: nil)
            }
            
        }
        itemView.layer.cornerRadius = 10
        encloseingImageview.addSubview(itemView)
        encloseingImageview.tag = currentKidInfo.value(forKey: "id") as? Int ?? -1
        encloseingImageview.layer.shadowColor = UIColor.init(red: 34.0/255.0, green: 198.0/255.0, blue: 252.0/255.0, alpha: 1.0).cgColor
        
        encloseingImageview.layer.shadowRadius = 10
        encloseingImageview.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        encloseingImageview.layer.masksToBounds = false
        encloseingImageview.layer.shadowOpacity = 0.25
        encloseingImageview.layoutIfNeeded()
        
        
        return encloseingImageview
        
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        switch (option) {
        case .spacing: return 0.5
        default: return value
        }
    }
    
    func scaleImage(image:UIImage, view:UIImageView) -> UIImage {
        
        let oldWidth = image.size.width
        let oldHeight = image.size.height
        
        var scaleFactor:CGFloat
        var newHeight:CGFloat
        var newWidth:CGFloat
        let viewWidth:CGFloat = view.bounds.width
        
        if oldWidth > oldHeight {
            
            scaleFactor = oldHeight/oldWidth
            newHeight = viewWidth * scaleFactor
            newWidth = viewWidth
        } else {
            
            scaleFactor = oldHeight/oldWidth
            newHeight = viewWidth * scaleFactor
            newWidth = viewWidth
        }
        
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
        image.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
        
    }
    
    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        print("Carousel current index \(carousel.currentItemIndex)")
        var currentKidInformation : NSDictionary = kidsArray.object(at: carousel.currentItemIndex) as! NSDictionary
        self.lblName.text = currentKidInformation.value(forKey: "fullname") as! String
        
        self.lblSchoolName.text = schoolInfo.value(forKey: "name") as! String
        driverInformation = currentKidInformation.value(forKey: "driver") as! NSDictionary
        CurrentKidDataFirebase.shared.currentKidInformationApi = currentKidInformation
        DriverInformation.shared.driverInformationCurrent = driverInformation
        UserDefaults.standard.setValue(currentKidInformation.value(forKey: "id") as! NSNumber, forKey: "selectedKidId")
        
        var dum = currentKidInformation
        self.getChildInformationFromFirebase()
    }
    func setupCarouselCell(currentPosition : Int){
        var singleKidData : NSDictionary = kidsArray[currentPosition] as! NSDictionary
        
    }
    @IBAction func btnCallPressed(_ sender: Any) {
        print("Address saved already \(SharedParentData.shared.parentData?.data?.address ?? "defaultvalue")")
        
        let vc = DriveInformationAlert.init(frame: UIScreen.main.bounds)
        
        self.view.addSubview(vc);
    }
    @IBAction func btnSearchPressed(_ sender: Any) {
        //        let vc = SimpleAlert.init(frame: UIScreen.main.bounds)
        self.performSegue(withIdentifier: "LocateMeSegue", sender: self)
        //        self.view.addSubview(vc);
    }
    
    func didTapButton() {
        self.vc.removeFromSuperview()
        self.performSegue(withIdentifier: "LocateMeSegue", sender: self)
    }
    func onFailure(_ failureError: Any!) {
        (self.tabBarController as! TabbarController).hideLoader()
        print((failureError as AnyObject).description)
    }
    func onGetDataReceive(_ data: Any!) {
        var dataConverted = data as! NSDictionary
        (self.tabBarController as! TabbarController).hideLoader()
        if (dataConverted.value(forKey: "status") as! NSNumber == 200)
        {
            if(isFirstTime == true)
            {
                isFirstTime = false
            }
            //        if(isGetKidsApiCalled != true)
            //        {
            if dataConverted.value(forKey: "data") is NSDictionary {
                
                
                dataConverted = dataConverted.value(forKey: "data") as! NSDictionary
                schoolInfo = dataConverted.value(forKey: "school") as! NSDictionary
                
                saveDataToDefaults(apiData: dataConverted)
                self.subscribeTopic()
            }
            else
            {
                return
            }
            
            
            kidsArray = self.sortArray(currentKidsArray: dataConverted.value(forKey: "kids") as! NSArray)
            KidsArrayData.shared.kidsArraySaved = kidsArray
            
            if(kidsArray.count != 0)
            {
                self.vuLoader.isHidden = true
                updateKidsInformation(kidsInfo: kidsArray)
            }
            self.vuCarousel.reloadData()
            
            print("data Present")
        }
        else
        {
            dataConverted = dataConverted.value(forKey: "data") as! NSDictionary
            print(dataConverted.value(forKey: "message"))
        }
        
        
        print("Get Data Received")
    }
    
    func sortArray(currentKidsArray : NSArray) -> NSArray {
        //       currentKidsArray =
        let sortedArray = (currentKidsArray as NSArray).sortedArray(using: [NSSortDescriptor(key: "id", ascending: true)]) as? NSArray ?? NSArray()
        return sortedArray
        
    }
    func saveDataToDefaults(apiData : NSDictionary) {
        let fullName : String  = apiData.value(forKey: "fullname") as! String
        let email : String  = apiData.value(forKey: "email") as! String
        var phone_no : String  = apiData.value(forKey: "phone_no") as! String
        var address : String  = apiData.value(forKey: "address") as! String
        var status : String  = apiData.value(forKey: "status") as! String
        var photo : String  = apiData.value(forKey: "photo") as! String
        var schoolId : String = String(apiData.value(forKey: "school_id") as? Int ?? -1)
        UserDefaults.standard.setValue(fullName, forKey: "userFullname")
        UserDefaults.standard.setValue(email, forKey: "userEmail")
        UserDefaults.standard.setValue(address, forKey: "userAddress")
        UserDefaults.standard.setValue(phone_no, forKey: "userPhoneNo")
        UserDefaults.standard.setValue(status, forKey: "userStatus")
        UserDefaults.standard.setValue(photo, forKey: "userPhoto")
        UserDefaults.standard.setValue(schoolId, forKey: "school_id")
        checkLocateMe(dataReceived: apiData)
        
        
    }
    func callKidsListApi() {
        var completeUrl : String!
        completeUrl = "http://access.bincee.com/school/parent/student/\(UserDefaults.standard.value(forKey: "userId")!)"
        NetworkingFunctions.shared().getJsonData(completeUrl, withParams: nil, withBearer: true)
        
    }
    func updateKidsInformation(kidsInfo : NSArray) {
        //here update kids information
    }
    func updateParentDataModel(jsonData : NSDictionary) {
        
        SharedParentData.shared.parentData?.data?.parentId = jsonData.value(forKey: "parent_id") as! Int
        SharedParentData.shared.parentData?.data?.fullname = jsonData.value(forKey: "fullname") as! String
        SharedParentData.shared.parentData?.data?.phoneNo = jsonData.value(forKey: "phone_no") as! String
        SharedParentData.shared.parentData?.data?.address = jsonData.value(forKey: "address") as! String
        SharedParentData.shared.parentData?.data?.email = jsonData.value(forKey: "email") as! String
        SharedParentData.shared.parentData?.data?.lat = jsonData.value(forKey: "lat") as! Float
        SharedParentData.shared.parentData?.data?.lng = jsonData.value(forKey: "lng") as! Float
        SharedParentData.shared.parentData?.data?.status = jsonData.value(forKey: "status") as! String
        SharedParentData.shared.parentData?.data?.photo = jsonData.value(forKey: "photo") as! String
        SharedParentData.shared.parentData?.data?.schoolId = jsonData.value(forKey: "school_id") as! Int
        //        var school =  jsonData.value(forKey: "school") as! School
        SharedParentData.shared.parentData?.data?.kids = jsonData.value(forKey: "kids") as! [response_Kid]
        SharedParentData.shared.parentData?.status = 200
        
        //      ParentDataModel.init(status: 200, data:  dataClassObject)
        
    }
    func getChildInformationFromFirebase() {
        
        var driverId = DriverInformation.shared.driverInformationCurrent.value(forKey: "driver_id") as! Int
        print("Driver id of current child : \(driverId)")
        
        
        db.collection("ride").document(String(driverId)).addSnapshotListener { (snapListen, error) in
            if let err = error
            {
                UserDefaults.standard.setValue("", forKey: "rideTime")
                print("Error getting documents: \(err)")
            }
            else
            {
                //call method to update data
                
                self.onDataChangeFirebase(snapListen: snapListen, driverId: driverId)
                
            }
        }
        
    }
    func onDataChangeFirebase(snapListen : DocumentSnapshot? , driverId : Int)  {
        let userValue : [String : Any] = snapListen?.data() ?? [:]
        var morningShiftId = CurrentKidDataFirebase.shared.currentKidInformationApi.value(forKey: "shift_morning") as? Int ?? -1
        var eveningShiftId = CurrentKidDataFirebase.shared.currentKidInformationApi.value(forKey: "shift_evening") as? Int ?? -1
        if((userValue["shiftId"] as? Int ?? -1) != morningShiftId && eveningShiftId != (userValue["shiftId"] as? Int ?? -1))
        {
            self.rideNotPresent()
            return
        }
        if(userValue.count == 0)
        {
            self.rideNotPresent()
            return
        }
        if(userValue["rideInProgress"] as! Bool == false)
        {
            self.rideNotPresent()
            return
        }
        else
        {
            self.rideInProgress()
        }
        print("Ride Id : \(userValue["rideId"]!)")
        
        let studentsArray = userValue["students"] as! NSArray
        var rideTime : String = userValue["shift"] as! String
        
        print(userValue)
        //print("**********Parent ID***** \(userValue["parent_id"])")
        UserDefaults.standard.setValue(rideTime, forKey: "rideTime")
        if let date = userValue["latLng"] as? GeoPoint {
            //Here you received string 'date'
            var points = userValue["latLng"] as! GeoPoint
            var schoolPoint = userValue["schoolLatLng"] as! GeoPoint
            print("students location \(points.latitude)")
            self.checkStudentExistence(studentsArray: studentsArray, driverPosition: points, driverId: driverId, schoolLatLng: schoolPoint)
        }
        else
        {
            var geoPoint : GeoPoint = GeoPoint.init(latitude: 0.0, longitude: 0.0)
            //            var points = userValue["latLng"] as? GeoPoint ??
            var schoolPoint = userValue["schoolLatLng"] as! GeoPoint
            self.checkStudentExistence(studentsArray: studentsArray, driverPosition: geoPoint, driverId: driverId, schoolLatLng: schoolPoint)
        }
    }
    func rideNotPresent() {
        //        self.btnFindMe.isHidden = true
        self.btnFindMe.isEnabled = false
        self.btnFindMe.backgroundColor = UIColor.gray
        
        self.lblStatus.text = ""
        self.lblETA.text = ""
        self.imgLocationIcon.isHidden = true
        self.lblETA.isHidden = true
        NotificationCenter.default.post(name: Notification.Name(rawValue: "markedAbsent"), object: nil, userInfo: nil)
    }
    func rideInProgress()  {
        //        self.btnFindMe.isHidden = false
        self.btnFindMe.isEnabled = true
        self.btnFindMe.backgroundColor = UIColor(red: 3/255, green: 197/255, blue: 255/255, alpha: 1.0)
        self.imgLocationIcon.isHidden = false
    }
    func markAbsent() {
        self.lblETA.isHidden = true
        //        self.btnFindMe.isHidden = true
        self.btnFindMe.isEnabled = false
        self.btnFindMe.backgroundColor = UIColor.gray
        
    }
    func markPresent()
    {
        self.lblETA.isHidden = false
        //        self.btnFindMe.isHidden = false
        self.btnFindMe.isEnabled = true
        self.btnFindMe.backgroundColor = UIColor(red: 3/255, green: 197/255, blue: 255/255, alpha: 1.0)
        self.btnFindMe.alpha = 1
        
    }
    func populateStatusAccordingly()  {
        
        if(UserDefaults.standard.value(forKey: "rideTime") as! String == "morning")
        {
            var statusCurrentKid =  CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "status") as? Int
            if(statusCurrentKid == 1)
            {
                self.lblStatus.text = "Bus is coming"
                self.lblETA.isHidden = false
                self.imgLocationIcon.isHidden = false
            }
            else if(statusCurrentKid == 2)
            {
                self.lblStatus.text = "Bus is here"
                self.lblETA.isHidden = true
                self.imgLocationIcon.isHidden = true
            }
            else if(statusCurrentKid == 3)
            {
                self.lblStatus.text = "On the Way"
                self.lblETA.isHidden = false
                self.imgLocationIcon.isHidden = false
            }
            else if(statusCurrentKid == 4)
            {
                self.lblStatus.text = "Reached"
                self.lblETA.isHidden = true
                self.imgLocationIcon.isHidden = true
                //When Driver will End Ride Then this button will be disabled
                //self.btnFindMe.isEnabled = false
                //self.btnFindMe.backgroundColor = UIColor.gray
            }
            else
            {
                self.lblStatus.text = ""
                self.lblETA.text = ""
            }
        }
        else if(UserDefaults.standard.value(forKey: "rideTime") as! String == "Evening1")
        {
            self.lblStatus.text = "School is over"
            self.lblETA.text = ""
            self.lblStatus.isHidden = false
            self.imgLocationIcon.isHidden = false
        }
        else
        {
            // Evening statuses goes here
            var statusCurrentKid =  CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "status") as? Int
            if(statusCurrentKid == 1)
            {
                self.lblStatus.text = "School is over"
                self.lblETA.text = ""
            }
            else if(statusCurrentKid == 2)
            {
                self.lblStatus.text = "In the bus"
                self.lblETA.isHidden = false
                self.imgLocationIcon.isHidden = false
            }
            else if(statusCurrentKid == 3)
            {
                self.lblStatus.text = "Almost There"
                self.lblETA.isHidden = false
                self.imgLocationIcon.isHidden = false
            }
            else if(statusCurrentKid == 4)
            {
                self.lblStatus.text = "At your doorstep"
                self.lblETA.isHidden = true
                self.imgLocationIcon.isHidden = false
            }
            else
            {
                self.lblStatus.text = ""
                self.lblETA.text = ""
                
            }
        }
        self.makePointIndividually(rideTime: UserDefaults.standard.value(forKey: "rideTime") as! String)
    }
    func eveningStatusReceived()
    {
        self.lblETA.text = ""
        if (CurrentKidDataFirebase.shared.isBusStatusVisible == true)
        {
            print("already present")
        }
        else
        {
            checkVisibility()
            self.performSegue(withIdentifier: "TrackKidSegue", sender: self)
        }
    }
    func checkVisibility() {
        var notificationReceivedForKid : Int =                     CurrentKidDataFirebase.shared.notificationForKidId as? Int ?? -1
        if(CurrentKidDataFirebase.shared.currentKidInformationApi.value(forKey: "id") as? Int == notificationReceivedForKid)
        {
            
        }
        else
        {
            for i in 0..<kidsArray.count
            {
                var currentLoopKid = kidsArray[i] as! NSDictionary
                if((currentLoopKid.value(forKey: "id") as? Int ?? -1) == notificationReceivedForKid)
                {
                    self.vuCarousel.scrollToItem(at: i, animated: true)
                    break
                }
            }
        }
    }
    func performTrackMyKidSegue() {
        
        checkVisibility()
        var driverId = DriverInformation.shared.driverInformationCurrent.value(forKey: "driver_id") as! Int
        db.collection("ride").document(String(driverId)).getDocument { (docSnapshot, error) in
            if let err = error
            {
                UserDefaults.standard.setValue("", forKey: "rideTime")
                print("Error getting documents: \(err)")
            }
            else
            {
                self.onDataChangeFirebase(snapListen: docSnapshot, driverId: driverId)
                
                var currentKidStatusInternal : Int = CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "status") as? Int ?? -1
                
                print("Segue perform \(currentKidStatusInternal)")
                if(self.checkCurrentKidInfo() == true)
                {
                    if (currentKidStatusInternal != -1 && currentKidStatusInternal != 4)
                    {
                        
                        if (CurrentKidDataFirebase.shared.isBusStatusVisible == true)
                        {
                            print("already present")
                        }
                        else
                        {
                            
                            self.performSegue(withIdentifier: "TrackKidSegue", sender: self)
                        }
                    }
                }
                else
                {
                    
                }
            }
            
        }
        
        
        
    }
    func performReachedKidSegue() {
        
        checkVisibility()
        var driverId = DriverInformation.shared.driverInformationCurrent.value(forKey: "driver_id") as! Int
        db.collection("ride").document(String(driverId)).getDocument { (docSnapshot, error) in
            if let err = error
            {
                UserDefaults.standard.setValue("", forKey: "rideTime")
                print("Error getting documents: \(err)")
            }
            else
            {
                self.onDataChangeFirebase(snapListen: docSnapshot, driverId: driverId)
                
                var currentKidStatusInternal : Int = CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "status") as? Int ?? -1
                
                print("Segue perform \(currentKidStatusInternal)")
                if(self.checkCurrentKidInfo() == true)
                {
                    if (currentKidStatusInternal != -1 && currentKidStatusInternal != 4)
                    {
                        
                        if (CurrentKidDataFirebase.shared.isBusStatusVisible == true)
                        {
                            print("already present")
                        }
                        else
                        {
                            
                            self.performSegue(withIdentifier: "TrackKidSegue", sender: self)
                        }
                    }
                }
                else
                {
                    
                }
            }
            
        }
        if (CurrentKidDataFirebase.shared.isFinalStatusVisible == true)
        {
            print("already present")
        }
        else
        {
            CurrentKidDataFirebase.shared.isFinalStatusVisible = true
            
            self.performSegue(withIdentifier: "SummarizedToConfirmationSegue", sender: self)
        }
        
        
        
    }
    func checkCurrentKidInfo() -> Bool {
        if(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "id") as? Int ?? -1 == CurrentKidDataFirebase.shared.notificationForKidId)
        {
            return true
        }
        else
        {
            var isMatched : Bool = false
            for i in 0..<kidsArray.count
            {
                var studentInfo = kidsArray.object(at: i) as! NSDictionary
                if(studentInfo.value(forKey: "id") as! Int == CurrentKidDataFirebase.shared.notificationForKidId)
                {
                    isMatched = true
                    self.vuCarousel.scrollToItem(at: i, animated: true)
                    break
                }
            }
            if(isMatched == true)
            {
                return true
            }
            else
            {
                return false
            }
            
        }
    }
    func checkStudentExistence(studentsArray : NSArray,driverPosition : GeoPoint,driverId : Int, schoolLatLng : GeoPoint)  {
        var currentObject : NSDictionary = NSDictionary()
        var currentKid : Int = UserDefaults.standard.value(forKey: "selectedKidId") as? Int ?? -1;
        for i in 0..<studentsArray.count
        {
            var currentStundentAtIndex = studentsArray.object(at: i) as! NSDictionary
            if(currentStundentAtIndex.value(forKey: "id") as! Int == currentKid)
            {
                currentObject = studentsArray.object(at: i) as! NSDictionary
                print("Found Item")
                break
            }
        }
        if (currentObject.count != 0)
        {
            // Here check for -1 for undefined status
            if( currentObject.value(forKey: "present") as! Int == -1)
            {
                self.lblStatus.text = ""
                CurrentKidDataFirebase.shared.isPresent = false
                CurrentKidDataFirebase.shared.currentDrivePositionFirebase = driverPosition
                CurrentKidDataFirebase.shared.schoolLatLng = schoolLatLng
                
                CurrentKidDataFirebase.shared.currentKidInformaiton = currentObject
                CurrentKidDataFirebase.shared.driverId = driverId
                self.markPresent()
                populateStatusAccordingly()
                NotificationCenter.default.post(name: Notification.Name(rawValue: "statusChanged"), object: nil, userInfo: nil)
            }
            else if(currentObject.value(forKey: "present") as! Int == 2)
            {
                self.lblStatus.text = "Absent"
                
                CurrentKidDataFirebase.shared.isPresent = false
                CurrentKidDataFirebase.shared.isAbsent = true
                self.markAbsent()
                NotificationCenter.default.post(name: Notification.Name(rawValue: "markedAbsent"), object: nil, userInfo: nil)
            }
            else
            {
                CurrentKidDataFirebase.shared.isPresent = true
                CurrentKidDataFirebase.shared.currentDrivePositionFirebase = driverPosition
                CurrentKidDataFirebase.shared.schoolLatLng = schoolLatLng
                
                CurrentKidDataFirebase.shared.currentKidInformaiton = currentObject
                CurrentKidDataFirebase.shared.driverId = driverId
                
                let ETACheck = Int(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "duration") as? Double ?? 0.0)
                print("***** ETA Value Check ***** \(ETACheck)")
                
                if ETACheck <= 5 {
                
                self.lblETA.text = "ETA : \(Int(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "duration") as? Double ?? 0.0)) min"
                } else {
                    self.lblETA.isHidden = true
                }
                
                self.markPresent()
                populateStatusAccordingly()
                NotificationCenter.default.post(name: Notification.Name(rawValue: "statusChanged"), object: nil, userInfo: nil)
            }
        }
        else
        {
            self.rideNotPresent()
        }
        print(currentObject)
        
    }
    
    
    
    // Mark : FCM Functions
    func subscribeTopic()  {
        var schoolId : String = UserDefaults.standard.value(forKey: "school_id") as? String ?? "-1"
        schoolId =  "school_\(schoolId)"
        Messaging.messaging().subscribe(toTopic: "\(schoolId)") { error in
            print("Subscribed to alerts topic")
        }
    }
    func saveTokenToFirebase() {
        let firebaseToken : String = UserDefaults.standard.value(forKey: "fcmToken") as? String ?? ""
        
        let parentId : String = UserDefaults.standard.value(forKey: "userId") as! String
        self.forMultipleDevices(firebaseToken: firebaseToken, parentId: parentId)
        print("****ParentID****\(parentId)")
        Messaging.messaging().subscribe(toTopic: "parent_\(parentId)") { error in
            print("Subscribed to ParentId topic")
        }
        
    }
    func forMultipleDevices(firebaseToken : String , parentId : String) {
        db.collection("token").document(parentId).collection("tokens").whereField("token", isEqualTo: firebaseToken).getDocuments { (querySnapshot, err) in
            if let err = err
            {
                print("Error getting documents: \(err)")
            }
            else {
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    self.tokenAlreadyExist = true
                    break
                }
                if(self.tokenAlreadyExist == false)
                {
                    self.db.collection("token").document(parentId).collection("tokens").addDocument(data: ["token": firebaseToken,"userId":parentId])
                }
                else
                {
                    
                }
            }
        }
        
    }
    func makePointIndividually(rideTime : String) {
        
        if(rideTime == "")
        {
            return
        }
        var driversCoordinate : CLLocationCoordinate2D = CLLocationCoordinate2D()
        var destinationCoordinate : CLLocationCoordinate2D = CLLocationCoordinate2D()
        
        driversCoordinate = CLLocationCoordinate2D(latitude:CurrentKidDataFirebase.shared.currentDrivePositionFirebase.latitude, longitude: CurrentKidDataFirebase.shared.currentDrivePositionFirebase.longitude)
        if((rideTime == "morning") && (CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "status") as? Int ?? -1 < 3))
        {
            if(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "lat") as? CLLocationDegrees ?? 0.0 == 0.0 || CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "lng") as? CLLocationDegrees ?? 0.0 == 0.0)
            {
                return
            }
            
            destinationCoordinate = CLLocationCoordinate2D(latitude:CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "lat") as! CLLocationDegrees, longitude: CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "lng") as! CLLocationDegrees)
        }
        else if (rideTime == "Evening" || rideTime == "afternoon")
        {
            if(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "lat") as? CLLocationDegrees ?? 0.0 == 0.0 || CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "lng") as? CLLocationDegrees ?? 0.0 == 0.0)
            {
                return
            }
            destinationCoordinate = CLLocationCoordinate2D(latitude:CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "lat") as! CLLocationDegrees, longitude: CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "lng") as! CLLocationDegrees)
        }
        else if ((CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "status") as? Int ?? -1 == 4))
        {
            self.lblETA.text = ""
            return
        }
        else
        {
            
            destinationCoordinate = CLLocationCoordinate2D(latitude:CurrentKidDataFirebase.shared.schoolLatLng.latitude, longitude: CurrentKidDataFirebase.shared.schoolLatLng.longitude)
        }
        
        
        let annotation = MGLPointAnnotation()
        annotation.coordinate = driversCoordinate
        
        let ETACheck = Int(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "duration") as? Double ?? 0.0)
        
        if ETACheck <= 5 {
            
            self.lblETA.text = "ETA : \(Int(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "duration") as? Double ?? 0.0)) min"
        
        }else{
            self.lblETA.isHidden = true
        }
        
        
    }
    
    
    func scaleUIImageToSize( image: UIImage,  size: CGSize) -> UIImage {
        let hasAlpha = false
        let scale: CGFloat = 1.0 // Automatically use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: size))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage!
    }
}
protocol PerformSegueFromTabbarDelegate {
    func performTrackMyKidSegue()
    func eveningStatusReceived()
    func performLocateMeSegue()
    func performReachedKidSegue()
}
protocol LocateMeDelegate {
    func didTapButton()
}
protocol RefreshAlertsAndAnnouncementDelegate {
    func refreshAnnouncementData()
    func refreshAlertData()
}


