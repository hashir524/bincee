//
//  TestingAnimationViewController.swift
//  bincee app
//
//  Created by Apple on 2/7/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Mapbox
import MapboxCoreNavigation
import MapboxNavigation
import MapboxDirections

class TestingAnimationViewController: UIViewController, MGLMapViewDelegate {
    
    var point = MGLPointAnnotation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let mapView = MGLMapView(frame: view.bounds)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.styleURL = MGLStyle.darkStyleURL(withVersion: 9)
        mapView.tintColor = .lightGray
        mapView.centerCoordinate = CLLocationCoordinate2D(latitude: 0, longitude: 66)
        mapView.zoomLevel = 3
        mapView.delegate = self
        view.addSubview(mapView)
        
        let coordinate = CLLocationCoordinate2D(latitude: 0, longitude: 66)
        point.coordinate = coordinate
        point.title = "\(coordinate.latitude), \(coordinate.longitude)"
        
        mapView.addAnnotation(point)
        
        startLocationSimulatorTimer()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopLocationSimulatorTimer()
    }
    
    // MARK: - Random annotation moving methods
    
    var locationSimulatorTimer:Timer?
    
    func startLocationSimulatorTimer() {
        
        self.locationSimulatorTimer?.invalidate()
        self.locationSimulatorTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.moveAnnotationRandomly), userInfo: nil, repeats: true)
        
    }
    
    func stopLocationSimulatorTimer() {
        self.locationSimulatorTimer?.invalidate()
    }
    
    @objc func moveAnnotationRandomly() {
        
        let newRandomCoordinate = getRandomCoordinate(fromCoordinate: point.coordinate, distanceRadius: 20)
        UIView.animate(withDuration: 1.0) {
            self.point.coordinate = newRandomCoordinate
        }
        
    }
    
    func getRandomCoordinate(fromCoordinate: CLLocationCoordinate2D, distanceRadius: Double) -> CLLocationCoordinate2D {
        
        let latitudeDelta = drand48() * distanceRadius
        let longitudeDelta = drand48() * distanceRadius
        
        var sign = 1.0
        if Int(arc4random_uniform(UInt32(2))) == 0 {
            sign = -1.0
        }
        
        var randomCoordinate = CLLocationCoordinate2D()
        randomCoordinate.latitude = fromCoordinate.latitude + (latitudeDelta * sign)
        randomCoordinate.longitude = fromCoordinate.longitude + (longitudeDelta * sign)
        
        return randomCoordinate
        
    }
    
    
    
    // MARK: - MGLMapViewDelegate methods
    
    
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        guard annotation is MGLPointAnnotation else {
            return nil
        }
        
        let reuseIdentifier = "\(annotation.coordinate.longitude)"
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        if annotationView == nil {
            annotationView = CustomAnnotationView(reuseIdentifier: reuseIdentifier)
            annotationView!.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            
            annotationView!.backgroundColor = UIColor.green
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
    
}





