//
//  RealTimeTrackerController.swift
//  bincee app
//
//  Created by Apple on 10/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import Firebase
import MapboxCoreNavigation
import MapboxNavigation
import MapboxDirections

class RealTimeTrackerController: UIViewController,NetworkingDelegate,CLLocationManagerDelegate,MGLMapViewDelegate {
    @IBOutlet weak var vuMapToShowOthers: MGLMapView!
    var mapView: NavigationMapView!
    var directionsRoute: Route?
    @IBOutlet weak var btnSearch: UIButton!
    var locationManager = CLLocationManager()
    @IBOutlet weak var vuImageProfile: UIView!
    var polyline : MGLPolylineFeature = MGLPolylineFeature()
    var timerObject : Timer = Timer()
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblSpeed: UILabel!
    @IBOutlet weak var lblArrivalTime: UILabel!
    @IBOutlet weak var lblKidName: UILabel!
    @IBOutlet weak var vuDistanceTime: UIView!
    
    var driversCoordinate : CLLocationCoordinate2D = CLLocationCoordinate2D()
    var destinationCoordinate : CLLocationCoordinate2D = CLLocationCoordinate2D()
    let annotationDestination = MGLPointAnnotation()
    let annotation = MGLPointAnnotation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        (self.tabBarController as! TabbarController).hideTabbar()
        
        updateViews()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        locationManager.stopUpdatingLocation()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.updateImageAndData()
    }
    override func viewDidAppear(_ animated: Bool) {
        NetworkingFunctions.shared()?.networkingDelegate = self
        //        self.makeChanges()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        timerObject.invalidate()
    }
    func updateViews() -> Void {
        self.vuImageProfile.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0).cgColor
        self.vuImageProfile.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        self.vuImageProfile.layer.shadowOpacity = 0.5
        self.vuImageProfile.layer.shadowRadius = 5.0
        self.vuImageProfile.layer.cornerRadius  = 5.0
        self.vuImageProfile.layer.masksToBounds = false
        self.vuDistanceTime.layer.shadowColor = UIColor.init(red: 34.0/255.0, green: 198.0/255.0, blue: 252.0/255.0, alpha: 1.0).cgColor
        self.vuDistanceTime.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        self.vuDistanceTime.layer.shadowOpacity = 0.5
        self.vuDistanceTime.layer.shadowRadius = 5.0
        self.vuDistanceTime.layer.masksToBounds = false
        self.vuDistanceTime.reloadInputViews()
        
        self.vuImageProfile.layoutIfNeeded()
        self.vuDistanceTime.layoutIfNeeded()
        self.vuMapToShowOthers.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.makeNavigation()
        //        self.makePath()
        //Rounding Corners of Profile Image on Ribbon
        self.roundCountainer(view: self.imgProfile)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        (self.tabBarController as! TabbarController).showTabbar()
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSearchPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "ShowConfirmationPopup", sender: self)
    }
    
    
    func onGetDataReceive(_ data: Any!) {
        print("data here")
    }
    func onFailure(_ failureError: Any!) {
        print("Failure Occured")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        
    }
    
    func updateImageAndData()  {
        //        self.imgProfile.image
        self.lblKidName.text = CurrentKidDataFirebase.shared.currentKidInformationApi.value(forKey: "fullname") as? String ?? ""
        var profileImageUrl : String = CurrentKidDataFirebase.shared.currentKidInformationApi.value(forKey: "photo") as! String
        self.imgProfile.sd_setShowActivityIndicatorView(true)
        
        self.imgProfile.sd_setIndicatorStyle(.gray)
        self.imgProfile.sd_setImage(with: URL.init(string: profileImageUrl), placeholderImage: UIImage.init(named: "Boy-avatar"), options: .continueInBackground, progress: nil, completed: nil)
        timerObject = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(timerFireFunction), userInfo: nil, repeats: true)
        
    }
    @objc func timerFireFunction()
    {
        self.makePointIndividually()
        self.animateAnnotations()
        
        
    }
    
    //Function to round Button or any View Corners
    func roundCountainer(view : UIView) {
        //Making a CardView
        view.layer.cornerRadius = 20.0
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 12.0
        view.layer.shadowOpacity = 0.7
    }
    
    
    // MARK : Mapbox Functions
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        
        self.makePointIndividually()
        self.makeAnnotation()
        
    }
    func makeNavigation() {
        
        
        self.vuMapToShowOthers.delegate = self
        
        self.vuMapToShowOthers.showsUserLocation = false
        self.vuMapToShowOthers.setUserTrackingMode(.none, animated: true)
        self.view.bringSubview(toFront: self.imgProfile)
        
        
        
        
    }
    func calculateRoute(from origin: CLLocationCoordinate2D,
                        to destination: CLLocationCoordinate2D,
                        completion: @escaping (Route?, Error?) -> ()) {
        
        // Coordinate accuracy is the maximum distance away from the waypoint that the route may still be considered viable, measured in meters. Negative values indicate that a indefinite number of meters away from the route and still be considered viable.
        let origin = Waypoint(coordinate: origin, coordinateAccuracy: -1, name: "Start")
        let destination = Waypoint(coordinate: destination, coordinateAccuracy: -1, name: "Finish")
        
        // Specify that the route is intended for automobiles avoiding traffic
        let options = NavigationRouteOptions(waypoints: [origin, destination], profileIdentifier: .automobile)
        
        // Generate the route object and draw it on the map
        _ = Directions.shared.calculate(options) { [unowned self] (waypoints, routes, error) in
            self.directionsRoute = routes?.first
            print("Route distance in Real Time Tracking")
            
            var timeInterval : Double = routes?.first?.expectedTravelTime as? Double ?? 0.0
            
            if(timeInterval != 0.0)
            {
                timeInterval = timeInterval / 60
                print("Estimated Time \(String(describing: timeInterval))")
                CurrentKidDataFirebase.shared.estimatedEta = String(Int(timeInterval))
            }
            let ETACheck = Int(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "duration") as? Double ?? 0.0)
            if ETACheck <= 5 {
                
                self.lblArrivalTime.text = "ETA : \(Int(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "duration") as? Double ?? 0.0)) min"
                
                //Marker tooltip Values..
                self.annotationDestination.title = " ETA "
                self.annotationDestination.subtitle = "\(Int(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "duration") as? Double ?? 0.0)) min"
        
            }else {
                 self.lblArrivalTime.isHidden = true
            }
            
            
            
            
            
            // Draw the route on the map after creating it
            self.drawRoute(route: self.directionsRoute!)
            if (self.vuMapToShowOthers.selectedAnnotations.count == 0)
            {
                self.vuMapToShowOthers.selectAnnotation(self.annotationDestination, animated: true)
            }
            else{
                self.vuMapToShowOthers.deselectAnnotation(self.annotationDestination, animated: true)
                self.vuMapToShowOthers.selectAnnotation(self.annotationDestination, animated: true)
            }
            
            
        }
    }
    func drawRoute(route: Route) {
        guard route.coordinateCount > 0 else { return }
        // Convert the route’s coordinates into a polyline
        
        var routeCoordinates = route.coordinates!
        polyline = MGLPolylineFeature(coordinates: &routeCoordinates, count: route.coordinateCount)
        
        // If there's already a route line on the map, reset its shape to the new route
        if let source = self.vuMapToShowOthers.style?.source(withIdentifier: "route-source") as? MGLShapeSource {
            
            
            source.shape = polyline
        } else {
            let source = MGLShapeSource(identifier: "route-source", features: [polyline], options: nil)
            
            // Customize the route line color and width
            let lineStyle = MGLLineStyleLayer(identifier: "route-style", source: source)
            lineStyle.lineColor = NSExpression(forConstantValue: #colorLiteral(red: 0.1333333333, green: 0.7764705882, blue: 0.9803921569, alpha: 1))
            lineStyle.lineWidth = NSExpression(forConstantValue: 4)
            lineStyle.lineDashPattern = NSExpression(forConstantValue: [1, 1.0])
            
            // Add the source and style layer of the route line to the map
            
            
            self.vuMapToShowOthers.style?.addSource(source)
            self.vuMapToShowOthers.style?.addLayer(lineStyle)
            
            
            
            
        }
        
        
    }
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        // This example is only concerned with point annotations.
        guard annotation is MGLPointAnnotation else {
            return nil
        }
        
        // Use the point annotation’s longitude value (as a string) as the reuse identifier for its view.
        
        let reuseIdentifier = "\(annotation.coordinate.longitude)"
        
        // For better performance, always try to reuse existing annotations.
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        // If there’s no reusable annotation view available, initialize a new one.
        if annotationView == nil {
            annotationView =  CustomAnnotationView(reuseIdentifier: reuseIdentifier)
            annotationView!.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            
            annotationView!.backgroundColor = UIColor.clear
            let image = UIImageView(frame: (annotationView?.frame)!)
            //            image.image = UIImage(named: "Bus-icon")!
            image.tag = 2001
            image.contentMode = .scaleAspectFill
            annotationView?.addSubview(image)
            annotationView?.clipsToBounds = false
            image.clipsToBounds = false
            annotationView!.layer.borderWidth = 0.0
        }
        
        if annotation.title == "Driver"{
            let image = annotationView?.viewWithTag(2001) as! UIImageView
            image.image = UIImage(named: "Bus-icon")!
            
        }
            
        else{
            let image = annotationView?.viewWithTag(2001) as! UIImageView
            image.image =  UIImage(named: "Map-icon")!
        }
        
        return annotationView
    }
    func makePointIndividually() {
        
        //        annotationDestination.title = "Destination"
        var rideTime : String = UserDefaults.standard.value(forKey: "rideTime") as! String
        
        driversCoordinate = CLLocationCoordinate2D(latitude:CurrentKidDataFirebase.shared.currentDrivePositionFirebase.latitude, longitude: CurrentKidDataFirebase.shared.currentDrivePositionFirebase.longitude)
        if(rideTime == "morning")
        {
            var dummyDIc = CurrentKidDataFirebase.shared.currentKidInformaiton
            
            if(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "status") as? Int ?? -1 < 3)
            {
                if(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "lat") as? CLLocationDegrees ?? 0.0 == 0.0 || CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "lng") as? CLLocationDegrees ?? 0.0 == 0.0)
                {
                    return
                }
                destinationCoordinate = CLLocationCoordinate2D(latitude:CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "lat") as! CLLocationDegrees, longitude: CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "lng") as! CLLocationDegrees)
                
            }
            else{
                
                destinationCoordinate = CLLocationCoordinate2D(latitude:CurrentKidDataFirebase.shared.schoolLatLng.latitude, longitude: CurrentKidDataFirebase.shared.schoolLatLng.longitude)
                
                
                
                
            }
        }
        else if (rideTime == "Evening" || rideTime == "afternoon")
        {
            if(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "lat") as? CLLocationDegrees ?? 0.0 == 0.0 || CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "lng") as? CLLocationDegrees ?? 0.0 == 0.0)
            {
                return
            }
            destinationCoordinate = CLLocationCoordinate2D(latitude:CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "lat") as! CLLocationDegrees, longitude: CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "lng") as! CLLocationDegrees)
        }
        else
        {
            
            destinationCoordinate = CLLocationCoordinate2D(latitude:CurrentKidDataFirebase.shared.schoolLatLng.latitude, longitude: CurrentKidDataFirebase.shared.schoolLatLng.longitude)
        }
        
        
        
        annotation.title = "Driver"
        
        
        
        // Calculate the route from the user's location to the set destination
        
        
        
    }
    @objc func animateAnnotations() {
        if(self.annotationDestination.coordinate != self.annotation.coordinate && self.annotation.coordinate != driversCoordinate)
        {
            
            
            
            
            
            
            calculateRoute(from: (destinationCoordinate), to: self.driversCoordinate) { (route, error) in
                if error != nil {
                    print("Error calculating route")
                }
                else
                {
                    print("Route distance \(String(describing: route?.distance))")
                    
                    
                    
                }
            }
            UIView.animate(withDuration: 3) {
                self.annotation.coordinate = self.driversCoordinate
            }
            
            
        }
        self.vuMapToShowOthers.selectAnnotation(annotationDestination, animated: true)
    }
    func makeAnnotation() {
        
        annotation.coordinate = driversCoordinate
        
        
        self.vuMapToShowOthers.addAnnotation(annotation)
        
        annotationDestination.coordinate = destinationCoordinate
        
        self.vuMapToShowOthers.addAnnotation(annotationDestination)
        self.vuMapToShowOthers.showAnnotations([annotation,annotationDestination], animated: true)
        calculateRoute(from: (destinationCoordinate), to: annotation.coordinate) { (route, error) in
            if error != nil {
                print("Error calculating route")
            }
            else
            {
                print("Route distance \(String(describing: route?.distance))")
                
            }
        }
    }
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
    /* Mapview custom markers */
    
    func mapView(_ mapView: MGLMapView, imageFor annotation: MGLAnnotation) -> MGLAnnotationImage? {
        // Try to reuse the existing ‘pisa’ annotation image, if it exists.
        var annotationImage = mapView.dequeueReusableAnnotationImage(withIdentifier: "pisa")
        if(annotation.title == "Driver")
        {
            if annotationImage == nil {
                // Leaning Tower of Pisa by Stefan Spieler from the Noun Project.
                var image = UIImage(named: "Bus-icon")!
                
                // The anchor point of an annotation is currently always the center. To
                // shift the anchor point to the bottom of the annotation, the image
                // asset includes transparent bottom padding equal to the original image
                // height.
                //
                // To make this padding non-interactive, we create another image object
                // with a custom alignment rect that excludes the padding.
                image = image.withAlignmentRectInsets(UIEdgeInsets(top: 0, left: 0, bottom: image.size.height/2, right: 0))
                
                // Initialize the ‘pisa’ annotation image with the UIImage we just loaded.
                annotationImage = MGLAnnotationImage(image: image, reuseIdentifier: "Bus-icon@2x")
            }
            
            
            return annotationImage
        }
        else
        {
            return annotationImage
        }
        
        
        
        
    }
    
    // Present the navigation view controller when the callout is selected
    func mapView(_ mapView: MGLMapView, tapOnCalloutFor annotation: MGLAnnotation) {
        
    }
}
class CustomAnnotationView: MGLAnnotationView {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        scalesWithViewingDistance = false
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
}


