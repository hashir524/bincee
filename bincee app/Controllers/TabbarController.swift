//
//  TabbarController.swift
//  bincee app
//
//  Created by Ali on 10/10/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class TabbarController: UITabBarController {
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    var activityIndicator : NVActivityIndicatorView!
    
    
    
    
    var allTabButtons = NSMutableArray()
    var allTabLabels = NSMutableArray()
    var vuBottomTabbar = UIView()
    var segueDelegate: PerformSegueFromTabbarDelegate?
    var refreshAlertScreenDelegate: RefreshAlertsAndAnnouncementDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if(self.revealViewController() != nil)
        {
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        self.tabBar.isHidden = true
        
        // Activity Indicator loader
        activityIndicator = NVActivityIndicatorView(frame: CGRect.init(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: 100, height: 100))
        activityIndicator.type = . ballScaleRippleMultiple // add your type
        activityIndicator.color = UIColor(red: 90 / 255, green: 198 / 255, blue: 243 / 255, alpha: 1) // add your color
        activityIndicator.center = self.view.center
        self.CreateCustomTabbar()
        
        selectCustomTab((allTabButtons.object(at: 0) as! UIButton))
        
        NotificationCenter.default.addObserver(self, selector: #selector(launchLocateMeScreen), name: NSNotification.Name(rawValue: "launchLocateMeScreen"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(launchTrackMyKid), name: NSNotification.Name(rawValue: "launchTrackMyKid"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(launchOneTimeEvening), name: NSNotification.Name(rawValue: "EveningStatusReceived"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(launchReachedScreen), name: NSNotification.Name(rawValue: "launchReachedScreen"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(launchAnnouncement), name: NSNotification.Name(rawValue: "AnnouncementReceived"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(launchAlert), name: NSNotification.Name(rawValue: "AlertReceived"), object: nil)
        
    }
    @objc func launchLocateMeScreen()
    {
        self.selectCustomTabWithIndex(0)
        segueDelegate?.performLocateMeSegue()
        print("check locate me")
    }
    @objc func launchReachedScreen()
    {
        self.selectCustomTabWithIndex(0)
        segueDelegate?.performReachedKidSegue()
        print("Reached kids")
    }
    @objc func launchTrackMyKid()
    {
        self.selectCustomTabWithIndex(0)
        segueDelegate?.performTrackMyKidSegue()
        print("check launch my kid")
    }
    @objc func launchOneTimeEvening()
    {
        self.selectCustomTabWithIndex(0)
        segueDelegate?.eveningStatusReceived()
    }
    @objc func launchAnnouncement()
    {
        self.selectCustomTabWithIndex(2)
        refreshAlertScreenDelegate?.refreshAnnouncementData()
    }
    @objc func launchAlert()
    {
        self.selectCustomTabWithIndex(2)
        refreshAlertScreenDelegate?.refreshAlertData()
    }
    func CreateCustomTabbar()
    {
        
        
        if(screenHeight >= 812)
        {
            vuBottomTabbar = UIView.init(frame: CGRect.init(x: 0, y: self.view.frame.size.height - 80, width: self.view.frame.size.width, height: 80))
        }
        else
        {
            vuBottomTabbar = UIView.init(frame: CGRect.init(x: 0, y: self.view.frame.size.height - 60, width: self.view.frame.size.width, height: 60))
        }
        vuBottomTabbar.backgroundColor = UIColor.white
        vuBottomTabbar.layer.shadowColor = UIColor.black.cgColor
        vuBottomTabbar.layer.shadowRadius = 20.0
        vuBottomTabbar.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        vuBottomTabbar.layer.masksToBounds = false
        vuBottomTabbar.layer.shadowOpacity = 1.0
        vuBottomTabbar.layoutIfNeeded()
        self.view.addSubview(vuBottomTabbar)
        for i in 0..<3
        {
            let btn = UIButton.init(type: UIButtonType.custom)
            var lbl = UILabel()
            if(screenHeight >= 812)
            {
                btn.frame = CGRect.init(x: (Int(self.view.frame.size.width/3) * i), y: 0, width: Int(self.view.frame.size.width/3), height: 60)
                lbl =  UILabel.init(frame: CGRect.init(x: btn.frame.origin.x, y: 45, width: btn.frame.size.width, height: 10))
            }
            else
            {
                btn.frame = CGRect.init(x: (Int(self.view.frame.size.width/3) * i), y: 0, width: Int(self.view.frame.size.width/3), height: 60)
                lbl = UILabel.init(frame: CGRect.init(x: btn.frame.origin.x, y: 45, width: btn.frame.size.width, height: 8))
            }
            
            lbl.font = UIFont.systemFont(ofSize: 8);
            lbl.textColor = UIColor.lightGray
            lbl.textAlignment = NSTextAlignment.center
            
            btn.tag = i
            if i == 0{
                btn.setImage(UIImage.init(named: "Status-uncheck"), for: UIControlState.normal)
                btn.setImage(UIImage.init(named: "Status-checked"), for: UIControlState.selected)
                lbl.text = "Summarized Status"
            }
            else if(i == 1)
            {
                btn.setImage(UIImage.init(named: "Caledar-uncheck"), for: UIControlState.normal)
                btn.setImage(UIImage.init(named: "Calendar-selected-icon"), for: UIControlState.selected)
                lbl.text = "Leaves"
            }
            else
            {
                btn.setImage(UIImage.init(named: "Announcement-unchecked"), for: UIControlState.normal)
                btn.setImage(UIImage.init(named: "Announcement-checked"), for: UIControlState.selected)
                lbl.text = "Announcement/Alerts"
            }
            
            btn.addTarget(self, action: #selector(selectCustomTab(_:)), for: UIControlEvents.touchUpInside)
            allTabButtons.add(btn)
            allTabLabels.add(lbl)
            vuBottomTabbar.addSubview(lbl)
            vuBottomTabbar.addSubview(btn)
            
        }
    }
    
    func deselectAllTabs()
    {
        for i in 0..<allTabButtons.count
        {
            (allTabButtons.object(at: i) as! UIButton).isSelected = false
            
            (allTabLabels.object(at: i) as! UILabel).textColor = UIColor.lightGray
        }
    }
    
    
    @objc func selectCustomTab(_ btn : UIButton)
    {
        self.deselectAllTabs()
        
        self.selectedIndex = btn.tag
        
        btn.isSelected = true
        
        (allTabLabels.object(at: btn.tag) as! UILabel).textColor = UIColor.black
    }
    
    func selectCustomTabWithIndex(_ btnTag : Int)
    {
        NetworkingFunctions.shared()?.cancelTask()
        self.deselectAllTabs()
        
        self.selectedIndex = btnTag
        
        (allTabButtons.object(at: btnTag) as! UIButton).isSelected = true
        
        (allTabLabels.object(at: btnTag) as! UILabel).textColor = UIColor.black
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func hideTabbar(){
        self.vuBottomTabbar.isHidden = true
    }
    func showTabbar(){
        self.vuBottomTabbar.isHidden = false
    }
    func showLoader()  {
        if(activityIndicator.isAnimating == false)
        {
            self.view.addSubview(activityIndicator) // or use  webView.addSubview(activityIndicator)
            activityIndicator.startAnimating()
        }
        //       loadingNotification.show(animated: true)
    }
    func hideLoader()  {
        if(activityIndicator.isAnimating == true)
        {
            activityIndicator.stopAnimating()
            self.activityIndicator.removeFromSuperview()
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

