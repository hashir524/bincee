//
//  CalendarAlertContoller.swift
//  bincee app
//
//  Created by Apple on 10/8/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import FSCalendar

class CalendarAlertContoller: UIViewController,ClickEventDelegate ,UITableViewDataSource,UITableViewDelegate,FSCalendarDelegate,FSCalendarDataSource,NetworkingDelegate,UITextViewDelegate, FSCalendarDelegateAppearance{
    var leavesArray : NSArray = NSArray()
    private var datesRange: [Date]?
    private var lastDate: Date?
    private var firstDate: Date?
    @IBOutlet weak var btnBackHistory: UIButton!
    @IBOutlet weak var vuParent: UIView!
    var clickDelegate : ClickEventDelegate!
    @IBOutlet weak var fsCalendarView: FSCalendar!
    @IBOutlet weak var vuContentAlert: UIView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet var vuLeaveHistory: UIView!
    @IBOutlet var vuLeaveApplication: UIView!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var txtComments: UITextView!
    @IBOutlet weak var chkMorningShift: UIButton!
    @IBOutlet weak var vuCalendar: UIView!
    @IBOutlet weak var btnHistory: UIButton!
    @IBOutlet weak var btnCalendar: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var chkAfternoonShift: UIButton!
    @IBOutlet weak var tableViewHistory: UITableView!
    @IBOutlet weak var btnClose: NSLayoutConstraint!
    var paramsForCalendarApi : NSMutableDictionary = NSMutableDictionary()
    @IBOutlet weak var btnOk: UIButton!
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    func removeTabbar() {
        
        (self.tabBarController as! TabbarController).selectCustomTabWithIndex(0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NetworkingFunctions.shared().networkingDelegate = self
        if(btnHistory.isSelected == true)
        {
            var selectedKidId = UserDefaults.standard.value(forKey: "selectedKidId") as! NSNumber
            var previousId = UserDefaults.standard.value(forKey: "apiCalledForId") as! NSNumber
            if(selectedKidId != previousId)
            {
                callApiToGetHistory()
            }
            
        }
    }
    @IBAction func btnBackHistoryPressed(_ sender: Any) {
        //here hide the view for history
        self.vuLeaveApplication.isHidden = true
        self.vuCalendar.isHidden = false
        self.btnBackHistory.isHidden = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fsCalendarView.delegate = self
        fsCalendarView.dataSource = self
        self.revealViewController().rearViewRevealWidth = self.view.frame.width-85;
        self.vuContentAlert.layer.cornerRadius = 20.0
        self.vuContentAlert.clipsToBounds = true
        
        self.vuContentAlert.layer.shadowColor = UIColor.lightGray.cgColor
        self.vuContentAlert.layer.shadowRadius = 10.0
        self.vuContentAlert.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        self.vuContentAlert.layer.masksToBounds = false
        self.vuContentAlert.layer.shadowOpacity = 1.0
        self.vuContentAlert.layoutIfNeeded()
        
        self.vuContentAlert.layoutIfNeeded()
        if self.revealViewController() != nil {
            self.btnMenu.addTarget(self.revealViewController(), action:  #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            
            
        }
        makeCornerRadius()
        if(screenHeight <= 568)
        {
            self.vuParent.transform = CGAffineTransform(scaleX:0.7,y: 0.7)
        }
        else if((screenHeight <= 667))
        {
            self.vuParent.transform = CGAffineTransform(scaleX:0.85,y: 0.85)
        }
        else if(screenHeight == 812)
        {
            self.vuParent.transform = CGAffineTransform(scaleX:0.85,y: 0.85)
        }
        else
        {
            self.vuParent.transform = CGAffineTransform(scaleX: 1,y: 1)
        }
        self.txtComments.delegate = self
        
        //        fsCalendar.locale = NSLocale(localeIdentifier: "fa_IR") as Locale
        fsCalendarView.locale = NSLocale.init(localeIdentifier: "Asia/Baghdad") as Locale
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    @IBAction func btnOkPressed(_ sender: Any) {
        
        (self.tabBarController as! TabbarController).selectCustomTabWithIndex(0)
        
    }
    func makeCornerRadius() -> Void {
        self.btnCalendar.layer.cornerRadius = self.btnCalendar.frame.height/2;
        self.btnDone.layer.cornerRadius = self.btnDone.frame.height/2;
        btnDone.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0).cgColor
        btnDone.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        btnDone.layer.shadowOpacity = 0.25
        btnDone.layer.shadowRadius = 5.0
        
        self.btnContinue.layer.cornerRadius = self.btnContinue.frame.height/2;
        
        btnContinue.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0).cgColor
        btnContinue.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        btnContinue.layer.shadowOpacity = 0.25
        btnContinue.layer.shadowRadius = 5.0
        
        
        
        self.btnHistory.layer.cornerRadius = self.btnHistory.frame.height/2;
        self.btnOk.layer.cornerRadius = self.btnOk.frame.height/2;
        btnOk.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0).cgColor
        btnOk.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        btnOk.layer.shadowOpacity = 0.25
        btnOk.layer.shadowRadius = 5.0
        
        vuContentAlert.layer.cornerRadius = 3
        vuLeaveApplication.isHidden = true
        self.btnBackHistory.isHidden = true
        self.tableViewHistory.dataSource = self
        self.tableViewHistory.delegate = self
        self.vuCalendar.isHidden = false
        self.vuLeaveHistory.isHidden = true;
        fsCalendarView.allowsMultipleSelection = true
        self.fsCalendarView.bottomBorder.isHidden = true
        fsCalendarView.today = nil
        fsCalendarView.appearance.titleFont = UIFont.init(name: "GothamBook", size: 17)
        self.vuContentAlert.clipsToBounds = true
        self.vuContentAlert.layer.cornerRadius = 5
        self.vuContentAlert.layer.masksToBounds = true
        
        
        self.txtComments.layer.cornerRadius = 5
        self.txtComments.layer.shadowColor = UIColor.lightGray.cgColor
        self.txtComments.layer.shadowRadius = 5.0
        self.txtComments.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        self.txtComments.layer.masksToBounds = false
        self.txtComments.layer.shadowOpacity = 1.0
        self.txtComments.layoutIfNeeded()
        self.btnCalendarPressed(self.btnCalendar)
        
        
        
    }
    @IBAction func btnCalenderDonePressed(_ sender: Any) {
        
    }
    @IBAction func btnClosePressed(_ sender: Any) {
        
        (self.tabBarController as! TabbarController).selectCustomTabWithIndex(0)
    }
    
    @IBAction func btnCalendarPressed(_ sender: Any) {
        btnCalendar.isSelected = !btnCalendar.isSelected
        if btnCalendar.isSelected
        {
            self.vuCalendar.isHidden = false
            self.vuLeaveHistory.isHidden = true;
            btnHistory.isSelected = false
            btnCalendar.titleLabel?.textColor = .white
            btnCalendar.backgroundColor = UIColor(red: 3.0/255, green: 199.0/255, blue: 255.0/255.0, alpha: 1)
            btnHistory.titleLabel?.textColor = .black
            
            btnHistory.backgroundColor = .clear
        }
    }
    @IBAction func btnHistoryPressed(_ sender: Any) {
        btnHistory.isSelected = !btnHistory.isSelected
        if btnHistory.isSelected
        {
            self.txtComments.endEditing(true)
            self.vuCalendar.isHidden = true
            self.vuLeaveHistory.isHidden = false;
            self.vuLeaveApplication.isHidden = true
            self.btnBackHistory.isHidden = true
            btnCalendar.isSelected = false
            
            self.btnCalendar.titleLabel?.textColor = UIColor.black
            btnHistory.titleLabel?.textColor = .white
            btnHistory.backgroundColor = UIColor(red: 3.0/255, green: 199.0/255, blue: 255.0/255.0, alpha: 1)
            btnCalendar.titleLabel?.textColor = .black
            btnCalendar.backgroundColor = .clear
            callApiToGetHistory()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leavesArray.count;
    }
    
    @IBAction func btnContinuePressed(_ sender: Any) {
        
        var timeTo : Double = 0.0
        var timeFrom : Double = 0.0
        if(firstDate == nil && lastDate == nil)
        {
            print("No date selected")
        }
        else
        {
            
            timeFrom = (datesRange?.first?.timeIntervalSince1970)!
            timeTo = (datesRange?.last?.timeIntervalSince1970)!
            //           params.setValue("102", forKey: "akjdskfjds")
            paramsForCalendarApi.setValue(Int(timeFrom), forKey: "from_date")
            paramsForCalendarApi.setValue(Int(timeTo), forKey: "to_date")
            
            print("dates selected else")
            vuLeaveApplication.isHidden = false
            self.btnBackHistory.isHidden = false
            vuCalendar.isHidden = true
            
        }
        
        
    }
    
    @IBAction func btnCalendarDonePressed(_ sender: Any) {
        self.txtComments.endEditing(true)
        if(txtComments.text.count == 0)
        {
            self.view.showToast(toastMessage: "Comment cannot be empty", duration: 3)
            print("Please enter some text")
            return
        }
        var dumcur = CurrentKidDataFirebase.shared.currentKidInformationApi
        //        var shiftInfo = CurrentKidDataFirebase.shared.currentKidInformationApi.value(forKey: "shift") as! NSDictionary
        var school_id = UserDefaults.standard.value(forKey: "school_id") as? String ?? ""
        //            shiftInfo.value(forKey: "school_id") as! Int
        paramsForCalendarApi.setValue(school_id, forKey: "school_id")
        paramsForCalendarApi.setValue(txtComments.text, forKey: "comment")
        paramsForCalendarApi.setValue(CurrentKidDataFirebase.shared.currentKidInformationApi.value(forKey: "id") as! Int, forKey: "student_id")
        print("sschosdjf")
        
        callApiToPostLeave()
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var currentChildLeaves = leavesArray.object(at: indexPath.row) as! NSDictionary
        self.tableViewHistory.register(UINib(nibName: "LeaveHistoryCell", bundle: nil), forCellReuseIdentifier: "LeaveHistoryCell")
        let cell = tableViewHistory.dequeueReusableCell(withIdentifier: "LeaveHistoryCell", for: indexPath) as! LeaveHistoryCell
       
        
        if(changeServerToDateOnly(serverDate: currentChildLeaves.value(forKey: "to_date") as! String) == changeServerDate(serverDate: currentChildLeaves.value(forKey: "from_date") as! String))
        {
            cell.lblTitle.text = changeServerToDateOnly(serverDate: currentChildLeaves.value(forKey: "to_date") as! String)
        }
        else
        {
            var toDate = changeServerDateToShowMultiple(serverDate: changeServerToDateOnly(serverDate: currentChildLeaves.value(forKey: "to_date") as! String))
            var fromDate = changeServerDateToShowMultiple(serverDate: changeServerDate(serverDate: currentChildLeaves.value(forKey: "from_date") as! String))
            cell.lblTitle.text = ("\(fromDate) - \(toDate) , \(self.getOnlyYear(serverDate: changeServerToDateOnly(serverDate: currentChildLeaves.value(forKey: "to_date") as! String)))")
        }
        
        cell.lblApplicationDescription.text = currentChildLeaves.value(forKey: "comment") as? String
        //            }
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    

    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        
        let curDate = Date().addingTimeInterval(-24*60*60)
        
        
        
        if date < curDate {
            
            return false
        } else {
            return true
        }
    }
    
    func calendar(calendar: FSCalendar!, appearance: FSCalendarAppearance!, titleDefaultColorForDate date: NSDate!) -> UIColor!
    {
        let curDate = Date().addingTimeInterval(-24*60*60)
        
        if (date as Date?)! < curDate {
            return UIColor.gray
        }else{
            return UIColor.red
        }
        
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // nothing selected:
        if firstDate == nil {
            
            firstDate = date
            datesRange = [firstDate!]
            
            print("datesRange contains: \(datesRange!)")
            
            return
        }
        
        // only first date is selected:
        if firstDate != nil && lastDate == nil {
            // handle the case of if the last date is less than the first date:
            if date <= firstDate! {
                calendar.deselect(firstDate!)
                firstDate = date
                datesRange = [firstDate!]
                
                print("datesRange contains: \(datesRange!)")
                
                return
            }
            
            let range = datesRange(from: firstDate!, to: date)
            
            lastDate = range.last
            
            for d in range {
                calendar.select(d)
            }
            
            datesRange = range
            
            print("datesRange contains: \(datesRange!)")
            
            return
        }
        
        // both are selected:
        if firstDate != nil && lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            
            lastDate = nil
            firstDate = nil
            
            datesRange = []
            
            print("datesRange contains: \(datesRange!)")
        }
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // both are selected:
        
        // NOTE: the is a REDUANDENT CODE:
        if firstDate != nil && lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            
            lastDate = nil
            firstDate = nil
            
            datesRange = []
            print("datesRange contains: \(datesRange!)")
        }
    }
    func datesRange(from: Date, to: Date) -> [Date] {
        // in case of the "from" date is more than "to" date,
        // it should returns an empty array:
        if from > to { return [Date]() }
        
        var tempDate = from
        var array = [tempDate]
        
        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }
        
        return array
    }
    @IBAction func btnMorningShiftClicked(_ sender: Any) {
        self.chkMorningShift.isSelected = !self.chkMorningShift.isSelected
    }
    @IBAction func btnAfternoonShiftClicked(_ sender: Any) {
        self.chkAfternoonShift.isSelected = !self.chkAfternoonShift.isSelected
        
    }
    func callApiForApplyingLeave()  {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.locale = NSLocale.init(localeIdentifier: "Asia/Baghdad") as Locale
        
        
        
        print("Converted Date")
        if(firstDate != nil)
        {
            let convertedDateFirst = dateFormatter.string(from: firstDate!)
        }
        if(lastDate != nil)
        {
            let convertedDateLast = dateFormatter.string(from: lastDate!)
        }
    }
    
    func callApiToGetHistory() {
        
        var selectedKidId : NSNumber = UserDefaults.standard.value(forKey: "selectedKidId") as! NSNumber
        UserDefaults.standard.setValue(selectedKidId, forKey: "apiCalledForId")
       
        (self.tabBarController as! TabbarController).showLoader()
        NetworkingFunctions.shared()?.getJsonData("http://access.bincee.com/school/student/leaves/\(selectedKidId)", withParams: nil, withBearer: true)
    }
    func callApiToPostLeave() {
        
        
        (self.tabBarController as! TabbarController).showLoader()
        var params : [String : Any] = ["from_date" :  paramsForCalendarApi.value(forKey: "from_date")!, "to_date" : paramsForCalendarApi.value(forKey: "to_date")!, "school_id" : paramsForCalendarApi.value(forKey: "school_id")!, "student_id" : paramsForCalendarApi.value(forKey: "student_id")!, "comment" : txtComments.text];
        NetworkingFunctions.shared()?.postJSON(toURL: "http://access.bincee.com/school/leave/create", withParams: params, withBearer: true)
    }
    func onPostDataReceive(_ data: Any!) {
        //        loadingNotification.hide(animated: true)
        (self.tabBarController as! TabbarController).hideLoader()
        var dataConverted = data as! NSDictionary
        print("Value for key \(String(describing: dataConverted.value(forKey: "username")))")
        
        if(dataConverted.value(forKey: "status") as! NSNumber == 200)
        {
            for singleDate in fsCalendarView.selectedDates
            {
                fsCalendarView.deselect(singleDate)
            }
            datesRange = []
            self.txtComments.text = ""
            self.chkMorningShift.isSelected = false
            self.chkAfternoonShift.isSelected = false
            self.btnHistoryPressed(self.btnHistory)
        }
        else
        {
            
        }
    }
    func onGetDataReceive(_ data: Any!) {
        (self.tabBarController as! TabbarController).hideLoader()
        var dataConverted = data as! NSDictionary
        if(dataConverted.value(forKey: "status") as! NSNumber == 200)
        {
            if(dataConverted.value(forKey: "data") as? NSArray ?? [] == [])
            {
                return
            }
            leavesArray = dataConverted.value(forKey: "data") as! NSArray
            print("Data converted count \(dataConverted.count)")
        }
        else{
            
        }
        self.tableViewHistory.reloadData()
        print("data received")
    }
    func onFailure(_ failureError: Any!) {
        //        loadingNotification.hide(animated: true)
        (self.tabBarController as! TabbarController).hideLoader()
        print("Error Occured")
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func changeServerDate(serverDate : String) -> String {
        if(serverDate == nil)
        {
            return ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "Asia/Baghdad")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: serverDate)
        
        print("date: \(date)")
        dateFormatter.locale = NSLocale.init(localeIdentifier: "Asia/Baghdad") as Locale
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        
        return convertToString(date: date!)
    }
    func changeServerToDateOnly(serverDate : String) -> String {
        if(serverDate == nil)
        {
            return ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "Asia/Baghdad")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: serverDate)!.addingTimeInterval(-1*24*60*60)
        print("date: \(date)")
        dateFormatter.locale = NSLocale.init(localeIdentifier: "Asia/Baghdad") as Locale
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        
        return convertToString(date: date)
    }
    func changeServerDateToShowMultiple(serverDate : String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "Asia/Baghdad")
        dateFormatter.dateFormat = "MMM dd, yyyy"
        let date = dateFormatter.date(from: serverDate)
        print("date: \(date)")
        dateFormatter.dateFormat = "MM-dd"
        
        
        return convertToStringMultiple(date: date!)
    }
    func getOnlyYear(serverDate : String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "Asia/Baghdad")
        dateFormatter.dateFormat = "MMM dd, yyyy"
        let date = dateFormatter.date(from: serverDate)
        print("date: \(date)")
        dateFormatter.dateFormat = "yyyy"
        
        
        return convertToStringYear(date: date!)
    }
    
    func convertToString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"// Your New Date format as per requirement change it own
        
        let newDate: String = dateFormatter.string(from: date) // pass Date here
        print(newDate) // New formatted Date string
        
        return newDate
    }
    func convertToStringMultiple(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd"// Your New Date format as per requirement change it own
        
        let newDate: String = dateFormatter.string(from: date) // pass Date here
        print(newDate) // New formatted Date string
        
        return newDate
    }
    func convertToStringYear(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"// Your New Date format as per requirement change it own
        
        let newDate: String = dateFormatter.string(from: date) // pass Date here
        print(newDate) // New formatted Date string
        
        return newDate
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView == self.txtComments)
        {
            UIView.animate(withDuration: 0.5) {
                if(self.screenHeight == 812)
                {
                    //height of iphone xs
                    self.vuParent.frame.origin.y -= 100
                }
                else
                {
                    self.vuParent.frame.origin.y -= 40
                }
            }
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView == self.txtComments)
        {
            UIView.animate(withDuration: 0.5) {
                if(self.screenHeight == 812)
                {
                    //height of iphone xr
                    self.vuParent.frame.origin.y += 100
                }
                else
                {
                    self.vuParent.frame.origin.y += 40
                }
            }
            
        }
    }
    func convertDateForComparison(sentDate :Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let result = formatter.string(from: sentDate)
        return result
        
    }
    
    func convertToCurrentLocale(dateToConvert : Date) {
        
    }
    
}
protocol ClickEventDelegate {
    
    func removeTabbar()
}

