//
//  LoginController.swift
//  bincee app
//
//  Created by Apple on 10/8/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class LoginController: UIViewController,NetworkingDelegate,UITextFieldDelegate {
    
    func onFailure(_ failureError: Any!) {
        activityIndicator.stopAnimating()
        self.activityIndicator.removeFromSuperview()
        print((failureError as AnyObject).debugDescription);
    }
    
    
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var chkRememberMe: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    var activityIndicator : NVActivityIndicatorView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator = NVActivityIndicatorView(frame: CGRect.init(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: 100, height: 100))
        activityIndicator.type = . ballScaleRippleMultiple // add your type
        activityIndicator.color = UIColor(red: 90 / 255, green: 198 / 255, blue: 243 / 255, alpha: 1) // add your color
        activityIndicator.center = self.view.center
        if(UserDefaults.standard.value(forKey: "rememberSelected") != nil)
        {
            if(UserDefaults.standard.value(forKey: "rememberSelected") as! String == "1")
            {
                self.txtUserName.text = UserDefaults.standard.value(forKey: "loginName") as? String ?? ""
                self.txtPassword.text = UserDefaults.standard.value(forKey: "loginPassword") as? String ?? ""
                print("Default Value User Name \(UserDefaults.standard.value(forKey: "loginName") as? String ?? "")")
                print("Default Values Password \(UserDefaults.standard.value(forKey: "loginPassword") as? String ?? "") ")
                self.chkRememberMe.isSelected = true
            }else {
                self.txtUserName.text = ""
                self.txtPassword.text = ""
            }
        }else {
            self.txtUserName.text = ""
            self.txtPassword.text = ""
        }
        self.btnLogin.layer.cornerRadius = self.btnLogin.frame.height/2;
        btnLogin.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0).cgColor
        btnLogin.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        btnLogin.layer.shadowOpacity = 0.25
        btnLogin.layer.shadowRadius = 5.0
        self.txtPassword.delegate = self
        self.txtUserName.delegate = self
        txtPassword.layer.cornerRadius = self.txtPassword.frame.height/2;
        txtUserName.layer.cornerRadius = self.txtUserName.frame.height/2;
        self.txtUserName.layer.shadowColor = UIColor.init(red: 34.0/255.0, green: 198.0/255.0, blue: 252.0/255.0, alpha: 1.0).cgColor //UIColor.lightGray.cgColor
        self.txtUserName.layer.shadowRadius = 5.0
        self.txtUserName.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        self.txtUserName.layer.masksToBounds = false
        self.txtUserName.layer.shadowOpacity = 0.25
        self.txtUserName.layoutIfNeeded()
        
        
        self.txtPassword.layer.shadowColor = UIColor.init(red: 34.0/255.0, green: 198.0/255.0, blue: 252.0/255.0, alpha: 1.0).cgColor//UIColor.lightGray.cgColor
        self.txtPassword.layer.shadowRadius = 5.0
        self.txtPassword.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        self.txtPassword.layer.masksToBounds = false
        self.txtPassword.layer.shadowOpacity = 0.25
        self.txtPassword.layoutIfNeeded()
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.txtPassword.frame.height))
        let paddingViewUser = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.txtUserName.frame.height))
        txtUserName.leftView = paddingViewUser
        txtUserName.leftViewMode = UITextFieldViewMode.always
        txtPassword.leftView = paddingView
        txtPassword.leftViewMode = UITextFieldViewMode.always
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
        NetworkingFunctions.shared().networkingDelegate = self
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    @IBAction func btnLoginPressed(_ sender: Any) {
        
        
        if(self.checkValidation() == true)
        {
            if(chkRememberMe.isSelected == true)
            {
                UserDefaults.standard.set(self.txtUserName.text, forKey: "loginName")
                UserDefaults.standard.set(self.txtPassword.text, forKey: "loginPassword")
            }
            else
            {
                UserDefaults.standard.set("", forKey: "loginName")
                UserDefaults.standard.set("", forKey: "loginPassword")
            }
            var userName : String = self.txtUserName.text!
            var password : String = self.txtPassword.text!
            var params : [String : Any] = ["username" : userName , "password" : password];
            self.view.addSubview(activityIndicator) // or use  webView.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            NetworkingFunctions.shared().postJSON(toURL: "http://access.bincee.com/auth/login" , withParams: params, withBearer: false)
            
        }
        else
        {
            
        }
        
        
    }
    func checkValidation() -> Bool {
        if(txtUserName.text?.count == 0 || txtUserName.text?.count == 1)
        {
            view.showToast(toastMessage: "Please Enter Username", duration: 3)
            return false
        }
        else if(txtPassword.text?.count == 0 || (txtPassword.text?.count)! <= 4)
        {
            view.showToast(toastMessage: "Please Enter Password", duration: 3)
            return false
        }
        else
        {
            return true
        }
    }
    @IBAction func btnRememberMeSelected(_ sender: Any) {
        self.chkRememberMe.isSelected = !self.chkRememberMe.isSelected
        if(self.chkRememberMe.isSelected == true)
        {
            UserDefaults.standard.set("1", forKey: "rememberSelected")
        }
        else
        {
            UserDefaults.standard.set("0", forKey: "rememberSelected")
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func onPostDataReceive(_ data: Any!) {
        activityIndicator.stopAnimating()
        self.activityIndicator.removeFromSuperview()
        var dataConverted = data as! NSDictionary
        print("Value for key \(String(describing: dataConverted.value(forKey: "username")))")
        
        if(dataConverted.value(forKey: "status") as! NSNumber == 200)
        {
            dataConverted = dataConverted.value(forKey: "data") as! NSDictionary
            if(dataConverted.value(forKey: "type") as! NSNumber == 4)
            {
                UserDefaults.standard.set(String(describing: dataConverted.value(forKey: "username")) , forKey: "userName")
                UserDefaults.standard.set(String(describing: dataConverted.value(forKey: "id") ?? "") , forKey: "userId")
                
                UserDefaults.standard.set(String(describing: dataConverted.value(forKey: "token") ?? "") , forKey: "authToken")
                UserDefaults.standard.set(true, forKey: "isFirstTime")
                UserDefaults.standard.set(true, forKey: "isLoggedIn")
                UIApplication.shared.registerForRemoteNotifications()
                print("***Brear Token***\(dataConverted.value(forKey: "token") ?? "")")
                self.performSegue(withIdentifier: "LoginSuccesfullSegue", sender: self)
            }
            else
            {
                
                print("Only Parents can login through this app")
            }
        }
        else
        {
            if(dataConverted != nil && dataConverted.count != 0)
            {
                dataConverted = dataConverted.value(forKey: "data") as! NSDictionary
                self.view.showToast(toastMessage: dataConverted.value(forKey: "message") as! String, duration: 3)
            }
        }
    }
    
}
extension UIView
{
    func showToast(toastMessage:String,duration:CGFloat)
    {
        //View to blur bg and stopping user interaction
        let bgView = UIView(frame: self.frame)
        bgView.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(255.0/255.0), blue: CGFloat(255.0/255.0), alpha: CGFloat(0.6))
        bgView.tag = 555
        
        //Label For showing toast text
        let lblMessage = UILabel()
        lblMessage.numberOfLines = 0
        lblMessage.lineBreakMode = .byWordWrapping
        lblMessage.textColor = .white
        lblMessage.backgroundColor = .black
        lblMessage.textAlignment = .center
        lblMessage.font = UIFont.init(name: "Helvetica Neue", size: 17)
        lblMessage.text = toastMessage
        
        //calculating toast label frame as per message content
        let maxSizeTitle : CGSize = CGSize(width: self.bounds.size.width-16, height: self.bounds.size.height)
        var expectedSizeTitle : CGSize = lblMessage.sizeThatFits(maxSizeTitle)
        // UILabel can return a size larger than the max size when the number of lines is 1
        expectedSizeTitle = CGSize(width:maxSizeTitle.width.getminimum(value2:expectedSizeTitle.width), height: maxSizeTitle.height.getminimum(value2:expectedSizeTitle.height))
        lblMessage.frame = CGRect(x:((self.bounds.size.width)/2) - ((expectedSizeTitle.width+16)/2) , y: (self.bounds.size.height/2) - ((expectedSizeTitle.height+16)/2), width: expectedSizeTitle.width+16, height: expectedSizeTitle.height+16)
        lblMessage.layer.cornerRadius = 8
        lblMessage.layer.masksToBounds = true
        lblMessage.padding = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        bgView.addSubview(lblMessage)
        self.addSubview(bgView)
        lblMessage.alpha = 0
        
        UIView.animateKeyframes(withDuration:TimeInterval(duration) , delay: 0, options: [] , animations: {
            lblMessage.alpha = 1
        }, completion: {
            sucess in
            UIView.animate(withDuration:TimeInterval(duration), delay: 8, options: [] , animations: {
                lblMessage.alpha = 0
                bgView.alpha = 0
            })
            bgView.removeFromSuperview()
        })
    }
}
extension CGFloat
{
    func getminimum(value2:CGFloat)->CGFloat
    {
        if self < value2
        {
            return self
        }
        else
        {
            return value2
        }
    }
}

//MARK: Extension on UILabel for adding insets - for adding padding in top, bottom, right, left.

extension UILabel
{
    private struct AssociatedKeys {
        static var padding = UIEdgeInsets()
    }
    
    var padding: UIEdgeInsets? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.padding) as? UIEdgeInsets
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &AssociatedKeys.padding, newValue as UIEdgeInsets!, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    override open func draw(_ rect: CGRect) {
        if let insets = padding {
            self.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
        } else {
            self.drawText(in: rect)
        }
    }
    
    override open var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            if let insets = padding {
                contentSize.height += insets.top + insets.bottom
                contentSize.width += insets.left + insets.right
            }
            return contentSize
        }
    }
}

