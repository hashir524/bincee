//
//  KidArrivedController.swift
//  bincee app
//
//  Created by Apple on 10/8/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class KidArrivedController: UIViewController {
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnOk: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnOk.layer.cornerRadius = self.btnOk.frame.height/2
        // Do any additional setup after loading the view.
        
    }
    
    @IBAction func btnOkPressed(_ sender: Any) {
        CurrentKidDataFirebase.shared.isFinalStatusVisible = false
        self.navigationController?.popToRootViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSearchPressed(_ sender: Any) {
        //        showLocateMe()
    }
    func showLocateMe() -> Void {
        let vc = LocateMeAlert.init(frame: UIScreen.main.bounds)
        
        self.view.addSubview(vc);
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

