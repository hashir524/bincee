//
//  LocateMeController.swift
//  bincee app
//
//  Created by Apple on 10/31/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Mapbox
import NVActivityIndicatorView
import MapboxGeocoder
import CoreLocation
import GooglePlaces

class LocateMeController: UIViewController,MGLMapViewDelegate,NetworkingDelegate, UITextFieldDelegate,UISearchBarDelegate,CLLocationManagerDelegate,UISearchControllerDelegate {
    //@IBOutlet weak var txtMyLocation: UITextField!
    @IBOutlet weak var searchView: UITextField!
    
   
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
   
    
    var resultView: UITextView?
    
    var locationLat : Double = Double()
    var locationLong : Double = Double()
    
    var activityIndicator : NVActivityIndicatorView!
    
    var pointAnnotations = MGLPointAnnotation()
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var currentLocationButton: UIButton!
  
    
    
    //var resultsViewController: GMSAutocompleteResultsViewController?
    
    @IBOutlet weak var vuMap: MGLMapView!
    @IBOutlet weak var btnConfirm: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTapped()
        
        
        // Do any additional setup after loading the view, typically from a nib.
        
        searchView.delegate = self
      
        
        
        //Stylinng the Searh Bar
        searchView.attributedPlaceholder = NSAttributedString(string: "Enter Location",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        
        searchView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0).cgColor
        searchView.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        searchView.layer.shadowOpacity = 0.25
        searchView.layer.shadowRadius = 5.0
        searchView.layer.masksToBounds = false
        searchView.layer.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 1.0).cgColor
        searchView.layer.cornerRadius = searchView.frame.height/2
        
        searchView.setLeftPaddingPoints(10)
        
        
        
        
        
        
   
        self.vuMap.userTrackingMode = .follow
        //self.txtMyLocation.layoutIfNeeded()
        //self.txtMyLocation.delegate = self
 
        activityIndicator = NVActivityIndicatorView(frame: CGRect.init(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: 100, height: 100))
        activityIndicator.type = . ballScaleRippleMultiple // add your type
        activityIndicator.color = UIColor(red: 90 / 255, green: 198 / 255, blue: 243 / 255, alpha: 1) // add your color
        activityIndicator.center = self.view.center
        // Do any additional setup after loading the view.
        
       
        
        
        (self.tabBarController as! TabbarController).hideTabbar()
        
        
       
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue))!
        autocompleteController.placeFields = fields
        
        /*
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .region
        autocompleteController.autocompleteFilter = filter
 */
 
        
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }


    
    @IBAction func backButtonClick(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    

    
    //These were the delagate methods for Drag and Drop Marker to set Location.
    
    /*
    // This delegate method is where you tell the map to load a view for a specific annotation. To load a static MGLAnnotationImage, you would use `-mapView:imageForAnnotation:`.
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        // This example is only concerned with point annotations.
        guard annotation is MGLPointAnnotation else {
            return nil
        }
        
        // For better performance, always try to reuse existing annotations. To use multiple different annotation views, change the reuse identifier for each.
        if let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "draggablePoint") {
            return annotationView
        } else {
            return DraggableAnnotationView(reuseIdentifier: "draggablePoint", size: 50)
        }
    }
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
 */
    
    //To Locate User Current Location at Button Click
    @IBAction func currentLocationButtonClick(_ sender: Any) {
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
         vuMap.userTrackingMode = .follow
        
        
    }
    
    // MARK: Location Delegate Methods
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        
        self.locationManager.stopUpdatingLocation()
        vuMap.userTrackingMode = .none
        
    }
    
    func  locationManager(manager: CLLocationManager, didFailWithError error: NSError)
    {
        print ("Errors:" + error.localizedDescription)
    }
    

    /*
    
    //To Perform Search After
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        //Testing MapBox Geocoder
        let geocoder = Geocoder.shared
        
        if  search.text != ""{
        let options = ForwardGeocodeOptions(query: "\(self.search.text!)")
        
        let task = geocoder.geocode(options) { (placemarks, attribution, error) in
            guard let placemark = placemarks?.first else {
                return
            }
            
            print(placemark.name)
            // 200 Queen St
            print(placemark.qualifiedName!)
            // 200 Queen St, Saint John, New Brunswick E2L 2X1, Canada
            
            self.vuMap.userTrackingMode = .none
            
            
            
            let coordinate = placemark.location!.coordinate
            print("\(coordinate.latitude), \(coordinate.longitude)")
            // 45.270093, -66.050985
            self.locationLat = coordinate.latitude
            self.locationLong = coordinate.longitude
            let searchAnnotation = MGLPointAnnotation()
            self.pointAnnotations.coordinate = CLLocationCoordinate2D(latitude: self.locationLat, longitude: self.locationLong)
            self.pointAnnotations.title = "\(coordinate.latitude),\(coordinate.longitude)"
            self.pointAnnotations.subtitle = "\(placemark.qualifiedName!)"
            //self.vuMap.addAnnotation(self.pointAnnotations)
            let camera = MGLMapCamera(lookingAtCenter: self.pointAnnotations.coordinate, fromDistance: 4500, pitch: 15, heading: 180)
            self.vuMap.fly(to: camera, withDuration: 4,
                        peakAltitude: 3000, completionHandler: nil)
            
            
        }
        }else{
            search.placeholder = "Enter Location"
        }
        
        //End Testing GeoCoder Code Here
        print("Search Button Click")
    }
    */
    
    //When Region Will Change this callback will work
    func mapView(_ mapView: MGLMapView, regionDidChangeAnimated animated: Bool) {
        print("Region Did Change Worked")
        
        let centerLocation = mapView.centerCoordinate
        print("Center Location Latitude  Region Changed \(centerLocation.latitude)")
        print("Center Location longitude Region Changed  \(centerLocation.longitude)")
        
        locationLat = Double(centerLocation.latitude)
        locationLong = Double(centerLocation.longitude)
    }
    
    //To Go to the annotation added
    func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
        let camera = MGLMapCamera(lookingAtCenter: annotation.coordinate, fromDistance: 20000, pitch: 15, heading: 180)
        mapView.fly(to: camera, withDuration: 4,
                    peakAltitude: 3000, completionHandler: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //After Rendreing The view
    override func viewDidAppear(_ animated: Bool) {
        
        self.vuMap.showsUserLocation = true
        self.vuMap.delegate = self
        
    
        //This Code Was for Drag and Drop to set location for a marker
        /*
        //Implementying Drag pin to Select Location Here
        // Specify coordinates for our annotations.
        let coordinates = [
            CLLocationCoordinate2D(latitude: 33.6844, longitude: 73.0479)
        ]
        
        // Fill an array with point annotations and add it to the map.
    
        for coordinate in coordinates {
            let point = MGLPointAnnotation()
            point.coordinate = coordinate
            point.title = "To drag this annotation, first tap and hold."
            //pointAnnotations.append(point)
            pointAnnotations = point
            
        }
        
        vuMap.addAnnotation(pointAnnotations)
 */
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    @IBAction func btnConfirmPressed(_ sender: Any) {
        self.vuMap.locationManager.stopUpdatingLocation()
        self.callApiToUpdateParent()
        
        print("On Confirm Button Pressed Latitude \(self.locationLat)")
        print("On Confirm Button Pressed Longitude \(self.locationLong)")
    }
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        
        print("User coordinates \(String(describing: mapView.userLocation?.coordinate))")
        
    }
    
    //When Ever the Location Changes on the Map. This Method Handles it
    
    func mapView(_ mapView: MGLMapView, didUpdate userLocation: MGLUserLocation?) {
        print("User coordinates didupdate location \(String(describing: userLocation?.coordinate))")
        
        //Setting up Current Lat Long Here..
        //self.txtMyLocation.text = ("\(mapView.userLocation?.coordinate.latitude) , \(mapView.userLocation?.coordinate.longitude)")
        print("Current Latutitude \(locationLat)")
        print("Current Longitude \(locationLong)")
        
    }
    
    
    func mapViewDidStopLocatingUser(_ mapView: MGLMapView) {
        print("User coordinates stop location \(String(describing: mapView.userLocation?.coordinate))")
    }
    func callApiToUpdateParent() {
        NetworkingFunctions.shared()?.networkingDelegate = self
        var params : [String : Any] = ["lat" : self.locationLat , "lng" : self.locationLong];
        var userId = UserDefaults.standard.value(forKey: "userId") ?? ""
        self.view.addSubview(activityIndicator) // or use  webView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        NetworkingFunctions.shared()?.postJSON(toURL: "http://access.bincee.com/school/parent/\(userId)", withParams: params, withBearer: true)
    }
    
    func onFailure(_ failureError: Any!) {
        activityIndicator.stopAnimating()
        self.activityIndicator.removeFromSuperview()
        print("failure occured")
    }
    
    func onPostDataReceive(_ data: Any!) {
        activityIndicator.stopAnimating()
        self.activityIndicator.removeFromSuperview()
        var convertedData : NSDictionary = data as! NSDictionary
        if(convertedData.value(forKey: "status") as! NSNumber == 200)
        {
            
            SharedParentData.shared.parentData?.data?.lat = Float(locationLat)
            SharedParentData.shared.parentData?.data?.lng = Float(locationLong)
            (self.tabBarController as! TabbarController).showTabbar()
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            print("No data received")
        }
        print("update successfully")
    }
   
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
  
    
    
}

extension LocateMeController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place ID: \(place.placeID)")
        print("Place attributions: \(place.attributions)")
        
        print("Formated Address \(place.formattedAddress)")
        print("Lat From Search.\(place.coordinate.latitude)")
        print("Long From Search.\(place.coordinate.longitude)")
        
        self.locationLat = place.coordinate.latitude
        self.locationLong = place.coordinate.longitude
        self.searchView.text = place.name!
        
        //Moving the map here
        self.vuMap.userTrackingMode = .none
        
        let searchAnnotation = MGLPointAnnotation()
        self.pointAnnotations.coordinate = CLLocationCoordinate2D(latitude: self.locationLat, longitude: self.locationLong)
        self.pointAnnotations.title = "\(place.coordinate.latitude),\(place.coordinate.longitude)"
        self.pointAnnotations.subtitle = "\(place.name!)"
        //self.vuMap.addAnnotation(self.pointAnnotations)
        let camera = MGLMapCamera(lookingAtCenter: self.pointAnnotations.coordinate, fromDistance: 4500, pitch: 15, heading: 180)
        self.vuMap.fly(to: camera, withDuration: 4,
                       peakAltitude: 3000, completionHandler: nil)
        
        
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}




// Put this piece of code anywhere you like
extension UIViewController {
    func hideKeyboardWhenTapped() {
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
}
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}





