//
//  AlertsAndAnnouncementController.swift
//  bincee app
//
//  Created by Apple on 10/8/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class AlertsAndAnnouncementController: UIViewController,UITableViewDelegate,UITableViewDataSource,RefreshAlertsAndAnnouncementDelegate{
    
    
    
    var isFirstTime : Bool = true
    var isAnnouncement : Bool = false
    @IBOutlet weak var btnEmergencyAlerts: UIButton!
    var emergencyAlertsArray : [Data] = []
    var announcmentsArrayAlmo : [AnnouncementData] = []
    @IBOutlet weak var btnSchoolAnnouncement: UIButton!
    var alertsArray : NSArray = NSArray()
    var announcementArray : NSArray = NSArray()
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var tableViewMain: UITableView!
    @IBOutlet weak var tableViewFooterImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if self.revealViewController() != nil {
            
            self.btnMenu.addTarget(self.revealViewController(), action:  #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            
        }
        self.tableViewMain.delegate = self
        self.tableViewMain.dataSource = self
        self.btnEmergencyAlerts.layer.cornerRadius = self.btnEmergencyAlerts.frame.height/2;
        self.btnSchoolAnnouncement.layer.cornerRadius = self.btnSchoolAnnouncement.frame.height/2;
        self.btnEmergencyAlertsPressed(self.btnEmergencyAlerts)
        (self.tabBarController as! TabbarController).refreshAlertScreenDelegate = self
        isFirstTime = false
        tableViewMain.estimatedRowHeight = 60
        (self.tabBarController as! TabbarController).showLoader()
        callApiForAlertAlmo()
        
        // Do any additional setup after loading the view.
        
        //Make an Array to store
        storeNotificationStatus()
    }
    override func viewDidAppear(_ animated: Bool) {
        //NetworkingFunctions.shared()?.networkingDelegate = self
        if(isFirstTime)
        {
            refreshAlertData()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func refreshAnnouncementData() {
        callApiForAnnoncementAlmo()
    }
    func refreshAlertData()
    {
        callApiForAlertAlmo()
    }
    
    //****Storing and Making Array******
    func storeNotificationStatus()  {
        
        if let integerArray = UserDefaults.standard.object(forKey: "alertArray") as? [Int]{
            //good to go
            print("Array is Already Created....")
        }else {
            
            print("Alert Array Not Fetched")
            //Storing Alerts Empty Array
            var integerArray: [Int] = []
            integerArray.append(1200)
            UserDefaults.standard.set(integerArray, forKey: "alertArray")
        }
        
        if let annocumentArray = UserDefaults.standard.object(forKey: "announcmentArray") as? [Int]{
            print("Annoucment Array Already Creadted")
        }else {
            
            print("Annoucment Array not fetched")
            //Storing and creating Annoucments Empty array
            var annoucmentArray : [Int] = []
            annoucmentArray.append(1200)
            UserDefaults.standard.set(annoucmentArray, forKey: "announcmentArray")
            
        }
        

        
    }
    
    @IBAction func btnMenuPressed(_ sender: Any) {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(btnEmergencyAlerts.isSelected == true){
            return emergencyAlertsArray.count
        }
        else
        {
            return announcmentsArrayAlmo.count
        }
    }
    
    
    @IBAction func btnEmergencyAlertsPressed(_ sender: Any) {
        btnEmergencyAlerts.isSelected = true
        if btnEmergencyAlerts.isSelected
        {
            btnSchoolAnnouncement.isSelected = false
            //                btnEmergencyAlerts.titleLabel?.textColor = .white
            btnEmergencyAlerts.backgroundColor = UIColor(red: 3.0/255, green: 199.0/255, blue: 255.0/255.0, alpha: 1)
            //                btnSchoolAnnouncement.titleLabel?.textColor = .black
            btnSchoolAnnouncement.backgroundColor = .clear
            (self.tabBarController as! TabbarController).showLoader()
            self.tableViewFooterImage.image = UIImage(named: "parents-emergency-bg")
            callApiForAlertAlmo()
            
        }
        
    }
    
    @IBAction func btnSchoolAnnouncementPressed(_ sender: Any) {
        btnSchoolAnnouncement.isSelected = true
        if btnSchoolAnnouncement.isSelected
        {
            //            btnSchoolAnnouncement.titleLabel?.textColor = .white
            btnSchoolAnnouncement.backgroundColor = UIColor(red: 3.0/255, green: 199.0/255, blue: 255.0/255.0, alpha: 1)
            //            btnEmergencyAlerts.titleLabel?.textColor = .black
            btnEmergencyAlerts.backgroundColor = .clear
            btnEmergencyAlerts.isSelected = false
            (self.tabBarController as! TabbarController).showLoader()
            self.tableViewFooterImage.image = UIImage(named: "parent-school-bg")
            callApiForAnnoncementAlmo()
        }
    }
    
    // MARK: Tableview
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.tableViewMain.register(UINib(nibName: "AnnouncementCell", bundle: nil), forCellReuseIdentifier: "AnnouncementCell")
        let cell = tableViewMain.dequeueReusableCell(withIdentifier: "AnnouncementCell", for: indexPath) as! AnnouncementCell
        cell.selectionStyle = .none
        if(btnEmergencyAlerts.isSelected == true)
        {
            //var currentCellAlert = alertsArray.object(at: indexPath.row) as! NSDictionary
            var currentCellAlert = emergencyAlertsArray[indexPath.row]
            print("****Notification Alerts Data***** \(currentCellAlert.id!)")
            cell.lblAlertTitle.text = currentCellAlert.title ?? ""
            cell.lblAlertDetail.text = currentCellAlert.descriptionField ?? ""
            //cell.lblAlertTitle.text = currentCellAlert.value(forKey: "title") as? String ?? ""
            //cell.lblAlertDetail.text = currentCellAlert.value(forKey: "description") as? String ?? ""
            if let integerArray = UserDefaults.standard.object(forKey: "alertArray") as? [Int]{
                //good to go
                if integerArray.count != 0 {
                    if let index = integerArray.firstIndex(of: emergencyAlertsArray[indexPath.row].id!) {
                        print(index)
                        print("ID Found, This Status is read...")
                        cell.imgAlertStatus.image = UIImage(named: "Notification")
                        
                    }else {
                        cell.imgAlertStatus.image = UIImage(named: "Alert-icon")
                        print("it's an Empty Array")
                    }
                }
               
                
            }else {
                
                print("Alert Array Not Fetched")
            }
            
            
        }
        else
        {
            
            var currentAnnoucment = announcmentsArrayAlmo[indexPath.row]
            print("****Notification Annoucments Data***** \(currentAnnoucment.id!)")
            cell.lblAlertTitle.text = currentAnnoucment.title ?? ""
            cell.lblAlertDetail.text = currentAnnoucment.descriptionField ?? ""
            
            
            if let integerArray = UserDefaults.standard.object(forKey: "announcmentArray") as? [Int]{
                //good to go
                if integerArray.count != 0 {
                    if let index = integerArray.firstIndex(of: announcmentsArrayAlmo[indexPath.row].id!) {
                        print(index)
                        print("ID Found, This Status is read...")
                        cell.imgAlertStatus.image = UIImage(named: "Notification")
                        
                    }else {
                        cell.imgAlertStatus.image = UIImage(named: "Alert-icon")
                        print("it's an Empty Array")
                    }
                }
                
                
            }else {
                
                print("Alert Array Not Fetched")
            }
            
        }
        
        return cell;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alertShow = SimpleAlert.init(frame: self.view.bounds)
        if(btnEmergencyAlerts.isSelected == true)
        {
            //var currentAlert = self.alertsArray.object(at: indexPath.row) as! NSDictionary
            var currentAlert = self.emergencyAlertsArray[indexPath.row]
            alertShow.lblAlertTitle.text = currentAlert.title ?? ""
            alertShow.txtAlert.text      = currentAlert.descriptionField ?? ""
            
            
            //Getting The Cell UI We Clicked
            let indexxPath = tableView.indexPathForSelectedRow
            let currentCell = tableView.cellForRow(at: indexxPath!) as! AnnouncementCell
            currentCell.imgAlertStatus.image = UIImage(named: "Notification")
            
            
            if let integerArray = UserDefaults.standard.object(forKey: "alertArray") as? [Int]{
                //good to go
                var statusArray = integerArray
                statusArray.append(emergencyAlertsArray[indexPath.row].id!)
                
                //Again Storing Values in User Defaults
                UserDefaults.standard.set(statusArray, forKey: "alertArray")
                
            }
            else{
                //no object for key "sugarArray" or object couldn't be converted to [Int]
                print("***Value Not Added To Alert List*******")
            }
            
            //When Ever User Clicks the notification it opens up. It Should be marked as read.
            //Logic # Every Norification Has a unique ID..
            //# We will user a boolean flag against that id.. True for Read..
            //# Then When the Data is feteced from api
        }
        else
        {
            //var currentAnnoucement = self.announcementArray.object(at: indexPath.row) as! NSDictionary
            var currentAnnoucement = self.announcmentsArrayAlmo[indexPath.row]
            alertShow.lblAlertTitle.text = currentAnnoucement.title ?? ""
            alertShow.txtAlert.text      = currentAnnoucement.descriptionField ?? ""
            //Getting The Cell UI We Clicked
            let indexxPath = tableView.indexPathForSelectedRow
            let currentCell = tableView.cellForRow(at: indexxPath!) as! AnnouncementCell
            currentCell.imgAlertStatus.image = UIImage(named: "Notification")
            
            if let integerArray = UserDefaults.standard.object(forKey: "announcmentArray") as? [Int]{
                //good to go
                var statusArray = integerArray
                statusArray.append(announcmentsArrayAlmo[indexPath.row].id!)
                
                //Again Storing Values in User Defaults
                UserDefaults.standard.set(statusArray, forKey: "announcmentArray")
                
            }
            else{
                //no object for key "sugarArray" or object couldn't be converted to [Int]
                print("***Value Not Added To Alert List*******")
            }
            
        }
        self.view.addSubview(alertShow)
    }
    
    /*
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
       /*
        if(btnSchoolAnnouncement.isSelected == false)
        {
            
            //315*222
            let ratio = self.view.frame.size.width / 315
            let height = 222 * ratio
            let imageViewGame = UIImageView(frame: CGRect(x: 0,y: 0,width: self.tableViewMain.frame.width,height: height));
            
            var myImage: UIImage = UIImage(named: "parents-emergency-bg")!
            imageViewGame.image = myImage
            imageViewGame.contentMode = .scaleAspectFit
            imageViewGame.backgroundColor = UIColor.init(red: 245 / 255, green: 245 / 255, blue: 245 / 255, alpha: 1.0)
            //            footer.addSubview(imageViewGame)
            return imageViewGame
        }
        else
        {
            //321*185
            let ratio = self.view.frame.size.width / 321
            let height = 185 * ratio
            let imageViewGame = UIImageView(frame: CGRect(x: 0,y: 0,width: self.tableViewMain.frame.width,height: height));
            
            var myImage: UIImage = UIImage(named: "parent-school-bg")!
            imageViewGame.image = myImage
            imageViewGame.contentMode = .scaleAspectFit
            imageViewGame.backgroundColor = UIColor.init(red: 245 / 255, green: 245 / 255, blue: 245 / 255, alpha: 1.0)
            //            footer.addSubview(imageViewGame)
            return imageViewGame
            
        }
 */
        
        
        
    }
 */
    
   /*
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if(btnSchoolAnnouncement.isSelected == false)
        {
            let ratio = self.view.frame.size.width / 315
            let height = 222 * ratio
            return height
        }
        else
        {
            let ratio = self.view.frame.size.width / 321
            let height = 185 * ratio
            return height
        }
    }
 */
    // Interchanged because of server side issues
    
    
    func callApiForAlertAlmo() {
        //Caalling Api For Alerts
        let header:HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "Bearer " + Constants.TOKEN
        ]
        
        var curKid = UserDefaults.standard.value(forKey: "selectedKidId") as! Int;
        print("**** Selected Kid ID**** \(curKid)")
        
        let parentId : String = UserDefaults.standard.value(forKey: "userId") as! String
        isAnnouncement = true
        Alamofire.request("http://access.bincee.com/school/notification/parent/\(parentId)", method: .get, encoding: JSONEncoding.default, headers: header)
            .responseObject {(response:
                DataResponse<AlertResponseModel>) in
                let alertsResponse =  response.result.value
                print(alertsResponse?.status ?? "null")
                (self.tabBarController as! TabbarController).hideLoader()
                switch alertsResponse?.status {
                case 200:
                    //Instantiating Dashboard drawer controller here in this IBAction
                    print("Alerts Api Success")

                    
                    for alerts in alertsResponse!.alertData! {
                        if alerts.studentId == curKid{
                            self.emergencyAlertsArray = alerts.data!
                            //Reload the Table View Here
                            self.tableViewMain.reloadData()
                        }
                    }
                    
                    
                    
                default:
                    print(alertsResponse?.status ?? "Api Problem")
                    
                }
                
        }
        
        
    }
    
    func callApiForAnnoncementAlmo()  {
        
        let header:HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "Bearer " + Constants.TOKEN
        ]
        
        var schoolId : String = UserDefaults.standard.value(forKey: "school_id") as? String ?? "-1"
        print("*****School Id******\(schoolId)")
        isAnnouncement = false
        Alamofire.request("http://access.bincee.com/school/notification/list/\(schoolId)", method: .get, encoding: JSONEncoding.default, headers: header)
            .responseObject {(response:
                DataResponse<AnnouncementsResponse>) in
                let announcmentResponse =  response.result.value
                print(announcmentResponse?.status ?? "null")
                (self.tabBarController as! TabbarController).hideLoader()
                switch announcmentResponse?.status {
                case 200:
                    //Instantiating Dashboard drawer controller here in this IBAction
                    print("Announcment Response Success")
                    self.announcmentsArrayAlmo = announcmentResponse!.announcementData!
                    self.tableViewMain.reloadData()
                    
                default:
                    print(announcmentResponse?.status ?? "Api Problem")
                    
                }
                
        }
        
        
    }
    
   
    /*
    func onGetDataReceive(_ data: Any!) {
        (self.tabBarController as! TabbarController).hideLoader()
        if(isAnnouncement == true)
        {
            if(data == nil)
            {
                return
            }
            var dataConverted = data as! NSDictionary
            if(dataConverted.value(forKey: "status") as! NSNumber == 200)
            {
                isAnnouncement = false
                
                var curKid = UserDefaults.standard.value(forKey: "selectedKidId") as! Int;
                var announcementForAllKids = dataConverted.value(forKey: "data") as? NSArray ?? NSArray()
                if(announcementForAllKids == NSArray())
                {
                    return
                }
                if(announcementForAllKids as? NSArray ?? [] == [])
                {
                    return
                }
                //                announcementArray = dataConverted.value(forKey: String(curKid)) as! NSArray
                for i in 0..<announcementForAllKids.count
                {
                    var singleKidInfo = announcementForAllKids.object(at: i) as? NSDictionary ?? NSDictionary()
                    if(singleKidInfo != NSDictionary() && singleKidInfo.value(forKey: "student_id") as? Int ?? -1 == curKid )
                    {
                        announcementForAllKids = singleKidInfo.value(forKey: "data") as? NSArray ?? NSArray()
                        announcementArray = announcementForAllKids
                        break
                    }
                }
                
                print(dataConverted)
                self.tableViewMain.reloadData()
            }
        }
        else
        {
            if(data == nil)
            {
                return
            }
            var dataConverted = data as! NSDictionary
            if(dataConverted.value(forKey: "status") as! NSNumber == 200)
            {
                
                alertsArray = dataConverted.value(forKey: "data") as? NSArray ?? NSArray()
                if(alertsArray == NSArray())
                {
                    return
                }
                print(dataConverted)
                //self.callApiForAnnoncement()
            }
        }
        
        self.tableViewMain.reloadData()
    }
    func onFailure(_ failureError: Any!) {
        (self.tabBarController as! TabbarController).hideLoader()
        print("Error occured")
    }
    
    //Using AlmoFire For Notifications
    //Calling All Kitchen Api Here
    */
    
}

