//
//  SplashAnimationController.swift
//  bincee app
//
//  Created by Apple on 1/31/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class SplashAnimationController: UIViewController {
    @IBOutlet weak var imgLogo: UIImageView!
    let animationDuration = 2.5
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        var array : [UIImage] = []
        for i in 1..<36 {
            array.append(UIImage(named: "\(i).png")!)
        }
        //        imgLogo.image = array.last
        
        imgLogo.animationImages = array
        imgLogo.animationRepeatCount = 1
        imgLogo.animationDuration = animationDuration
        imgLogo.startAnimating()
        
        self.perform(#selector(SplashAnimationController.finishAnimation), with: nil, afterDelay: animationDuration)
    }
    
    @objc func finishAnimation() {
        imgLogo.image = imgLogo.animationImages?.last
        if(UserDefaults.standard.value(forKey: "isLoggedIn") as? Bool == true)
        {
            self.performSegue(withIdentifier: "AlreadyLoggedInSegue", sender: self)
        }
        else
        {
            self.performSegue(withIdentifier: "LoginViewControllerSegue", sender: self)
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

