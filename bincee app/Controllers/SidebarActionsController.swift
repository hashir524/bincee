//
//  SidebarActionsController.swift
//  bincee app
//
//  Created by Apple on 10/17/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import SDWebImage
import FirebaseMessaging

class SidebarActionsController: UIViewController {
    
    
    @IBOutlet weak var btnLogout: UIButton!
    
    @IBOutlet weak var imgProfileSidebar: UIImageView!
    @IBOutlet weak var lblPersonName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        self.lblPersonName.text = UserDefaults.standard.value(forKey: "userName") as! String
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillAppear(_ animated: Bool) {
        setViewsWithData()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnMyKidsProfilePressed(_ sender: Any) {
    }
    @IBAction func btnSettingsPressed(_ sender: Any) {
    }
    @IBAction func btnDriverProfilePressed(_ sender: Any) {
        self.revealViewController().revealToggle(animated: true)
        let vc = DriveInformationAlert.init(frame: UIScreen.main.bounds)
        UIApplication.shared.keyWindow?.addSubview(vc)
        
    }
    @IBAction func btnContactUsPressed(_ sender: Any) {
        
        let contactViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactViewNavigationController") as! UINavigationController
        self.present(contactViewController, animated: true, completion: nil)
        self.revealViewController().revealToggle(animated: true)
    }
    @IBAction func btnFAQPressed(_ sender: Any) {
        self.revealViewController().revealToggle(animated: true)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "launchLocateMeScreen"), object: nil, userInfo: nil)
        
    }
    @IBAction func btnAlertsPressed(_ sender: Any) {
        //        let tabbarController = self.storyboard?.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
        
        
        
        //        revealViewController().pushFrontViewController(TabbarController, animated: true)
        (revealViewController().frontViewController as! TabbarController).selectCustomTabWithIndex(2)
        revealViewController().revealToggle(animated: true)
    }
    
    @IBAction func btnMyProfilePressed(_ sender: Any) {
        self.revealViewController().revealToggle(animated: true)
        let profileViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewNavigationController") as! UINavigationController
        self.present(profileViewController, animated: true, completion: nil)
        
    }
    @IBAction func btnHomePressed(_ sender: Any) {
        
        (revealViewController().frontViewController as! TabbarController).selectCustomTabWithIndex(0)
        revealViewController().revealToggle(animated: true)
        
        
        //        TabbarController
    }
    func setViewsWithData() {
        self.imgProfileSidebar.layer.cornerRadius = 10
        imgProfileSidebar.layer.shadowColor = UIColor.init(red: 34.0/255.0, green: 198.0/255.0, blue: 252.0/255.0, alpha: 1.0).cgColor
        imgProfileSidebar.layer.shadowRadius = 10
        imgProfileSidebar.layer.shadowOffset = CGSize.init(width: 0, height: 5)
        imgProfileSidebar.layer.masksToBounds = false
        imgProfileSidebar.layer.shadowOpacity = 0.25
        imgProfileSidebar.layoutIfNeeded()
        lblPersonName.text = UserDefaults.standard.value(forKey: "userFullname") as? String
        var profileImageUrl : String = UserDefaults.standard.value(forKey: "userPhoto") as! String
        imgProfileSidebar.sd_setShowActivityIndicatorView(true)
        imgProfileSidebar.sd_setIndicatorStyle(.gray)
        imgProfileSidebar.sd_setImage(with: URL.init(string: profileImageUrl), placeholderImage: UIImage.init(named: "Avatar"), options: .continueInBackground, progress: nil, completed: nil)
        
    }
    @IBAction func btnLogoutPressed(_ sender: Any) {
        
        
        self.revealViewController().revealToggle(animated: true)
        let vc = LogoutAlert.init(frame: UIScreen.main.bounds)
        UIApplication.shared.keyWindow?.addSubview(vc)
        
        
    }
    func logoutAndMakeNavigation() {
        self.unSubscribeTopic()			
        let domain = Bundle.main.bundleIdentifier!
        var fcmToken = UserDefaults.standard.value(forKey: "fcmToken") as! String
        
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
        
        UserDefaults.standard.setValue(fcmToken as! String , forKey: "fcmToken")
        UIApplication.shared.unregisterForRemoteNotifications()
        
        self.launchMainScreen()
    }
    func unSubscribeTopic()  {
        var schoolId : String = UserDefaults.standard.value(forKey: "school_id") as? String ?? "-1"
        schoolId =  "school-\(schoolId)"
        
        Messaging.messaging().unsubscribe(fromTopic: schoolId)
    }
    func launchMainScreen()  {
        self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

