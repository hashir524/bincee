//
//  LogoutController.swift
//  bincee app
//
//  Created by Apple on 1/31/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import FirebaseMessaging

class LogoutController: UIViewController {
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var vuContent: UIView!
    @IBOutlet weak var imgLogout : UIImageView!
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.vuContent.layer.cornerRadius = 20.0
        self.vuContent.clipsToBounds = true
        
        self.vuContent.layer.shadowColor = UIColor.lightGray.cgColor
        self.vuContent.layer.shadowRadius = 10.0
        self.vuContent.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        self.vuContent.layer.masksToBounds = false
        self.vuContent.layer.shadowOpacity = 1.0
        self.vuContent.layoutIfNeeded()
        imgLogout.clipsToBounds = true
        self.makeCornerRadius()
        if(screenHeight <= 568)
        {
            self.vuContent.transform = CGAffineTransform(scaleX:0.7,y: 0.7)
        }
        else if((screenHeight <= 667))
        {
            self.vuContent.transform = CGAffineTransform(scaleX:0.85,y: 0.85)
        }
        else
        {
            self.vuContent.transform = CGAffineTransform(scaleX: 1,y: 1)
        }
        // Do any additional setup after loading the view.
    }
    func makeCornerRadius() -> Void {
        self.btnLogout.layer.cornerRadius = self.btnLogout.frame.height/2;
        
        btnLogout.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0).cgColor
        btnLogout.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        btnLogout.layer.shadowOpacity = 0.25
        btnLogout.layer.shadowRadius = 5.0
        
        
    }
    @IBAction func btnCancelPressed(_ sender: Any) {
    }
    @IBAction func btnLogoutPressed(_ sender: Any) {
        self.logoutAndMakeNavigation()
    }
    func logoutAndMakeNavigation() {
        self.unSubscribeTopic()
        let domain = Bundle.main.bundleIdentifier!
        var fcmToken = UserDefaults.standard.value(forKey: "fcmToken") as! String
        
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
        
        UserDefaults.standard.setValue(fcmToken as! String , forKey: "fcmToken")
        UIApplication.shared.unregisterForRemoteNotifications()
        
        self.launchMainScreen()
    }
    func unSubscribeTopic()  {
        var schoolId : String = UserDefaults.standard.value(forKey: "school_id") as? String ?? "-1"
        schoolId =  "school-\(schoolId)"
        
        Messaging.messaging().unsubscribe(fromTopic: schoolId)
    }
    func launchMainScreen()  {
        self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

