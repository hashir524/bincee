//
//  ForgotPasswordController.swift
//  bincee app
//
//  Created by Apple on 1/28/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ForgotPasswordController: UIViewController,UITextFieldDelegate,NetworkingDelegate {
    @IBOutlet weak var btnBack: UIButton!
    var activityIndicator : NVActivityIndicatorView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtUsername: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator = NVActivityIndicatorView(frame: CGRect.init(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: 100, height: 100))
        activityIndicator.type = . ballScaleRippleMultiple // add your type
        activityIndicator.color = UIColor(red: 90 / 255, green: 198 / 255, blue: 243 / 255, alpha: 1) // add your color
        activityIndicator.center = self.view.center
        self.updateView()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        NetworkingFunctions.shared()?.networkingDelegate = self
    }
    func updateView() {
        self.btnSubmit.layer.cornerRadius = self.btnSubmit.frame.height/2;
        btnSubmit.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0).cgColor
        btnSubmit.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        btnSubmit.layer.shadowOpacity = 0.25
        btnSubmit.layer.shadowRadius = 5.0
        self.txtEmail.delegate = self
        
        txtEmail.layer.cornerRadius = self.txtEmail.frame.height/2;
        
        self.txtEmail.layer.shadowColor = UIColor.init(red: 34.0/255.0, green: 198.0/255.0, blue: 252.0/255.0, alpha: 1.0).cgColor //UIColor.lightGray.cgColor
        self.txtEmail.layer.shadowRadius = 5.0
        self.txtEmail.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        self.txtEmail.layer.masksToBounds = false
        self.txtEmail.layer.shadowOpacity = 0.25
        self.txtEmail.layoutIfNeeded()
        
        
        self.txtUsername.delegate = self
        
        txtUsername.layer.cornerRadius = self.txtEmail.frame.height/2;
        
        self.txtUsername.layer.shadowColor = UIColor.init(red: 34.0/255.0, green: 198.0/255.0, blue: 252.0/255.0, alpha: 1.0).cgColor //UIColor.lightGray.cgColor
        self.txtUsername.layer.shadowRadius = 5.0
        self.txtUsername.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        self.txtUsername.layer.masksToBounds = false
        self.txtUsername.layer.shadowOpacity = 0.25
        self.txtUsername.layoutIfNeeded()
        
        let paddingViewUserName = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.txtUsername.frame.height))
        txtUsername.leftView = paddingViewUserName
        txtUsername.leftViewMode = UITextFieldViewMode.always
        
        
        let paddingViewUser = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.txtEmail.frame.height))
        txtEmail.leftView = paddingViewUser
        txtEmail.leftViewMode = UITextFieldViewMode.always
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    @IBAction func btnSubmitPressed(_ sender: Any) {
        if(checkValidation() == true)
        {
            self.callApiForForgotPassword()
        }
        else
        {
            
        }
    }
    func callApiForForgotPassword()  {
        var email : String = self.txtEmail.text!
        var userName : String = self.txtUsername.text!
        var params : [String : Any] = ["email" : email,
                                       "username" : userName,
                                       "type" : "Parent",
                                       "selected_option" : "email"];
        self.view.addSubview(activityIndicator) // or use  webView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        NetworkingFunctions.shared().postJSON(toURL: "http://access.bincee.com/users/passwordreset" , withParams: params, withBearer: false)
    }
    func checkValidation() -> Bool {
        if(txtEmail.text?.count ?? 0 <= 4 || txtUsername.text?.count ?? 0 <= 4)
        {
            view.showToast(toastMessage: "Please Enter valid username and email", duration: 3)
            return false
        }
        else if(isValidEmail(testStr: txtEmail.text!) == false)
        {
            view.showToast(toastMessage: "Please Enter a valid email", duration: 3)
            return false
        }
        else
        {
            return true
        }
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    @IBAction func btnBackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func onPostDataReceive(_ data: Any!) {
        activityIndicator.stopAnimating()
        self.activityIndicator.removeFromSuperview()
        var dataConverted = data as! NSDictionary
        
        if(dataConverted.value(forKey: "status") as! NSNumber == 200)
        {
            dataConverted = dataConverted.value(forKey: "data") as! NSDictionary
            self.view.showToast(toastMessage: dataConverted.value(forKey: "message") as? String ?? "Check your email", duration: 3)
        }
        else if(dataConverted.value(forKey: "status") as! NSNumber == 404)  {
            self.view.showToast(toastMessage: "Invalid Email Address" as! String, duration: 3)
        }
        else
        {
            if(dataConverted != nil && dataConverted.count != 0)
            {
                dataConverted = dataConverted.value(forKey: "data") as! NSDictionary
                self.view.showToast(toastMessage: dataConverted.value(forKey: "message") as! String, duration: 3)
            }
        }
    }
    func onFailure(_ failureError: Any!) {
        //on failure
        activityIndicator.stopAnimating()
        self.activityIndicator.removeFromSuperview()
        print((failureError as AnyObject).debugDescription);
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

