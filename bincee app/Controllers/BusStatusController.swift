//
//  BusStatusController.swift
//  bincee app
//
//  Created by Apple on 10/8/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class BusStatusController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    
    @IBOutlet weak var imgContraintEnd: NSLayoutConstraint!
    @IBOutlet weak var lblShiftName: UILabel!
    @IBOutlet weak var imgStatus: UIImageView!
     var etaEstimate : String = ""
    var isMorning : Bool = false
    var isNotAvailable : Bool = true
    @IBOutlet weak var btnMenu: UIButton!
    var counter : Int = 0;
    @IBOutlet weak var tableViewMain: UITableView!
    @IBOutlet weak var btnRealTimeTracking: UIButton!
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(showSide), name: NSNotification.Name(rawValue: "statusChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(markedAbsent), name: NSNotification.Name(rawValue: "markedAbsent"), object: nil)
        
        self.tableViewMain.delegate = self
        self.tableViewMain.dataSource = self
        counter = 0;
        self.btnRealTimeTracking.layer.cornerRadius = self.btnRealTimeTracking.frame.height/2;
        btnRealTimeTracking.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0).cgColor
        btnRealTimeTracking.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        btnRealTimeTracking.layer.shadowOpacity = 0.25
        btnRealTimeTracking.layer.shadowRadius = 5.0
        
        // Do any additional setup after loading the view.
    }
    
    @objc func showSide()
    {
        counter = CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "status") as! Int
        self.tableViewMain.reloadData()
        print("status changed of student")
    }
    @objc func markedAbsent()
    {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        if(screenHeight == 812)
        {
            self.imgContraintEnd.constant = 65
        }
        else
        {
            self.imgContraintEnd.constant = 37
        }
        var rideTime : String = UserDefaults.standard.value(forKey: "rideTime") as! String
        if(rideTime == nil || rideTime == "")
        {
            isNotAvailable = true
            return
        }
        else if(rideTime == "morning")
        {
            isMorning = true
            isNotAvailable = false
            counter = CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "status") as! Int
            self.lblShiftName.text = "Pickup"
            self.tableViewMain.reloadData()
        }
        else if(rideTime == "Evening1")
        {
            isMorning = false
            isNotAvailable = false
            counter = 1
            self.lblShiftName.text = "Dropoff"
            self.tableViewMain.reloadData()
        }
        else
        {
            isMorning = false
            isNotAvailable = false
            counter = CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "status") as! Int
            self.lblShiftName.text = "Dropoff"
            self.tableViewMain.reloadData()
        }

    }
    override func viewDidAppear(_ animated: Bool) {
        CurrentKidDataFirebase.shared.isBusStatusVisible = true
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        CurrentKidDataFirebase.shared.isBusStatusVisible = false
    }
    // MARK: Tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        etaEstimate = "\(Int(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "duration") as? Double ?? 0.0))"
        let etaCheck = Int(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "duration") as? Double ?? 0.0)
        if(isMorning == true)
        {
            //Morning shift
            if(indexPath.row == 0)
            {
                            self.tableViewMain.register(UINib(nibName: "BusStatusRightCell", bundle: nil), forCellReuseIdentifier: "BusStatusRightCell")
                            let cell = tableViewMain.dequeueReusableCell(withIdentifier: "BusStatusRightCell", for: indexPath) as! BusStatusRightCell
               
                if etaCheck <= 5 {
                    cell.lblStatusTitle.text = "Bus is coming"
                    cell.lblStatusDescription.text = "Bus is on its way to pickup \n\(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "fullname") as! String) and will \nbe there in \(etaEstimate) minutes"
                }else{
                    cell.lblStatusTitle.text = "Bus is coming"
                    cell.lblStatusDescription.text = "Bus is on its way to pickup \n\(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "fullname") as! String)"
                    
                }
               
                 cell.lblStatusTitle.textColor = UIColor.init(red: 45 / 255, green: 54 / 255, blue: 57 / 255, alpha: 1.0);
                cell.lblStatusDescription.textColor = UIColor.init(red: 56.0 / 255, green: 56.0 / 255, blue: 56.0 / 255, alpha: 1.0);
                                imgStatus.image = UIImage(named: "Status1-bg")
                

                return cell
            }
            else if(indexPath.row == 1)
            {
                            self.tableViewMain.register(UINib(nibName: "BusStatusLeftCell", bundle: nil), forCellReuseIdentifier: "BusStatusLeftCell")
                            let cell = tableViewMain.dequeueReusableCell(withIdentifier: "BusStatusLeftCell", for: indexPath) as! BusStatusLeftCell
                                cell.lblStatusTitle.text = "Bus is here"
                                cell.lblStatusDescription.text = "Bus has arrived to pickup  \n\(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "fullname") as! String) and will \n  leave soon."
                
                                if(counter == 1)
                                {
//                                    193 208 198
                                    cell.lblStatusTitle.textColor = UIColor.lightGray;
                                    cell.lblStatusDescription.textColor = UIColor.lightGray;
                                    cell.imgCheck.image = UIImage.init(named: "Unselected-status")
                                }
                                else
                                {
                                   
                                    imgStatus.image = UIImage(named: "status2-bg")
//                                    cell.lblStatusTitle.textColor = UIColor.black;
                                    
                                    cell.lblStatusTitle.textColor = UIColor.init(red: 45 / 255, green: 54 / 255, blue: 57 / 255, alpha: 1.0);
                                    cell.lblStatusDescription.textColor = UIColor.init(red: 56.0 / 255, green: 56.0 / 255, blue: 56.0 / 255, alpha: 1.0);

//                                    cell.lblStatusDescription.textColor = UIColor.init(red: 56.0 / 255, green: 56.0 / 255, blue: 56.0 / 255, alpha: 1.0)
                                    cell.imgCheck.image = UIImage.init(named: "Checkmark-map")
                                    
                
                                }
                return cell
            }
            else if(indexPath.row == 2)
            {
                            self.tableViewMain.register(UINib(nibName: "BusStatusRightCell", bundle: nil), forCellReuseIdentifier: "BusStatusRightCell")
                            let cell = tableViewMain.dequeueReusableCell(withIdentifier: "BusStatusRightCell", for: indexPath) as! BusStatusRightCell
                if(counter == 1 || counter == 2)
                                {
                                    cell.lblStatusTitle.textColor = UIColor.lightGray;
                                    cell.lblStatusDescription.textColor = UIColor.lightGray;
                                    cell.imgCheck.image = UIImage.init(named: "Unselected-status")
                                }
                                else
                                {
//                                    cell.lblStatusTitle.textColor = UIColor.black;
                                    imgStatus.image = UIImage(named: "Status3-bg")
                                    cell.lblStatusTitle.textColor = UIColor.init(red: 45 / 255, green: 54 / 255, blue: 57 / 255, alpha: 1.0);
                                    cell.lblStatusDescription.textColor = UIColor.init(red: 56.0 / 255, green: 56.0 / 255, blue: 56.0 / 255, alpha: 1.0);
//                                    cell.lblStatusDescription.textColor = UIColor.black;
                                    if(counter == 3)
                                    {
                                    cell.imgCheck.image = UIImage.init(named: "Watch-icon")
                                        if etaCheck <= 5 {
                                            cell.btnStatusTime.isHidden = false
                                            cell.btnStatusTime.setTitle("\(etaEstimate) Mintues", for: .normal)
                                        }else {
                                            cell.btnStatusTime.isHidden = true
                                        }
                                   
                                    }
                                    else
                                    {
                                        cell.btnStatusTime.isHidden = true
                                    }
                                }
                                cell.lblStatusTitle.text = "On the way"
                               if etaCheck <= 5{
                                cell.lblStatusDescription.text = "Bus is on the way to school \nand will be there in \(etaEstimate) minutes                                   \n       "
                    
                               }else {
                                cell.lblStatusDescription.text = "Bus is on the way to school."
                }
                
               
                return cell
            }
            else
            {
                            self.tableViewMain.register(UINib(nibName: "BusStatusLeftCell", bundle: nil), forCellReuseIdentifier: "BusStatusLeftCell")
                            let cell = tableViewMain.dequeueReusableCell(withIdentifier: "BusStatusLeftCell", for: indexPath) as! BusStatusLeftCell
                if(counter == 1 || counter == 2 || counter == 3)
                                {
                                    cell.lblStatusTitle.textColor = UIColor.lightGray;
                                    cell.lblStatusDescription.textColor = UIColor.lightGray;
                                    cell.imgCheck.image = UIImage.init(named: "Unselected-status")
                                    self.btnRealTimeTracking.isHidden = false
                                }
                                else
                                {
                                    imgStatus.image = UIImage(named: "status4-bg")
//                                    cell.lblStatusTitle.textColor = UIColor.black;
//                                    cell.lblStatusDescription.textColor = UIColor.black;
                                    cell.lblStatusTitle.textColor = UIColor.init(red: 45 / 255, green: 54 / 255, blue: 57 / 255, alpha: 1.0);
                                    cell.lblStatusDescription.textColor = UIColor.init(red: 56.0 / 255, green: 56.0 / 255, blue: 56.0 / 255, alpha: 1.0);
                                    cell.imgCheck.image = UIImage.init(named: "Checkmark-map")
                                    self.btnRealTimeTracking.isHidden = true
                                    
                                }
                
                                cell.lblStatusTitle.text = "Reached"
                                cell.lblStatusDescription.text = "Bus has reached \n the school \n"
                                cell.vuLine.isHidden = true
                return cell
            }
        }
        else
        {
        //Evening shift statuses here
            if(indexPath.row == 0)
            {
                self.tableViewMain.register(UINib(nibName: "BusStatusRightCell", bundle: nil), forCellReuseIdentifier: "BusStatusRightCell")
                let cell = tableViewMain.dequeueReusableCell(withIdentifier: "BusStatusRightCell", for: indexPath) as! BusStatusRightCell
                
                cell.lblStatusTitle.text = "School is over"
                
                cell.lblStatusDescription.text = "School is over, and bus is \nwaiting for \(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "fullname") as! String) \nto hop in"
                imgStatus.image = UIImage(named: "status4-bg")
                cell.lblStatusTitle.textColor = UIColor.init(red: 45 / 255, green: 54 / 255, blue: 57 / 255, alpha: 1.0);
                cell.lblStatusDescription.textColor = UIColor.init(red: 56.0 / 255, green: 56.0 / 255, blue: 56.0 / 255, alpha: 1.0);
//                 cell.lblStatusDescription.textColor = UIColor.black;
                return cell
            }
            else if(indexPath.row == 1)
            {
                self.tableViewMain.register(UINib(nibName: "BusStatusLeftCell", bundle: nil), forCellReuseIdentifier: "BusStatusLeftCell")
                let cell = tableViewMain.dequeueReusableCell(withIdentifier: "BusStatusLeftCell", for: indexPath) as! BusStatusLeftCell
                cell.lblStatusTitle.text = "In the Bus"
                
                if etaCheck <= 5{
                    cell.lblStatusDescription.text = "\(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "fullname") as! String) is in the bus \n and will reach in around \(etaEstimate) \nminutes."
                }else {
                    cell.lblStatusDescription.text = "\(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "fullname") as! String) is in the bus. and will rech in few mintues"
                }
                
                
                if(counter == 1)
                {
                    cell.lblStatusTitle.textColor = UIColor.lightGray;
                    cell.lblStatusDescription.textColor = UIColor.lightGray;
                    cell.imgCheck.image = UIImage.init(named: "Unselected-status")
                }
                else
                {
                    
                    imgStatus.image = UIImage(named: "status2-bg")
//                    cell.lblStatusTitle.textColor = UIColor.black;
//                    cell.lblStatusDescription.textColor = UIColor.black;
                    cell.lblStatusTitle.textColor = UIColor.init(red: 45 / 255, green: 54 / 255, blue: 57 / 255, alpha: 1.0);
                    cell.lblStatusDescription.textColor = UIColor.init(red: 56.0 / 255, green: 56.0 / 255, blue: 56.0 / 255, alpha: 1.0);
                    
                    if(counter == 2)
                    {
                        cell.busStatusTime.isHidden = false
                        cell.busStatusTime.setTitle("\(etaEstimate) Mintues", for: .normal)
                        cell.imgCheck.image = UIImage.init(named: "Watch-icon")
                    }
                    else
                    {
                        cell.busStatusTime.isHidden = true
                        
                        cell.imgCheck.image = UIImage.init(named: "Checkmark-map")
                    }
                    
                    
                }
                return cell
            }
            else if(indexPath.row == 2)
            {
                self.tableViewMain.register(UINib(nibName: "BusStatusRightCell", bundle: nil), forCellReuseIdentifier: "BusStatusRightCell")
                let cell = tableViewMain.dequeueReusableCell(withIdentifier: "BusStatusRightCell", for: indexPath) as! BusStatusRightCell
                if(counter == 1 || counter == 2)
                {
                    cell.lblStatusTitle.textColor = UIColor.lightGray;
                    cell.lblStatusDescription.textColor = UIColor.lightGray;
                    cell.imgCheck.image = UIImage.init(named: "Unselected-status")
                }
                else
                {
//                    cell.lblStatusTitle.textColor = UIColor.black;
                    cell.lblStatusTitle.textColor = UIColor.init(red: 45 / 255, green: 54 / 255, blue: 57 / 255, alpha: 1.0);
                    cell.lblStatusDescription.textColor = UIColor.init(red: 56.0 / 255, green: 56.0 / 255, blue: 56.0 / 255, alpha: 1.0);
                    imgStatus.image = UIImage(named: "Status3-bg")
//                    cell.lblStatusDescription.textColor = UIColor.black;
                    cell.imgCheck.image = UIImage.init(named: "Checkmark-map")
                }
                cell.lblStatusTitle.text = "Almost there"
               
                cell.lblStatusDescription.text = "\(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "fullname") as! String) will reach \nhome soon      \n      "
                return cell
            }
            else
            {
                self.tableViewMain.register(UINib(nibName: "BusStatusLeftCell", bundle: nil), forCellReuseIdentifier: "BusStatusLeftCell")
                let cell = tableViewMain.dequeueReusableCell(withIdentifier: "BusStatusLeftCell", for: indexPath) as! BusStatusLeftCell
                if(counter == 1 || counter == 2 || counter == 3)
                {
                    cell.lblStatusTitle.textColor = UIColor.lightGray;
                    cell.lblStatusDescription.textColor = UIColor.lightGray;
                    cell.imgCheck.image = UIImage.init(named: "Unselected-status")
                    self.btnRealTimeTracking.isHidden = false
                }
                else
                {
                    imgStatus.image = UIImage(named: "status2-bg")
//                    cell.lblStatusTitle.textColor = UIColor.black;
//                    cell.lblStatusDescription.textColor = UIColor.black;
                    cell.lblStatusTitle.textColor = UIColor.init(red: 45 / 255, green: 54 / 255, blue: 57 / 255, alpha: 1.0);
                    cell.lblStatusDescription.textColor = UIColor.init(red: 56.0 / 255, green: 56.0 / 255, blue: 56.0 / 255, alpha: 1.0);
                    cell.imgCheck.image = UIImage.init(named: "Checkmark-map")
                    self.btnRealTimeTracking.isHidden = true
                }
                
                cell.lblStatusTitle.text = "At your doorstep"
                cell.lblStatusDescription.text = "Please open the door \n \(CurrentKidDataFirebase.shared.currentKidInformaiton.value(forKey: "fullname") as! String) is waiting \noutside       "
                cell.vuLine.isHidden = true
                return cell
            }
        }
        
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    @IBAction func btnRealTimePressed(_ sender: Any) {
        
            self.performSegue(withIdentifier: "RealTimeTrackingSegue", sender: self)
                   print("Clicked")
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
   
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


