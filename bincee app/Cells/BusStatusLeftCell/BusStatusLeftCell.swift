//
//  BusStatusLeftCell.swift
//  bincee app
//
//  Created by Apple on 10/8/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class BusStatusLeftCell: UITableViewCell {
    @IBOutlet weak var vuLine: UIView!
    
  
    @IBOutlet weak var busStatusTime: UIButton!
    @IBOutlet weak var imgCheck: UIImageView!
    
    @IBOutlet weak var lblStatusDescription: UILabel!
    @IBOutlet weak var lblStatusTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
         self.busStatusTime.layer.cornerRadius = self.busStatusTime.bounds.height / 2
        // Initialization code
    }
   

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
