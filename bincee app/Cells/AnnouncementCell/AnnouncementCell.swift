//
//  AnnouncementCell.swift
//  bincee app
//
//  Created by Apple on 10/9/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class AnnouncementCell: UITableViewCell {

    @IBOutlet weak var btnAlertDetail: UIButton!
    @IBOutlet weak var lblAlertDetail: UILabel!
    @IBOutlet weak var lblAlertTitle: UILabel!
    @IBOutlet weak var imgAlertStatus: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
