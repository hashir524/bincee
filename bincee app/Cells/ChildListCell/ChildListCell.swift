//
//  ChildListCell.swift
//  bincee app
//
//  Created by Apple on 10/30/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ChildListCell: UITableViewCell {
    @IBOutlet weak var imgChild: UIImageView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblChildName: UILabel!
    var childImageDelegate: ChangeKidProfileImageProtocol?
    @IBOutlet weak var btnChildImageChange: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func btnChildImageChangePressed(_ sender: Any) {
        var senderBtn = sender as! UIButton
        print("Sender tag \(senderBtn.tag)")
        childImageDelegate?.changeImageButtonPressed(btnIndex: senderBtn.tag)
    }
    
}
