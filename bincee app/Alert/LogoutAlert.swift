//
//  LogoutAlert.swift
//  bincee app
//
//  Created by Apple on 1/31/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import FirebaseMessaging
import Firebase
import FirebaseFirestore
class LogoutAlert: UIView {
    let db = Firestore.firestore()
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vuPopupContent: UIView!
    @IBOutlet weak var vuPopup: UIView!
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        Bundle.main.loadNibNamed("LogoutAlert", owner: self, options: nil)
        self.addSubview(self.contentView)
        self.contentView.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height + 60)
        //        self.contentView.frame = frame
        //self.layoutSubviews()
        
        self.imgBG.layer.masksToBounds = true
        self.imgBG.layoutIfNeeded()
        self.vuPopup.clipsToBounds = true
        self.vuPopup.layer.cornerRadius = 5
        self.vuPopup.layer.masksToBounds = true
        makeCornerRadius()
        if(screenHeight <= 568)
        {
            self.vuPopupContent.transform = CGAffineTransform(scaleX:0.7,y: 0.7)
        }
        else if((screenHeight <= 667))
        {
            self.vuPopupContent.transform = CGAffineTransform(scaleX:0.85,y: 0.85)
        }
        else
        {
            self.vuPopupContent.transform = CGAffineTransform(scaleX: 1,y: 1)
        }
        
        //
    }
    func makeCornerRadius() -> Void {
        self.btnLogout.layer.cornerRadius = self.btnLogout.bounds.height/2
        
        self.btnLogout.layer.shadowColor = UIColor.lightGray.cgColor
        self.btnLogout.layer.shadowRadius = 5.0
        self.btnLogout.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        self.btnLogout.layer.masksToBounds = false
        self.btnLogout.layer.shadowOpacity = 1.0
        self.btnLogout.layoutIfNeeded()
        
//        self.btnMessage.layer.shadowColor = UIColor.init(red: 35.0/255.0, green: 199.0/255.0, blue: 252.0/255.0, alpha: 0.50).cgColor
//        self.btnMessage.layer.shadowRadius = 5.0
//        self.btnMessage.layer.shadowOffset = CGSize.init(width: 0, height: 0)
//        self.btnMessage.layer.masksToBounds = false
//        self.btnMessage.layer.shadowOpacity = 1.0
//        self.btnMessage.layoutIfNeeded()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @IBAction func btnLogoutPressed(_ sender: Any) {
        self.logoutAndMakeNavigation()
    }
    
    @IBAction func btnCancelPressed(_ sender: Any) {
        self.removeFromSuperview()
    }
    func logoutAndMakeNavigation() {
        self.unSubscribeTopic()
        let domain = Bundle.main.bundleIdentifier!
        var fcmToken = UserDefaults.standard.value(forKey: "fcmToken") as! String
        let parentId : String = UserDefaults.standard.value(forKey: "userId") as! String
        let loginName = UserDefaults.standard.value(forKey: "loginName") as? String ?? ""
        let loginPass = UserDefaults.standard.value(forKey: "loginPassword") as? String ?? ""
        let rememberMeState = UserDefaults.standard.value(forKey: "rememberSelected") as? String ?? ""
            //         db.collection("token").document(parentId).delete()
        self.deleteMultipleDevices(fcmToken: fcmToken, parentId: parentId)
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
        UserDefaults.standard.set(loginName, forKey: "loginName")
        UserDefaults.standard.set(loginPass, forKey: "loginPassword")
        UserDefaults.standard.set(rememberMeState, forKey: "rememberSelected")
        UserDefaults.standard.setValue(fcmToken as! String , forKey: "fcmToken")
        UIApplication.shared.unregisterForRemoteNotifications()
        
        self.launchMainScreen()
    }
    func deleteMultipleDevices(fcmToken : String , parentId : String) {
        db.collection("token").document(parentId).collection("tokens").whereField("token", isEqualTo: fcmToken).getDocuments { (querySnapshot, err) in
            if let err = err
            {
                print("Error getting documents: \(err)")
            }
            else {
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    document.reference.delete()
                    
                }
            }
        }
    }
    func unSubscribeTopic()  {
        var schoolId : String = UserDefaults.standard.value(forKey: "school_id") as? String ?? "-1"
        schoolId =  "school-\(schoolId)"
        
        Messaging.messaging().unsubscribe(fromTopic: schoolId)
    }
    func launchMainScreen()  {
        self.window!.rootViewController?.dismiss(animated: false, completion: nil)
                self.removeFromSuperview()

    }
    
}
