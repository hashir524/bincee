//
//  CalendarAlert.swift
//  bincee app
//
//  Created by Apple on 10/8/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import FSCalendar

class CalendarAlert: UIView,UITableViewDataSource,UITableViewDelegate,FSCalendarDelegate,FSCalendarDataSource,NetworkingDelegate {
    var leavesArray : NSArray = NSArray()
    private var datesRange: [Date]?
    private var lastDate: Date?
    private var firstDate: Date?
    @IBOutlet weak var vuParent: UIView!
    var clickDelegate : ClickEventDelegate!
    @IBOutlet weak var fsCalendarView: FSCalendar!
    @IBOutlet weak var vuContentAlert: UIView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var vuLeaveHistory: UIView!
    @IBOutlet var vuLeaveApplication: UIView!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var txtComments: UITextView!
    @IBOutlet weak var chkMorningShift: UIButton!
    @IBOutlet weak var vuCalendar: UIView!
    @IBOutlet weak var btnHistory: UIButton!
    @IBOutlet weak var btnCalendar: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var chkAfternoonShift: UIButton!
    @IBOutlet weak var tableViewHistory: UITableView!
    @IBOutlet weak var btnClose: NSLayoutConstraint!
    @IBOutlet weak var btnOk: UIButton!
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
   
        Bundle.main.loadNibNamed("CalendarAlert", owner: self, options: nil)
        self.addSubview(self.contentView)
        NetworkingFunctions.shared().networkingDelegate = self

        self.contentView.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        //        self.contentView.frame = frame
        //self.layoutSubviews()
        fsCalendarView.delegate = self
        fsCalendarView.dataSource = self
        
        self.vuContentAlert.layer.cornerRadius = 20.0
        self.vuContentAlert.clipsToBounds = true
        
        self.vuContentAlert.layer.shadowColor = UIColor.lightGray.cgColor
        self.vuContentAlert.layer.shadowRadius = 10.0
        self.vuContentAlert.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        self.vuContentAlert.layer.masksToBounds = false
        self.vuContentAlert.layer.shadowOpacity = 1.0
        self.vuContentAlert.layoutIfNeeded()
        
        self.vuContentAlert.layoutIfNeeded()
        makeCornerRadius()
        if(screenHeight <= 568)
        {
            self.vuParent.transform = CGAffineTransform(scaleX:0.7,y: 0.7)
        }
        else if((screenHeight <= 667))
        {
            self.vuParent.transform = CGAffineTransform(scaleX:0.85,y: 0.85)
        }
        else
        {
            self.vuParent.transform = CGAffineTransform(scaleX: 1,y: 1)
        }
        
        
    }
    @IBAction func btnOkPressed(_ sender: Any) {
        
//        var tabbar : UITabBarController =  UIApplication.shared.keyWindow?.rootViewController as! UITabBarController!
        self.clickDelegate.removeTabbar()
//        tabbar.selectedIndex = 0
//        self.removeFromSuperview()
        
    }
    func makeCornerRadius() -> Void {
        self.btnCalendar.layer.cornerRadius = self.btnCalendar.frame.height/2;
        self.btnDone.layer.cornerRadius = self.btnDone.frame.height/2;
        btnDone.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0).cgColor
        btnDone.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        btnDone.layer.shadowOpacity = 0.25
        btnDone.layer.shadowRadius = 5.0
       
        self.btnContinue.layer.cornerRadius = self.btnContinue.frame.height/2;
        
        btnContinue.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0).cgColor
        btnContinue.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        btnContinue.layer.shadowOpacity = 0.25
        btnContinue.layer.shadowRadius = 5.0
        

        
        self.btnHistory.layer.cornerRadius = self.btnHistory.frame.height/2;
        self.btnOk.layer.cornerRadius = self.btnOk.frame.height/2;
        btnOk.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0).cgColor
        btnOk.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        btnOk.layer.shadowOpacity = 0.25
        btnOk.layer.shadowRadius = 5.0
        
        vuContentAlert.layer.cornerRadius = 3
        vuLeaveApplication.isHidden = true
        self.tableViewHistory.dataSource = self
        self.tableViewHistory.delegate = self
        self.vuCalendar.isHidden = false
        self.vuLeaveHistory.isHidden = true;
        fsCalendarView.allowsMultipleSelection = true
        self.fsCalendarView.bottomBorder.isHidden = true
        fsCalendarView.today = nil
        
        self.vuContentAlert.clipsToBounds = true
        self.vuContentAlert.layer.cornerRadius = 5
        self.vuContentAlert.layer.masksToBounds = true
        
        
        self.txtComments.layer.cornerRadius = 5
        self.txtComments.layer.shadowColor = UIColor.lightGray.cgColor
        self.txtComments.layer.shadowRadius = 5.0
        self.txtComments.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        self.txtComments.layer.masksToBounds = false
        self.txtComments.layer.shadowOpacity = 1.0
        self.txtComments.layoutIfNeeded()
    }
    @IBAction func btnCalenderDonePressed(_ sender: Any) {
        if(firstDate == nil && lastDate == nil)
        {
            print("No date selected")
        }
        else
        {
        vuLeaveApplication.isHidden = false
        vuCalendar.isHidden = true
        }
    }
    @IBAction func btnClosePressed(_ sender: Any) {
//        var tabbar : UITabBarController =  UIApplication.shared.keyWindow?.rootViewController?.childViewControllers.index(after: 0) as! UITabBarController!
//        tabbar.selectedIndex = 0
//        self.removeFromSuperview()
        self.clickDelegate.removeTabbar()
    }
    
    @IBAction func btnCalendarPressed(_ sender: Any) {
        btnCalendar.isSelected = !btnCalendar.isSelected
        if btnCalendar.isSelected
        {
            self.vuCalendar.isHidden = false
            self.vuLeaveHistory.isHidden = true;
            btnHistory.isSelected = false
            btnCalendar.titleLabel?.textColor = .white
            btnCalendar.backgroundColor = UIColor(red: 3.0/255, green: 199.0/255, blue: 255.0/255.0, alpha: 1)
            btnHistory.titleLabel?.textColor = .black
            btnHistory.backgroundColor = .clear
        }
    }
    @IBAction func btnHistoryPressed(_ sender: Any) {
        btnHistory.isSelected = !btnHistory.isSelected
        if btnHistory.isSelected
        {
            self.vuCalendar.isHidden = true
            self.vuLeaveHistory.isHidden = false;
            self.vuLeaveApplication.isHidden = true
            btnCalendar.isSelected = false
            btnHistory.titleLabel?.textColor = .white
            btnHistory.backgroundColor = UIColor(red: 3.0/255, green: 199.0/255, blue: 255.0/255.0, alpha: 1)
            btnCalendar.titleLabel?.textColor = .black
            btnCalendar.backgroundColor = .clear
            callApiToGetHistory()
        }
//        else
//        {
//            btnHistory.titleLabel?.textColor = .black
//            btnHistory.backgroundColor = .clear
//
//        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leavesArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            var currentChildLeaves = leavesArray.object(at: indexPath.row) as! NSDictionary
            self.tableViewHistory.register(UINib(nibName: "LeaveHistoryCell", bundle: nil), forCellReuseIdentifier: "LeaveHistoryCell")
            let cell = tableViewHistory.dequeueReusableCell(withIdentifier: "LeaveHistoryCell", for: indexPath) as! LeaveHistoryCell
//            if(indexPath.row == 1)
//            {
//                cell.lblTitle.text
//                cell.lblStatusTitle.text = "At the Location"
//                cell.lblStatusDescription.text = "Driver is at your location \n to pickup Ahmad"
//            }
//            else
//            {
        cell.lblTitle.text = currentChildLeaves.value(forKey: "to_date") as! String
        cell.lblApplicationDescription.text = "My child can not come to school today"
//            }
            return cell
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // nothing selected:
        if firstDate == nil {
            firstDate = date
            datesRange = [firstDate!]
            
            print("datesRange contains: \(datesRange!)")
            
            return
        }
        
        // only first date is selected:
        if firstDate != nil && lastDate == nil {
            // handle the case of if the last date is less than the first date:
            if date <= firstDate! {
                calendar.deselect(firstDate!)
                firstDate = date
                datesRange = [firstDate!]
                
                print("datesRange contains: \(datesRange!)")
                
                return
            }
            
            let range = datesRange(from: firstDate!, to: date)
            
            lastDate = range.last
            
            for d in range {
                calendar.select(d)
            }
            
            datesRange = range
            
            print("datesRange contains: \(datesRange!)")
            
            return
        }
        
        // both are selected:
        if firstDate != nil && lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            
            lastDate = nil
            firstDate = nil
            
            datesRange = []
            
            print("datesRange contains: \(datesRange!)")
        }
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // both are selected:
        
        // NOTE: the is a REDUANDENT CODE:
        if firstDate != nil && lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            
            lastDate = nil
            firstDate = nil
            
            datesRange = []
            print("datesRange contains: \(datesRange!)")
        }
    }
    func datesRange(from: Date, to: Date) -> [Date] {
        // in case of the "from" date is more than "to" date,
        // it should returns an empty array:
        if from > to { return [Date]() }
        
        var tempDate = from
        var array = [tempDate]
        
        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }
        
        return array
    }
    @IBAction func btnMorningShiftClicked(_ sender: Any) {
        self.chkMorningShift.isSelected = !self.chkMorningShift.isSelected
    }
    @IBAction func btnAfternoonShiftClicked(_ sender: Any) {
    self.chkAfternoonShift.isSelected = !self.chkAfternoonShift.isSelected
     
    }
    func callApiForApplyingLeave()  {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        print("Converted Date")
        if(firstDate != nil)
        {
        let convertedDateFirst = dateFormatter.string(from: firstDate!)
        }
        if(lastDate != nil)
        {
            let convertedDateLast = dateFormatter.string(from: lastDate!)
        }
    }
    func callApiToGetHistory() {
        
        var selectedKidId : NSNumber = UserDefaults.standard.value(forKey: "selectedKidId") as! NSNumber
                NetworkingFunctions.shared()?.getJsonData("http://access.bincee.com/school/student/leaves/\(selectedKidId)", withParams: nil, withBearer: true)
    }
    func onGetDataReceive(_ data: Any!) {
       var dataConverted = data as! NSDictionary
        if(dataConverted.value(forKey: "status") as! NSNumber == 200)
        {
            leavesArray = dataConverted.value(forKey: "data") as! NSArray
            print("Data converted count \(dataConverted.count)")
        }
        else{
        
        }
        self.tableViewHistory.reloadData()
        print("data received")
    }
    func onFailure(_ failureError: Any!) {
        print("Error Occured")
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
