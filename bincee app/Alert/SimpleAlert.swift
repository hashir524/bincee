//
//  SimpleAlert.swift
//  bincee app
//
//  Created by Apple on 10/5/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class SimpleAlert: UIView {

    @IBOutlet weak var vuParent: UIView!
    @IBOutlet weak var imgAnnouncement: UIImageView!
    @IBOutlet weak var vuAlertContent: UIView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblAlertTitle: UILabel!
    @IBOutlet weak var txtAlert: UITextView!
    @IBOutlet weak var btnOk: UIButton!
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    @IBAction func btnOkPressed(_ sender: Any) {
        print("btn close pressed")
        self.removeFromSuperview()
//        self.removeFromSuperview();
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    @IBAction func btnClosePressed(_ sender: Any) {
        print("Btn close pressed")
        self.removeFromSuperview()
    }
    @IBOutlet var contentView: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        Bundle.main.loadNibNamed("SimpleAlert", owner: self, options: nil)
        self.addSubview(self.contentView)
        self.contentView.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        updateViews()
//        self.contentView.frame = frame
        //self.layoutSubviews()
        
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func updateViews() -> Void {
        
        self.vuAlertContent.clipsToBounds = true
        self.vuAlertContent.layer.cornerRadius = 5
        self.vuAlertContent.layer.masksToBounds = true
        self.btnOk.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0).cgColor
        self.btnOk.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        self.btnOk.layer.shadowOpacity = 0.25
        self.btnOk.layer.shadowRadius = 5.0
        self.vuAlertContent.layer.shadowColor = UIColor.lightGray.cgColor
        self.vuAlertContent.layer.shadowRadius = 10.0
        self.vuAlertContent.layer.shadowOffset = CGSize.init(width: 0, height: 0)
//        self.vuAlertContent.layer.masksToBounds = false
        self.vuAlertContent.layer.shadowOpacity = 1.0
        self.vuAlertContent.layoutIfNeeded()
        self.btnOk.layer.cornerRadius = self.btnOk.frame.height/2;
        
        if(screenHeight <= 568)
        {
            self.vuParent.transform = CGAffineTransform(scaleX:0.7,y: 0.7)
        }
        else if((screenHeight <= 667))
        {
            self.vuParent.transform = CGAffineTransform(scaleX:0.85,y: 0.85)
        }
        else
        {
            self.vuParent.transform = CGAffineTransform(scaleX: 1,y: 1)
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
