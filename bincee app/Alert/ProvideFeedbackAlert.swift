//
//  ProvideFeedbackAlert.swift
//  bincee app
//
//  Created by Apple on 10/17/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ProvideFeedbackAlert: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnOneStar: UIButton!
    @IBOutlet weak var btnTwoStar: UIButton!
    @IBOutlet weak var btnThreeStar: UIButton!
    @IBOutlet weak var btnFourStar: UIButton!
    @IBOutlet weak var btnFiveStar: UIButton!
    @IBOutlet weak var txtFeedback: UITextView!
    @IBOutlet weak var vuAlertContent: UIView!
    @IBOutlet weak var btnNoThanks: UIButton!
    @IBOutlet var contentView: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        Bundle.main.loadNibNamed("ProvideFeedbackAlert", owner: self, options: nil)
        self.addSubview(self.contentView)
        self.contentView.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        
        updateViews()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func updateViews() -> Void {
        
       
        self.btnDone.layer.cornerRadius = self.btnDone.bounds.height / 2
        
        self.btnNoThanks.layer.cornerRadius = self.btnNoThanks.bounds.height / 2
        self.vuAlertContent.clipsToBounds = true
        self.vuAlertContent.layer.cornerRadius = 5
        self.vuAlertContent.layer.masksToBounds = true
        
        
        self.txtFeedback.layer.cornerRadius = 5.0
        self.txtFeedback.clipsToBounds = true
        
        self.txtFeedback.layer.shadowColor = UIColor.lightGray.cgColor
        self.txtFeedback.layer.shadowRadius = 2.0
        self.txtFeedback.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        self.txtFeedback.layer.masksToBounds = false
        self.txtFeedback.layer.shadowOpacity = 1.0
        self.txtFeedback.layoutIfNeeded()
        
        self.txtFeedback.layoutIfNeeded()
    }
    
    
    @IBAction func btnOneStarPressed(_ sender: Any) {
        
        btnOneStar.isSelected = !btnOneStar.isSelected
        if(btnOneStar.isSelected)
        {
            btnTwoStar.isSelected = false
            btnThreeStar.isSelected = false
            btnFourStar.isSelected = false
            btnFiveStar.isSelected = false
        }
        else
        {
            btnTwoStar.isSelected = false
            btnThreeStar.isSelected = false
            btnFourStar.isSelected = false
            btnFiveStar.isSelected = false
        }
        
    }
    @IBAction func btnTwoStarPressed(_ sender: Any) {
        btnTwoStar.isSelected = !btnTwoStar.isSelected
        if(btnTwoStar.isSelected)
        {
            btnThreeStar.isSelected = false
            btnFourStar.isSelected = false
            btnFiveStar.isSelected = false
            btnOneStar.isSelected = true
        }
        else
        {
            btnThreeStar.isSelected = false
            btnFourStar.isSelected = false
            btnFiveStar.isSelected = false
        }
        
    }
    @IBAction func btnThreeStarPressed(_ sender: Any) {
        btnThreeStar.isSelected = !btnThreeStar.isSelected
        if(btnThreeStar.isSelected)
        {
            btnFourStar.isSelected = false
            btnFiveStar.isSelected = false
            btnTwoStar.isSelected = true
            btnOneStar.isSelected = true
        }
        else
        {
            btnTwoStar.isSelected = true
            btnOneStar.isSelected = true
        }
    }
    @IBAction func btnFourStarPressed(_ sender: Any) {
        btnFourStar.isSelected = !btnFourStar.isSelected
        if(btnFourStar.isSelected)
        {
            btnFiveStar.isSelected = false
            btnOneStar.isSelected = true
            btnTwoStar.isSelected = true
            btnThreeStar.isSelected = true
        }
        else
        {
             btnFiveStar.isSelected = false
        }
    }
    @IBAction func btnFiveStarPressed(_ sender: Any) {
         btnFiveStar.isSelected = !btnFiveStar.isSelected
        if(btnFiveStar.isSelected)
        {
            btnFourStar.isSelected = true
            btnOneStar.isSelected = true
            btnTwoStar.isSelected = true
            btnThreeStar.isSelected = true
        }
    }
    @IBAction func btnClosePressed(_ sender: Any) {
        self.removeFromSuperview()
    }
}
