//
//  DriveInformationAlert.swift
//  bincee app
//
//  Created by Apple on 10/9/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class DriveInformationAlert: UIView {
    var driverInformation : NSDictionary = NSDictionary()
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    @IBOutlet weak var vuPopup: UIView!
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var lblDrivePhone: UILabel!
    @IBOutlet weak var vuPopupContent: UIView!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnMessage: UIButton!
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        Bundle.main.loadNibNamed("DriverInformationAlert", owner: self, options: nil)
        self.addSubview(self.contentView)
        self.contentView.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        //        self.contentView.frame = frame
        //self.layoutSubviews()
        
        self.imgBG.layer.masksToBounds = true
        self.imgBG.layoutIfNeeded()
        self.vuPopup.clipsToBounds = true
        self.vuPopup.layer.cornerRadius = 5
        self.vuPopup.layer.masksToBounds = true
        makeCornerRadius()
       if(screenHeight <= 568)
       {
        self.vuPopupContent.transform = CGAffineTransform(scaleX:0.7,y: 0.7)
        }
        else if((screenHeight <= 667))
       {
        self.vuPopupContent.transform = CGAffineTransform(scaleX:0.85,y: 0.85)
        }
        else
       {
        self.vuPopupContent.transform = CGAffineTransform(scaleX: 1,y: 1)
        }
        driverInformation = DriverInformation.shared.driverInformationCurrent
        lblDriverName.text = driverInformation.value(forKey: "fullname") as? String
        lblDrivePhone.text = driverInformation.value(forKey: "phone_no") as? String
//
    }
    func makeCornerRadius() -> Void {
        self.btnCall.layer.cornerRadius = self.btnCall.bounds.height/2
        self.btnMessage.layer.cornerRadius = self.btnMessage.bounds.height/2
        self.btnCall.layer.shadowColor = UIColor.lightGray.cgColor
        self.btnCall.layer.shadowRadius = 5.0
        self.btnCall.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        self.btnCall.layer.masksToBounds = false
        self.btnCall.layer.shadowOpacity = 1.0
        self.btnCall.layoutIfNeeded()
        
        self.btnMessage.layer.shadowColor = UIColor.init(red: 35.0/255.0, green: 199.0/255.0, blue: 252.0/255.0, alpha: 0.50).cgColor
        self.btnMessage.layer.shadowRadius = 5.0
        self.btnMessage.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        self.btnMessage.layer.masksToBounds = false
        self.btnMessage.layer.shadowOpacity = 1.0
        self.btnMessage.layoutIfNeeded()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @IBAction func btnCallPressed(_ sender: Any) {
        var phoneNumber : String = "tel://\(self.lblDrivePhone.text ?? "")"
        print(phoneNumber)
        let url: NSURL = URL(string: phoneNumber)! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }
    @IBAction func btnClosePressed(_ sender: Any) {
        self.removeFromSuperview()
    }
    @IBAction func btnMessagePressed(_ sender: Any) {
//        UIApplication.shared.open(URL(string: "sms:")!, options: [:], completionHandler: nil)
        
        let number = "sms:\(self.lblDrivePhone.text ?? "")"
        UIApplication.shared.openURL(NSURL(string: number)! as URL)

    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
