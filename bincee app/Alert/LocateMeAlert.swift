//
//  LocateMeAlert.swift
//  bincee app
//
//  Created by Apple on 10/9/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class LocateMeAlert: UIView {
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var vuLocateMe: UIView!
    @IBOutlet weak var btnLocateMe: UIButton!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var vuBlur: UIView!
    var delegate: LocateMeDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        Bundle.main.loadNibNamed("LocateMeAlert", owner: self, options: nil)
        self.addSubview(self.contentView)
        self.contentView.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        self.btnLocateMe.layer.cornerRadius = self.btnLocateMe.bounds.height / 2
        updateViews()
        
    }
    func updateViews() -> Void {
        
        self.vuLocateMe.clipsToBounds = true
        self.vuLocateMe.layer.cornerRadius = 5
        self.vuLocateMe.layer.masksToBounds = true
    }
    @IBAction func btnClosePressed(_ sender: Any) {
        self.removeFromSuperview()
    }
    @IBAction func btnLocateMePressed(_ sender: Any) {
        self.btnClosePressed(self.btnClose)
        delegate?.didTapButton()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
