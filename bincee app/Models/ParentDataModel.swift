//
//  ParentDataModel.swift
//  bincee app
//
//  Created by Apple on 12/6/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ParentDataModel: NSObject {
   
//        let status: Int
//        let data: DataClass
//        
//        init(status: Int, data: DataClass) {
//            self.status = status
//            self.data = data
//        }
//    }
//    
//    class DataClass: Codable {
//        let parentID: Int
//        let fullname, phoneNo, address, email: String
//        let lat, lng: Double
//        let status: String
//        let photo: String
//        let schoolID: Int
//        let kids: [Kid]
//        
//        enum CodingKeys: String, CodingKey {
//            case parentID = "parent_id"
//            case fullname
//            case phoneNo = "phone_no"
//            case address, email, lat, lng, status, photo
//            case schoolID = "school_id"
//            case kids
//        }
//        
//        init(parentID: Int, fullname: String, phoneNo: String, address: String, email: String, lat: Double, lng: Double, status: String, photo: String, schoolID: Int, kids: [Kid]) {
//            self.parentID = parentID
//            self.fullname = fullname
//            self.phoneNo = phoneNo
//            self.address = address
//            self.email = email
//            self.lat = lat
//            self.lng = lng
//            self.status = status
//            self.photo = photo
//            self.schoolID = schoolID
//            self.kids = kids
//        }
//    }
//    
//    class Kid: Codable {
//        let id: Int
//        let fullname: String
//        let grade: Grade
//        let photo: JSONNull?
//        let shift: Shift
//        let parentID, driverID: Int
//        let status: String
//        let driver: Driver
//        
//        enum CodingKeys: String, CodingKey {
//            case id, fullname, grade, photo, shift
//            case parentID = "parent_id"
//            case driverID = "driver_id"
//            case status, driver
//        }
//        
//        init(id: Int, fullname: String, grade: Grade, photo: JSONNull?, shift: Shift, parentID: Int, driverID: Int, status: String, driver: Driver) {
//            self.id = id
//            self.fullname = fullname
//            self.grade = grade
//            self.photo = photo
//            self.shift = shift
//            self.parentID = parentID
//            self.driverID = driverID
//            self.status = status
//            self.driver = driver
//        }
//    }
//    
//    class Driver: Codable {
//        let driverID: Int
//        let fullname, phoneNo: String
//        let photo: String?
//        let schoolID: Int
//        let status: String
//        
//        enum CodingKeys: String, CodingKey {
//            case driverID = "driver_id"
//            case fullname
//            case phoneNo = "phone_no"
//            case photo
//            case schoolID = "school_id"
//            case status
//        }
//        
//        init(driverID: Int, fullname: String, phoneNo: String, photo: String?, schoolID: Int, status: String) {
//            self.driverID = driverID
//            self.fullname = fullname
//            self.phoneNo = phoneNo
//            self.photo = photo
//            self.schoolID = schoolID
//            self.status = status
//        }
//    }
//    
//    class Grade: Codable {
//        let gradeID: Int
//        let gradeName, section, gradeSection: String
//        let schoolID: Int
//        
//        enum CodingKeys: String, CodingKey {
//            case gradeID = "grade_id"
//            case gradeName = "grade_name"
//            case section
//            case gradeSection = "grade_section"
//            case schoolID = "school_id"
//        }
//        
//        init(gradeID: Int, gradeName: String, section: String, gradeSection: String, schoolID: Int) {
//            self.gradeID = gradeID
//            self.gradeName = gradeName
//            self.section = section
//            self.gradeSection = gradeSection
//            self.schoolID = schoolID
//        }
//    }
//    
//    class Shift: Codable {
//        let shiftID: Int
//        let shiftName, startTime, endTime: String
//        let schoolID: Int
//        
//        enum CodingKeys: String, CodingKey {
//            case shiftID = "shift_id"
//            case shiftName = "shift_name"
//            case startTime = "start_time"
//            case endTime = "end_time"
//            case schoolID = "school_id"
//        }
//        
//        init(shiftID: Int, shiftName: String, startTime: String, endTime: String, schoolID: Int) {
//            self.shiftID = shiftID
//            self.shiftName = shiftName
//            self.startTime = startTime
//            self.endTime = endTime
//            self.schoolID = schoolID
//        }
//    }
//    
////    class School: Codable {
////        let schoolID: Int
////        let address, name, phoneNo: String
////        let lat, lng: Double
////
////        enum CodingKeys: String, CodingKey {
////            case schoolID = "school_id"
////            case address, name
////            case phoneNo = "phone_no"
////            case lat, lng
////        }
////
////        init(schoolID: Int, address: String, name: String, phoneNo: String, lat: Double, lng: Double) {
////            self.schoolID = schoolID
////            self.address = address
////            self.name = name
////            self.phoneNo = phoneNo
////            self.lat = lat
////            self.lng = lng
////        }
////    }
//
//    // MARK: Encode/decode helpers
//    
//    class JSONNull: Codable, Hashable {
//        
//        public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
//            return true
//        }
//        
//        public var hashValue: Int {
//            return 0
//        }
//        
//        public init() {}
//        
//        public required init(from decoder: Decoder) throws {
//            let container = try decoder.singleValueContainer()
//            if !container.decodeNil() {
//                throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
//            }
//        }
//        
//        public func encode(to encoder: Encoder) throws {
//            var container = encoder.singleValueContainer()
//            try container.encodeNil()
//        }
    }

