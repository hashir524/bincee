//
//	response_School.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class response_School : NSObject, NSCoding, Mappable{

	var address : String?
	var lat : Float?
	var lng : Float?
	var name : String?
	var phoneNo : String?
	var schoolId : Int?


	class func newInstance(map: Map) -> Mappable?{
		return response_School()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		address <- map["address"]
		lat <- map["lat"]
		lng <- map["lng"]
		name <- map["name"]
		phoneNo <- map["phone_no"]
		schoolId <- map["school_id"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? String
         lat = aDecoder.decodeObject(forKey: "lat") as? Float
         lng = aDecoder.decodeObject(forKey: "lng") as? Float
         name = aDecoder.decodeObject(forKey: "name") as? String
         phoneNo = aDecoder.decodeObject(forKey: "phone_no") as? String
         schoolId = aDecoder.decodeObject(forKey: "school_id") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if lat != nil{
			aCoder.encode(lat, forKey: "lat")
		}
		if lng != nil{
			aCoder.encode(lng, forKey: "lng")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if phoneNo != nil{
			aCoder.encode(phoneNo, forKey: "phone_no")
		}
		if schoolId != nil{
			aCoder.encode(schoolId, forKey: "school_id")
		}

	}

}