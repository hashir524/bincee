//
//	response_Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class response_Data : NSObject, NSCoding, Mappable{

	var address : String?
	var email : String?
	var fullname : String?
	var kids : [response_Kid]?
	var lat : Float?
	var lng : Float?
	var parentId : Int?
	var phoneNo : String?
	var photo : String?
	var school : response_School?
	var schoolId : Int?
	var status : String?


	class func newInstance(map: Map) -> Mappable?{
		return response_Data()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		address <- map["address"]
		email <- map["email"]
		fullname <- map["fullname"]
		kids <- map["kids"]
		lat <- map["lat"]
		lng <- map["lng"]
		parentId <- map["parent_id"]
		phoneNo <- map["phone_no"]
		photo <- map["photo"]
		school <- map["school"]
		schoolId <- map["school_id"]
		status <- map["status"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         fullname = aDecoder.decodeObject(forKey: "fullname") as? String
         kids = aDecoder.decodeObject(forKey: "kids") as? [response_Kid]
         lat = aDecoder.decodeObject(forKey: "lat") as? Float
         lng = aDecoder.decodeObject(forKey: "lng") as? Float
         parentId = aDecoder.decodeObject(forKey: "parent_id") as? Int
         phoneNo = aDecoder.decodeObject(forKey: "phone_no") as? String
         photo = aDecoder.decodeObject(forKey: "photo") as? String
         school = aDecoder.decodeObject(forKey: "school") as? response_School
         schoolId = aDecoder.decodeObject(forKey: "school_id") as? Int
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if fullname != nil{
			aCoder.encode(fullname, forKey: "fullname")
		}
		if kids != nil{
			aCoder.encode(kids, forKey: "kids")
		}
		if lat != nil{
			aCoder.encode(lat, forKey: "lat")
		}
		if lng != nil{
			aCoder.encode(lng, forKey: "lng")
		}
		if parentId != nil{
			aCoder.encode(parentId, forKey: "parent_id")
		}
		if phoneNo != nil{
			aCoder.encode(phoneNo, forKey: "phone_no")
		}
		if photo != nil{
			aCoder.encode(photo, forKey: "photo")
		}
		if school != nil{
			aCoder.encode(school, forKey: "school")
		}
		if schoolId != nil{
			aCoder.encode(schoolId, forKey: "school_id")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}