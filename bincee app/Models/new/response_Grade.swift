//
//	response_Grade.swift
//	Model file generated using JSONExport:

import Foundation 
import ObjectMapper


class response_Grade : NSObject, NSCoding, Mappable{

	var gradeId : Int?
	var gradeName : String?
	var gradeSection : String?
	var schoolId : Int?
	var section : String?


	class func newInstance(map: Map) -> Mappable?{
		return response_Grade()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		gradeId <- map["grade_id"]
		gradeName <- map["grade_name"]
		gradeSection <- map["grade_section"]
		schoolId <- map["school_id"]
		section <- map["section"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         gradeId = aDecoder.decodeObject(forKey: "grade_id") as? Int
         gradeName = aDecoder.decodeObject(forKey: "grade_name") as? String
         gradeSection = aDecoder.decodeObject(forKey: "grade_section") as? String
         schoolId = aDecoder.decodeObject(forKey: "school_id") as? Int
         section = aDecoder.decodeObject(forKey: "section") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if gradeId != nil{
			aCoder.encode(gradeId, forKey: "grade_id")
		}
		if gradeName != nil{
			aCoder.encode(gradeName, forKey: "grade_name")
		}
		if gradeSection != nil{
			aCoder.encode(gradeSection, forKey: "grade_section")
		}
		if schoolId != nil{
			aCoder.encode(schoolId, forKey: "school_id")
		}
		if section != nil{
			aCoder.encode(section, forKey: "section")
		}

	}

}
