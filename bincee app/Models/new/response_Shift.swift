//
//	response_Shift.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class response_Shift : NSObject, NSCoding, Mappable{

	var endTime : String?
	var schoolId : Int?
	var shiftId : Int?
	var shiftName : String?
	var startTime : String?


	class func newInstance(map: Map) -> Mappable?{
		return response_Shift()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		endTime <- map["end_time"]
		schoolId <- map["school_id"]
		shiftId <- map["shift_id"]
		shiftName <- map["shift_name"]
		startTime <- map["start_time"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         endTime = aDecoder.decodeObject(forKey: "end_time") as? String
         schoolId = aDecoder.decodeObject(forKey: "school_id") as? Int
         shiftId = aDecoder.decodeObject(forKey: "shift_id") as? Int
         shiftName = aDecoder.decodeObject(forKey: "shift_name") as? String
         startTime = aDecoder.decodeObject(forKey: "start_time") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if endTime != nil{
			aCoder.encode(endTime, forKey: "end_time")
		}
		if schoolId != nil{
			aCoder.encode(schoolId, forKey: "school_id")
		}
		if shiftId != nil{
			aCoder.encode(shiftId, forKey: "shift_id")
		}
		if shiftName != nil{
			aCoder.encode(shiftName, forKey: "shift_name")
		}
		if startTime != nil{
			aCoder.encode(startTime, forKey: "start_time")
		}

	}

}