//
//	response_Driver.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class response_Driver : NSObject, NSCoding, Mappable{

	var driverId : Int?
	var fullname : String?
	var phoneNo : String?
	var photo : String?
	var schoolId : Int?
	var status : String?


	class func newInstance(map: Map) -> Mappable?{
		return response_Driver()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		driverId <- map["driver_id"]
		fullname <- map["fullname"]
		phoneNo <- map["phone_no"]
		photo <- map["photo"]
		schoolId <- map["school_id"]
		status <- map["status"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         driverId = aDecoder.decodeObject(forKey: "driver_id") as? Int
         fullname = aDecoder.decodeObject(forKey: "fullname") as? String
         phoneNo = aDecoder.decodeObject(forKey: "phone_no") as? String
         photo = aDecoder.decodeObject(forKey: "photo") as? String
         schoolId = aDecoder.decodeObject(forKey: "school_id") as? Int
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if driverId != nil{
			aCoder.encode(driverId, forKey: "driver_id")
		}
		if fullname != nil{
			aCoder.encode(fullname, forKey: "fullname")
		}
		if phoneNo != nil{
			aCoder.encode(phoneNo, forKey: "phone_no")
		}
		if photo != nil{
			aCoder.encode(photo, forKey: "photo")
		}
		if schoolId != nil{
			aCoder.encode(schoolId, forKey: "school_id")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}