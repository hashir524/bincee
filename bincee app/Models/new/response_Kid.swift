//
//	response_Kid.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class response_Kid : NSObject, NSCoding, Mappable{

	var driver : response_Driver?
	var driverId : Int?
	var fullname : String?
	var grade : response_Grade?
	var id : Int?
	var parentId : Int?
	var photo : AnyObject?
	var shift : response_Shift?
	var status : String?


	class func newInstance(map: Map) -> Mappable?{
		return response_Kid()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		driver <- map["driver"]
		driverId <- map["driver_id"]
		fullname <- map["fullname"]
		grade <- map["grade"]
		id <- map["id"]
		parentId <- map["parent_id"]
		photo <- map["photo"]
		shift <- map["shift"]
		status <- map["status"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         driver = aDecoder.decodeObject(forKey: "driver") as? response_Driver
         driverId = aDecoder.decodeObject(forKey: "driver_id") as? Int
         fullname = aDecoder.decodeObject(forKey: "fullname") as? String
         grade = aDecoder.decodeObject(forKey: "grade") as? response_Grade
         id = aDecoder.decodeObject(forKey: "id") as? Int
         parentId = aDecoder.decodeObject(forKey: "parent_id") as? Int
         photo = aDecoder.decodeObject(forKey: "photo") as? AnyObject
         shift = aDecoder.decodeObject(forKey: "shift") as? response_Shift
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if driver != nil{
			aCoder.encode(driver, forKey: "driver")
		}
		if driverId != nil{
			aCoder.encode(driverId, forKey: "driver_id")
		}
		if fullname != nil{
			aCoder.encode(fullname, forKey: "fullname")
		}
		if grade != nil{
			aCoder.encode(grade, forKey: "grade")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if parentId != nil{
			aCoder.encode(parentId, forKey: "parent_id")
		}
		if photo != nil{
			aCoder.encode(photo, forKey: "photo")
		}
		if shift != nil{
			aCoder.encode(shift, forKey: "shift")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}