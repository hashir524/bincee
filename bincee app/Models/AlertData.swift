//
//  AlertData.swift
//  bincee app
//
//  Created by Hashir Saeed on 14/05/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import ObjectMapper


class AlertData : NSObject, NSCoding, Mappable{
    
    var data : [Data]?
    var studentId : Int?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return AlertData()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        data <- map["data"]
        studentId <- map["student_id"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        data = aDecoder.decodeObject(forKey: "data") as? [Data]
        studentId = aDecoder.decodeObject(forKey: "student_id") as? Int
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if data != nil{
            aCoder.encode(data, forKey: "data")
        }
        if studentId != nil{
            aCoder.encode(studentId, forKey: "student_id")
        }
        
    }
    
}
