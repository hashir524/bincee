//
//  AlertResponseModel.swift
//  bincee app
//
//  Created by Hashir Saeed on 14/05/2019.
//  Copyright © 2019 Apple. All rights reserved.
import Foundation
import ObjectMapper


class AlertResponseModel : NSObject, NSCoding, Mappable{
    
    var alertData : [AlertData]?
    var status : Int?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return AlertResponseModel()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        alertData <- map["data"]
        status <- map["status"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        alertData = aDecoder.decodeObject(forKey: "data") as? [AlertData]
        status = aDecoder.decodeObject(forKey: "status") as? Int
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if alertData != nil{
            aCoder.encode(alertData, forKey: "data")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        
    }
    
}
