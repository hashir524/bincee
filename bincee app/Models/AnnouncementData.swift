//
//  AnnouncementData.swift
//  bincee app
//
//  Created by Hashir Saeed on 14/05/2019.
//  Copyright © 2019 Apple. All rights reserved.


import Foundation
import ObjectMapper


class AnnouncementData : NSObject, NSCoding, Mappable{
    
    var descriptionField : String?
    var id : Int?
    var lastUpdated : String?
    var schoolId : Int?
    var title : String?
    var type : String?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return AnnouncementData()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        descriptionField <- map["description"]
        id <- map["id"]
        lastUpdated <- map["last_updated"]
        schoolId <- map["school_id"]
        title <- map["title"]
        type <- map["type"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        descriptionField = aDecoder.decodeObject(forKey: "description") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        lastUpdated = aDecoder.decodeObject(forKey: "last_updated") as? String
        schoolId = aDecoder.decodeObject(forKey: "school_id") as? Int
        title = aDecoder.decodeObject(forKey: "title") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if lastUpdated != nil{
            aCoder.encode(lastUpdated, forKey: "last_updated")
        }
        if schoolId != nil{
            aCoder.encode(schoolId, forKey: "school_id")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }
        
    }
    
}
