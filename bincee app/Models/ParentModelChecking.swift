//
//  ParentModelChecking.swift
//  bincee app
//
//  Created by Apple on 12/13/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ParentModelChecking : NSObject {
    
    private override init() { }
    var status: Int?
    var data: DataClass?
    // MARK: Shared Instance
    static let shared = ParentModelChecking()
    func setValue(status : Int , data : DataClass){
      self.status = status
      self.data = data
    }
   
}
struct DataModel : Codable {
   
}
struct DataClass: Codable {
    
    let parentID: Int
    let fullname, phoneNo, address, email: String
    let lat, lng: Double
    let status: String
    let photo: String
    let schoolID: Int
    let kids: [Kid]
    
}

struct Kid: Codable {
    let id: Int
    let fullname: String
    let grade: Grade
    let photo: JSONNull?
    let shift: Shift
    let parentID, driverID: Int
    let status: String
    let driver: Driver
    
}

struct Driver: Codable {
    let driverID: Int
    let fullname, phoneNo: String
    let photo: String?
    let schoolID: Int
    let status: String
    
   
}

struct Grade: Codable {
    let gradeID: Int
    let gradeName, section, gradeSection: String
    let schoolID: Int
    
}

struct Shift: Codable {
    let shiftID: Int
    let shiftName, startTime, endTime: String
    let schoolID: Int
    
   
}

//    class School: Codable {
//        let schoolID: Int
//        let address, name, phoneNo: String
//        let lat, lng: Double
//
//        enum CodingKeys: String, CodingKey {
//            case schoolID = "school_id"
//            case address, name
//            case phoneNo = "phone_no"
//            case lat, lng
//        }
//
//        init(schoolID: Int, address: String, name: String, phoneNo: String, lat: Double, lng: Double) {
//            self.schoolID = schoolID
//            self.address = address
//            self.name = name
//            self.phoneNo = phoneNo
//            self.lat = lat
//            self.lng = lng
//        }
//    }

// MARK: Encode/decode helpers

class JSONNull: Codable, Hashable {
    
    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }
    
    public var hashValue: Int {
        return 0
    }
    
    public init() {}
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

