//
//  NotificationStatusModel.swift
//  bincee app
//
//  Created by Hashir Saeed on 19/05/2019.
//  Copyright © 2019 Apple. All rights reserved.


import Foundation
import ObjectMapper


class NotificationStatusModel : NSObject, NSCoding, Mappable{
    
    var id : Int?
    var readStatus : Bool?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return NotificationStatusModel()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        id <- map["id"]
        readStatus <- map["readStatus"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        readStatus = aDecoder.decodeObject(forKey: "readStatus") as? Bool
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if readStatus != nil{
            aCoder.encode(readStatus, forKey: "readStatus")
        }
        
    }
    
}
