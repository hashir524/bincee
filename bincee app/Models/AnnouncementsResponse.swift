//
//  AnnouncementsResponse.swift
//  bincee app
//
//  Created by Hashir Saeed on 14/05/2019.
//  Copyright © 2019 Apple. All rights reserved.
//
import Foundation
import ObjectMapper


class AnnouncementsResponse : NSObject, NSCoding, Mappable{
    
    var announcementData : [AnnouncementData]?
    var status : Int?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return AnnouncementsResponse()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        announcementData <- map["data"]
        status <- map["status"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        announcementData = aDecoder.decodeObject(forKey: "data") as? [AnnouncementData]
        status = aDecoder.decodeObject(forKey: "status") as? Int
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if announcementData != nil{
            aCoder.encode(announcementData, forKey: "data")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        
    }
    
}
