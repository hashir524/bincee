//
//  AppDelegate.swift
//  bincee app
//
//  Created by Apple on 10/5/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import FirebaseMessaging
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    let gcmMessageIDKey = "gcm.message_id"
    let gcmMessageType = "gcm.notification.type"
    let gcmMessageData = "gcm.notification.data"
    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
      
//        GMSServices.provideAPIKey("AIzaSyDX3o5wXcDGblfx_LcJhwsPH_fHH5391dE")
//        GMSServices.provideAPIKey("AIzaSyBoul0BlF39Rl7SgVLXeFwzF6v7d2tCBoQ")
        NetworkingFunctions().setupValues()
        FirebaseApp.configure()
        
        generateFcm(application)
        // Override point for customization after application launch.
        GMSPlacesClient.provideAPIKey("AIzaSyCIqCUWoNBupsq4oo54hr0Qtfp3AWPA6YE")
        
        Messaging.messaging().subscribe(toTopic: "parent_4") { error in
            print("Subscribed to weather topic")
        }
        
        return true
    }
    func generateFcm(_ application: UIApplication)
    {
        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()

    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print("********User Info Recieied From Notification is being printed here*********")
        print(userInfo)
        print("********User Info Recieied From Notification Ends Here*********")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print("********User Info Recieied From Notification is being printed here*********")
        print(userInfo)
        print("********User Info Recieied From Notification Ends Here*********")
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    // [END receive_message]
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            //check status and call
            print("Message ID: \(messageID)")
            //here call check notification
            
            self.checkNotificationType(userInfo: userInfo)
//            NotificationCenter.default.post(name: Notification.Name(rawValue: "launchTrackMyKid"), object: nil, userInfo: nil)
        }
        
        // Print full message.
        print("********User Info Recieied From Notification is being printed here*********")
        print(userInfo)
        print("********User Info Recieied From Notification Ends Here*********")
        
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
            self.checkNotificationType(userInfo: userInfo)
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
    
    func checkNotificationType(userInfo : [AnyHashable : Any])
    {
        let userInfo = userInfo
        print("user info here \(userInfo)")
        let testtype = userInfo[gcmMessageType]
        print("Notification Type Print \(testtype!)")
        if let type = userInfo[gcmMessageType]
        {
            print("Notification Type Print \(type)")
//            || (userInfo["gcm.notification.message"] as? String ?? "").contains("Bus has Reached the school")
            if((userInfo["gcm.notification.message"] as? String ?? "").contains("Please open the door") || (userInfo["gcm.notification.message"] as? String ?? "").contains("Bus has Reached the school"))
            {
                print("Reached")
                NotificationCenter.default.post(name: Notification.Name(rawValue: "launchReachedScreen"), object: nil, userInfo: nil)
                return
            }
            if(type as! String == "3")
            {
                //Ride
                print(type as! String)
            return
            }
            else if(type as! String == "2" || type as! String == "1")
            {
                 //attendance // update status
                if let data = userInfo[gcmMessageData]
                {
                    var studentData = data as! String
//                    var studentId = studentData.value(forKey: "studentId") as! Int
                    
                   studentData.removeFirst(13)
                    studentData.removeLast(1)
                    print("Student id sent \(studentData)")
                    CurrentKidDataFirebase.shared.notificationForKidId = Int(studentData)!
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "launchTrackMyKid"), object: nil, userInfo: nil)
                }
            }
            else if(type as! String == "Evening1")
            {
                if let data = userInfo["studentId"]
                {
                    var studentData = data as! String
                    //                    var studentId = studentData.value(forKey: "studentId") as! Int
                    print("Student id sent \(studentData)")
                    CurrentKidDataFirebase.shared.notificationForKidId = Int(studentData)!
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "EveningStatusReceived"), object: nil, userInfo: nil)
                }
            }
               
            else if(type as! String == "Announcement")
            {
                 NotificationCenter.default.post(name: Notification.Name(rawValue: "AnnouncementReceived"), object: nil, userInfo: nil)
                print("********Annoucment Received**********")
                
                //The notification We Re
                
            }
            else if(type as! String == "Alert")
            {
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "AlertReceived"), object: nil, userInfo: nil)
                print("********Alert Received**********")
            }
            else
            {
               
            }

        }

//        call commented section only when user status is changed
        
    }
}
// [END ios_10_message_handling]

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        UserDefaults.standard.setValue(fcmToken as! String , forKey: "fcmToken")
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}
