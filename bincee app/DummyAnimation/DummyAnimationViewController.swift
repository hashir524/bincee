//
//  DummyAnimationViewController.swift
//  bincee app
//
//  Created by Apple on 12/21/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class DummyAnimationViewController: UIViewController {
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var imgBus: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imgBus.frame.origin.x = -400
        animate(self.imgBus)
//        animateImageView(fromStartPosition: CGPoint(x: 0, y: 0), toEndPosition: CGPoint(x: self.view.frame.width, y: self.view.frame.width))
        // Do any additional setup after loading the view.
    }
    
    func animateImageView(fromStartPosition aStartPosition: CGPoint, toEndPosition aEndPosition: CGPoint) {
        let oldFrame = CGRect(x: aStartPosition.x, y: aStartPosition.y, width: imgBus.frame.size.width, height: imgBus.frame.size.height)
        imgBus.frame = oldFrame
        
        UIView.animate(withDuration: 5.0, delay: 0.0, options: .curveEaseInOut, animations: {
            var newFrame: CGRect = self.imgBus.frame
            newFrame.origin = aEndPosition
            self.imgBus.frame = newFrame
        }) { finished in
            if finished {
                self.animateImageView(fromStartPosition: aStartPosition, toEndPosition: aEndPosition)
            }
        }
    }
    func animate(_ image: UIImageView) {
        
        UIView.animate(withDuration: 5, delay: 0, options: .curveLinear, animations: {
            image.transform = CGAffineTransform(translationX: self.view.frame.width + 150 , y: 0)
            
        }) { (success: Bool) in
            image.transform = CGAffineTransform.identity
            if(image.frame.origin.x == self.view.frame.width / 2)
            {
                print("Horizontally in center")
            }
            self.animate(image)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
