//
//  NetworkingFunctions.h
//  shop catalog
//
//  Created by Arslan Raza on 8/31/17.
//  Copyright © 2017 MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkingDelegate.h"
#import <AFNetworking/AFNetworking.h>

@interface NetworkingFunctions : NSObject

+(NetworkingFunctions *)sharedNetworkingFunctions;
-(void)postData:(NSString*)url andParameters: (NSDictionary*)parameters;
-(void)setupValues;
-(void)getData:(NSString*)url andParameters:(NSDictionary*)parameters;
@property(nonatomic,retain) id<NetworkingDelegate> networkingDelegate;
@property(nonatomic,retain) AFHTTPSessionManager* manager;
//-(void)PostJSONToURL:(NSString *)url withParams:(NSDictionary *)params ;
-(void)PostJSONToURL:(NSString *)url withParams:(NSDictionary *)params withBearer:(BOOL)bearerPresent;
-(void)uploadPictures:(NSString*)url;
-(void)cancelTask;
-(void)uploadImageToServer:(NSURL *)fileNameSelected;
-(void)deleteDataOfId:(NSString *)completeUrl;
-(void)getJsonData:(NSString *)url withParams:(NSDictionary *)params withBearer:(BOOL)bearerPresent;
@end
