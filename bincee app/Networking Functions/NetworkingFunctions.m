//
//  NetworkingFunctions.m
//  shop catalog
//
//  Created by Arslan Raza on 8/31/17.
//  Copyright © 2017 MacBook Pro. All rights reserved.
//

#import "NetworkingFunctions.h"
#import <AFNetworking/AFNetworking.h>

@implementation NetworkingFunctions

{
    NSURLSessionDataTask * serviceTask;
    UIProgressView* progressView;
}

+(NetworkingFunctions *)sharedNetworkingFunctions{
    static NetworkingFunctions* sharedNetworking= nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate,^{
        sharedNetworking= [[self alloc]init];
    });
    return sharedNetworking;
}

/*! @brief This function will be called once so that only one manager is used throughout the project. */

-(void)setupValues{
    self.manager = [AFHTTPSessionManager manager];
}

/*! @brief The use of this function is to call GET api. */

-(void)postData:(NSString*)url andParameters: (NSString*)parameters
{
    NSError * error;
    NSLog(@"Received Parameters%@", parameters);
    //    NSMutableDictionary* response = [[NSMutableDictionary alloc]init];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:[parameters dataUsingEncoding:NSUTF8StringEncoding]
                                                         options:NSJSONReadingMutableContainers
                                                           error:&error];
    
    
    serviceTask = [   self.manager POST:url parameters:json progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        [_networkingDelegate onPostDataReceive:responseObject];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
        [_networkingDelegate onFailure:error];
        
    }
     ];
}

/*! @brief The use of this function is to call GET api. */

-(void)getData:(NSString*)url andParameters:(NSDictionary*)parameters
{
    
    serviceTask = [self.manager GET:url parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@", responseObject);
        [_networkingDelegate onGetDataReceive:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
        [_networkingDelegate onFailure:error];
    }];
}

-(void)uploadPictures:(NSString*)url
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
   NSString * authToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"authToken"];
  [request setValue:[NSString stringWithFormat:@"Bearer %@",authToken] forHTTPHeaderField:@"Authorization"];
    request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:@"http://access.bincee.com/avatar/upload" parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:[NSURL fileURLWithPath:url] name:@"file" fileName:url mimeType:@"image/jpeg" error:nil];
    } error:nil];
    
    self.manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [self.manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      dispatch_async(dispatch_get_main_queue(), ^{
                          //Update the progress view
                          [progressView setProgress:uploadProgress.fractionCompleted];
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          NSLog(@"Error: %@", error);
                      } else {
                          NSLog(@"%@ %@", response, responseObject);
                      }
                  }];
    
    [uploadTask resume];
}

/*! @brief The use of this function is to call POST api with json data in body as parameteres this function can be called from anywhere within the project by passing url and required parameteres */

-(void)PostJSONToURL:(NSString *)url withParams:(NSDictionary *)params withBearer:(BOOL)bearerPresent
{

    NSError * err;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&err];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setAllHTTPHeaderFields:self.manager.requestSerializer.HTTPRequestHeaders];
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    if(bearerPresent == true)
    {
        NSString * authToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"authToken"];
        [request setValue:[NSString stringWithFormat:@"Bearer %@",authToken] forHTTPHeaderField:@"Authorization"];
    }
    request.HTTPBody = jsonData;
    NSLog(@"Params Are %@",jsonData);
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
//    NSURLSessionDataTask *sessionTask
    serviceTask  = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                NSLog(@"url->%@\nparams->%@\nresponse->%@",url,jsonData,response);
                               [self.networkingDelegate onPostDataReceive:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            } else {
                [self.networkingDelegate onFailure:error];
            }
        });
        
    }];
    [serviceTask resume];
    
    
}

-(void)getJsonData:(NSString *)url withParams:(NSDictionary *)params withBearer:(BOOL)bearerPresent
{
    
    NSError * err;
   
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setAllHTTPHeaderFields:self.manager.requestSerializer.HTTPRequestHeaders];
    request.HTTPMethod = @"GET";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    if(bearerPresent == true)
    {
        NSString * authToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"authToken"];
        [request setValue:[NSString stringWithFormat:@"Bearer %@",authToken] forHTTPHeaderField:@"Authorization"];
    }
    
   
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
//    NSURLSessionDataTask *sessionTask
    serviceTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                NSLog(@"url->%@\nresponse->%@",url,response);
                [self.networkingDelegate onGetDataReceive:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            } else {
                [self.networkingDelegate onFailure:error];
            }
        });
        
    }];
    [serviceTask resume];
    
    
}
-(void)cancelTask
{
    [serviceTask cancel];
    
}

-(void)deleteDataOfId:(NSString *)completeUrl
{
    serviceTask = [self.manager DELETE:completeUrl parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [_networkingDelegate onDeleteDataResult:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [_networkingDelegate onFailure:error];
    }];
    
    
}
-(void)uploadImageToServer:(NSURL *)fileNameSelected
{
    NSString * authToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"authToken"];
    NSString * bearerToken = [NSString stringWithFormat:@"Bearer %@",authToken];
    NSDictionary *headers = @{ @"content-type": @"multipart/form-data",
                               @"Authorization": bearerToken,
                               @"cache-control": @"no-cache"};
    NSArray *parameters = @[ @{ @"name": @"image", @"fileName": fileNameSelected.absoluteString } ];
    NSString *boundary = @"----WebKitFormBoundary7MA4YWxkTrZu0gW";
    
    NSError *error;
    NSMutableString *body = [NSMutableString string];
    for (NSDictionary *param in parameters) {
        [body appendFormat:@"--%@\r\n", boundary];
        if (param[@"fileName"]) {
            [body appendFormat:@"Content-Disposition:form-data; name=\"%@\"; filename=\"%@\"\r\n", param[@"name"], param[@"fileName"]];
            [body appendFormat:@"Content-Type: %@\r\n\r\n", param[@"contentType"]];
            [body appendFormat:@"%@", [NSString stringWithContentsOfFile:param[@"fileName"] encoding:NSUTF8StringEncoding error:&error]];
            if (error) {
                NSLog(@"%@", error);
            }
        } else {
            [body appendFormat:@"Content-Disposition:form-data; name=\"%@\"\r\n\r\n", param[@"name"]];
            [body appendFormat:@"%@", param[@"value"]];
        }
    }
    [body appendFormat:@"\r\n--%@--\r\n", boundary];
    NSData *postData = [body dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://access.bincee.com/avatar/upload"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                                                             options:kNilOptions
                                                                                                               error:&error];
                                                         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"Response of image upload %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}
@end
