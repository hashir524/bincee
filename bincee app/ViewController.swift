//
//  ViewController.swift
//  bincee app
//
//  Created by Apple on 10/5/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var btnBar: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.revealViewController().rearViewRevealWidth = self.view.frame.width-85;
         
       // self.btnBar.target = self.revealViewController()
        if self.revealViewController() != nil {
            self.btnBar.target = self.revealViewController()
            self.btnBar.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func showAlert(_ sender: Any) {
        let vc = CalendarAlert.init(frame: UIScreen.main.bounds)
        
        self.view.addSubview(vc);
        //self.view.bringSubview(toFront: vc)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

