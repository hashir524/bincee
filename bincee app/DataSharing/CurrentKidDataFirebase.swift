//
//  CurrentKidDataFirebase.swift
//  bincee app
//
//  Created by Apple on 1/2/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Firebase

class CurrentKidDataFirebase: NSObject {
    static let shared = CurrentKidDataFirebase()
    var currentKidInformationApi : NSDictionary = NSDictionary()
    var currentDrivePositionFirebase : GeoPoint = GeoPoint(latitude: 0, longitude: 0)
    var currentKidInformaiton : NSDictionary = NSDictionary()
    var driverId : Int = Int()
    var notificationForKidId : Int = Int()
    var schoolLatLng : GeoPoint = GeoPoint(latitude: 0, longitude: 0)
    var isPresent : Bool = false
    var isAbsent : Bool = false
    var estimatedEta : String = ""
    var isBusStatusVisible : Bool = false
     var isFinalStatusVisible : Bool = false
    private override init() {
        
    }
}
