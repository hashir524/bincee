#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class MBBannerInstruction;
@class MBFixLocation;
@class MBNavigationStatus;
@class MBRouterResult;
@class MBVoiceInstruction;

@interface MBNavigator : NSObject

- (nonnull instancetype)init;
- (nonnull MBNavigationStatus *)setRouteForRouteResponse:(nonnull NSString *)routeResponse
                                                   route:(uint32_t)route
                                                     leg:(uint32_t)leg;
- (BOOL)updateLocationForFixLocation:(nonnull MBFixLocation *)fixLocation;
- (nonnull MBNavigationStatus *)getStatusForTimestamp:(nonnull NSDate *)timestamp;
- (nullable NSNumber *)getBearing;
- (nullable MBBannerInstruction *)getBannerInstruction;
- (nullable MBVoiceInstruction *)getVoiceInstruction;
- (nullable MBBannerInstruction *)getBannerInstructionForIndexInRoute:(uint32_t)indexInRoute;
- (nullable MBVoiceInstruction *)getVoiceInstructionForIndexInRoute:(uint32_t)indexInRoute;
- (nullable NSArray<CLLocation *> *)getRouteGeometry;
- (nullable NSArray<CLLocation *> *)getRouteBoundingBox;
- (nonnull NSString *)getHistory;
- (void)toggleHistoryForOnOff:(BOOL)onOff;
- (nonnull MBNavigationStatus *)changeRouteLegForRoute:(uint32_t)route
                                                   leg:(uint32_t)leg;
- (BOOL)configureNavigatorForName:(nonnull NSString *)name
                            value:(nonnull NSString *)value;
/** Offline only functions */
- (uint64_t)configureRouterForTilesPath:(nonnull NSString *)tilesPath
                       translationsPath:(nonnull NSString *)translationsPath;
- (nonnull MBRouterResult *)getRouteForDirectionsUri:(nonnull NSString *)directionsUri;
- (uint64_t)unpackTilesForPacked_tiles_path:(nonnull NSString *)packed_tiles_path
                           output_directory:(nonnull NSString *)output_directory;

@end
